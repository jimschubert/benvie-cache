(function(global, require, exports){

  /**
   * Compiled script wrapper holding the actual script and information about it
   * @param {Script} script  Raw script object
   * @param {String} code    Raw source code
   * @param {String} [name]  Optional name that can be used in stack traces
   */
  function CompiledScript(script, code, name){
    Object.defineProperties(this, {
      script:  { value: script },
      code:    { value: code },
      name:    { value: name || 'anonymous', enumerable: true },
      history: { value: new RunHistory('context'), enumerable: true }
    });
  }

  function setRunInContext(runner){
    CompiledScript.prototype.runInContext = function runInContext(context){
      if (typeof context === 'undefined') {
        context = createContext('global');
      } else if (Object.getPrototypeOf(context) !== ExecutionContext.prototype) {
        context = new ExecutionContext(context);
      }

      var result = runner(this.script, context.global);
      context.history.add(result, this);
      return this.history.add(result, context);
    }
  }


  /**
   * Context wrapper holding the actual context object and information about it
   * @param {Context} global  The context global object
   * @param {String}  [name]  Optional name to identify the context
   */
  function ExecutionContext(global, name){
    Object.defineProperties(this, {
      name:    { value: name || '', enumerable: true },
      global:  { value: global },
      history: { value: new RunHistory('script'), enumerable: true }
    });
  }

  ExecutionContext.prototype.run = function run(input){
    if (typeof input === 'string') {
      input = wrap(input);
    }
    input.runInContext(this);
    return this.history.last();
  }


  /**
   * Class for basic history recording of events, inheriting from Array.prototype
   * @param {String} type Label for the counterparty for records
   */
  function RunHistory(type){
    Array.call(this);
    Object.defineProperty(this, 'type', { value: type });
  }

  RunHistory.prototype = {
    __proto__: [],
    constructor: RunHistory,
    add: function add(result, counterparty){
      result = { result: result };
      result[this.type] = counterparty;
      this.push(Object.freeze(result));
      return result;
    },
    last: function last(){
      return this[this.length - 1];
    }
  };


  // Filter out environments with require but not the specific (Node) module
  function hasModule(mod){
    try { return require(mod), true }
    catch (e) { return false }
  }


  var wrap = exports.wrap

  =hasModule('vm')
    ?setRunInContext(function(script, context){
      if (typeof context === 'undefined' || context === global) {
        return require('vm').runInThisContext(script);
      }
      if (context + '' === '[object Context]') {
        // ensure the context is initiated
        require('vm').runInContext('this', context);
        return script.runInContext(context);
      }
    })
    ||function wrap(code, name){
      try {
        return new CompiledScript(require('vm').createScript(code, name), code, name);
      }
      catch (e) { return e }
    }

    :setRunInContext(function(script, context){
      return script.call(context);
    })
    ||function wrap(code, name){
      try {
        var wrapper = Function('with(this){return(function(){\n'+code+' \n}.call(this))}');
        return new CompiledScript(wrapper, code, name);
      }
      catch (e) { return e }
    }


  var loadScript = exports.loadScript

  =hasModule('fs')
    ?function loadScript(filepath, callback){
       var fs = require('fs');
       var path = require('path');

       var async = typeof callback === 'function';
       var resolved = filepath;

       if (!path.existsSync(resolved)) {
         resolved = path.resolve(__dirname, resolved);
         if (!path.existsSync(resolved)) {
           var e = new Error("File " + filepath + " not found");
           if (async) return callback(e), false;
           throw e;
         }
       }
       var name = path.basename(filepath);
       if (async) {
         fs.readFile(resolved,'utf8', function(e, code){
           if (e) callback(e);
           var result = wrap(code, name);
           typeof result === 'function' ? callback(null, result) : callback(result);
         })
       } else {
         return wrap(fs.readFileSync(resolved, 'utf8'), name)
       }
     }

  :typeof XMLHttpRequest !== 'undefined'
    ?function loadScript(filepath, callback){
       var xhr = new XMLHttpRequest;
       var xhrError;
       var async = typeof callback === 'function';
       xhr.open('get', filepath, async);
       xhr.onerror = function(e){ xhrError = e }
       xhr.send();
       if (async){
         xhr.onload = function(){
           var result = xhrError ? xhrError : wrap(xhr.response, filepath);
           typeof result === 'function' ? callback(null, result) : callback(result);
         }
       } else {
         return xhrError ? xhrError : wrap(xhr.response, filepath);
       }
     }

  :false


  var createContext = exports.createContext

  =hasModule('vm')
    ?function createContext(name){
      if (name === 'global') {
        return new ExecutionContext(global, name);
      } else {
        return new ExecutionContext(require('vm').createContext(), name);
      }
    }

  :typeof HTMLIFrameElement
    ?function createContext(name){
      if (name === 'global') {
        return new ExecutionContext(global, name);
      } else {
        var iframe = document.body.appendChild(document.createElement('iframe'));
        var context = iframe.contentWindow;
        // removing the element leaves a mostly bare context that still has JS builtins (in FF and Chrome at least)
        // this results in almost exactly the same result as vm.createContext() in Chrome and close to it in FF
        //document.body.removeChild(iframe);
        return new ExecutionContext(context, name);
      }
    }

  :false

}).apply(this, function bootstrapper(name){
  var _global = typeof global === 'undefined' ?
                typeof window === 'undefined' ?
                this : window : global;

  // basic browser require that tries to find the needed module on global or global.exports
  function rummage(name){
    name = name.replace(/[\\\/\.]/g, '');
    if (name in _global) {
      return _global[name];
    } else if ('exports' in _global && name in _global.exports) {
      return _global.exports[name];
    } else {
      throw new Error('Module "'+name+'" not found.');
    }
  }

  var _require = typeof require !== 'function' ?
                 typeof load !== 'function' ?
                 rummage : load : require;

  if (typeof exports === 'undefined') {
    // uninitiated and lacking exports
    _global.exports = {};
  }
  if ('exports' in _global) {
    // initiated but not native exports since it's on the global object
    var _exports = _global.exports[name] = {};
  } else {
    // real exports is available and non-global
    var _exports = exports;
  }

  return [_global, _require, _exports];
}('context-loader'));
