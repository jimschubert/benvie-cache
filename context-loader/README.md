# Context Loader

Create execution contexts, load JavaScript files, compile scripts without running them, run them as many times as you want in as many contexts as you want. Do it in Node or in the browser, with either asynchronous or synchronous loading.

# Usage

## First. Load context-loader

The main difference between usage in browsers and Node is in initially loading.

```
// Node
var contextLoader = require('context-loader');

// Browser
<script src="context-loader.js"></script>
<script>var contextLoader = exports['context-loader']</script>
```

## Second. create a context

The global object also works

```javascript
var context = contextLoader.createContext('myContext');
```

## Third. load scripts with context-loader

Script loading can be done in three ways.

```javascript
// asynchronous
contextLoader.loadScript('path/filename.js', function callback(error, compiledScript){
	if (compiledScript) { /*...*/ }
});

// synchronous
var compiledScript = contextLoader.loadScript('path/filename.js');
if (compiledScript) { /*...*/ }

// raw code
var compiledScript = contextLoader.wrap('(function(){ return "I am compiled code" })()');
if (compiledScript) { /*...*/ }
```
If loaded synchronously any errors will be passed back as the result


## Fourth. Run code in contexts

Once you have the compiled script you can then run it in contexts. State isn't shared inside the script itself so it can be used multiple times in the same or different contexts. In Node, a context is either the global object or a Context object created using the vm module. In the browser a context is either the global object or some other window object like an iframe contentWindow.

```javascript
// run using the current global context
compiledScript.runInContext();

// run scripts and code in various ways
var executionOutcome = compiledScript.runInContext(context);
var executionOutcome = context.run(compiledScript);
var executionOutcome = context.run('x = {}');

```

Execution outcome is one of:
```javascript
// using script.runInContext(context)
{ context: [object ExecutionContext],   // either the provided or newly initialized wrapped context
	result: returnValue }                 // completion value of the executed code, if any

// using context.run(scriptOrCode)
{ script: [object CompiledScript],      // either the provided CompiledScript or newly created script wrapper if code was passed
	result: returnValue }                 // completion value of the executed code, if any
```

## Execution History

Every time a script is run there is a record created on both the script and the context. Each piece of code is wrapped in a CompiledScript object with a history record, and every context is wrapped in an ExecutionContext object also with a history.

The history allows you to easily replay a series of code executions in the same order, or to cross-reference which contexts have had what code run in them.

```javascript
// "clone" a context organically by replaying all the code run in it in a new context
var newContext = contextLoader.createContext();
existingContext.history.forEach(function(outcome){
	newContext.run(outcome.script);
});