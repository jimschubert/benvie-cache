# symbol-then

_then_ is a module who's sole purpose is to provide a standardized way to reliably indicate something is *thenable*, even between separate libraries. It does this using a ES6 `Symbol` object.

This is a forward-looking problem. That is: it will be awhile until ES6 Symbols are reliably available across different JS engines. Until that time, this library will is focused on environments with a centralized module system, like node.js.

# Well Known Symbols

ES6 introduces the concept of "Well Known Symbols". These are Symbols that are provided by the JS engine that are universal. The primary example for this is the _@@iterator_ Symbol. An object which is iterable need only define a property using _@@iterator_ as its key and then everyone knows how to get an iterator from that object (`obj[iteratorSymbol]()`).

As long as everyone uses the same universal _then_ Symbol then we have an agreed upon way to designate something as a *thenable*.

# Usage of "then"

Example of marking Q promises as *thenable*.

```js
var then = require('symbol-then').symbol;
var isThenable = require('symbol-then').isThenable;

Q.makePromise.prototype[then] = Q.makePromise.prototype.then;


Q('hello world')[then](function(value){
  console.log(value); // 'hello world'
});

console.log(isThenable(Q())); // true
```