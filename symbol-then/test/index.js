var assert = require('assert');

var then = require('../symbol-then').symbol;
var isThenable = require('../symbol-then').isThenable;

var notThenable = {};

var thenable = {
  then: function(){
    return "I'm thenable!";
  }
};

thenable[then] = thenable.then;


assert.equal(isThenable(notThenable), false,
  'isThenable(notThenable) === false');

assert.equal(isThenable(thenable), true,
  'isThenable(thenable) === true');

assert.equal(thenable[then](), "I'm thenable!",
  'thenable[then]() === "I\'m thenable!');