void function(exports){
  if (typeof Symbol === 'undefined') {
    throw new Error('Symbols are unsupported');
  }

  var substrate;

  if (typeof process !== 'undefined') {
    substrate = process;
  } else if (typeof navigator !== 'undefined') {
    substrate = navigator;
  } else {
    throw new Error('Unsupported host environment');
  }

  // global namespace for Well Known Symbols
  if (!substrate.wellKnownSymbols) {
    substrate.wellKnownSymbols = Object.create(null);
  }

  // add Well Known Symbol for "then"
  if (!substrate.wellKnownSymbols.then) {
    substrate.wellKnownSymbols.then = {
      symbol: Symbol('then'),
      repo: "https://github.com/Benvie/symbol-then.git",
    };
  }

  var then = exports.symbol = substrate.wellKnownSymbols.then.symbol;

  exports.isThenable = function isThenable(value){
    return value ? typeof value[then] === 'function': false;
  };
}(typeof exports === 'undefined' ? {} : exports);