class Mirror {
  private @target;

  constructor(target){
    this.@target = target;
  }


}

class Queue {
  private @in;
          @out;

  constructor(){
    this.@in = [];
    this.@out = [];
  }

  queue(item){
    this.@in.push(item);
  }

  dequeue(){
    if (!this.@out.length) {
      if (this.@in.length) {
        this.@out = this.@in;
      }
    }
    return this.@out.pop();
  }
}
