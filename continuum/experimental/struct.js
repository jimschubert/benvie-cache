const TYPE = {};
const VIEW = {};

function TypedBuffer(type){
  this.@type = type;
  this.@raw = new ArrayBuffer(type.@byteLength);
  this.@pointers = Object.create(null);
}

extend(TypedBuffer.prototype, {
  get byteLength(){
    throw new TypeError('byteLength not provided for typed buffers');
  }
});

function int(bytes, min, max, View, typeName, toInt) {
  function t(x){
    if ($$IsConstruct()) {
      throw new TypeError(`cannot instantiate atomic type ${typeName}`);
    }

    const type = Type(x);

    if (type === 'String') {
      x = parseInt(x);
      if (isNaN(x)) {
        throw new TypeError(`invalid ${typeName} string`);
      }
    } else if (type !== 'Number') {
      throw new TypeError(`invalid ${typeName}: ${x}`);
    }

    return toInt(x);
  }

  extend(t, {
    toString(){
      return `[type ${typeName}]`;
    },
    @IsSame(u){
      return u.@variant === this.@variant;
    },
    @Reify(raw, i){
      return new View(raw, i, 1)[0];
    },
    @Convert(x, raw, i){
      const view = new View(raw, i, 1);

      if (x === true) {
        view[0] = 1;
      } else if (x === false) {
        view[0] = 0;
      } else if (typeof x === 'number' && x >= min && x <= max && floor(x) === ceil(x)) {
        view[0] = x;
      } else {
        throw new TypeError(`invalid ${typeName} value: ${x}`);
      }
    },
    @class     : TYPE,
    @shortName : typeName,
    @variant   : typeName,
    @container : false,
    @fixedSize : true,
    @allowCasts: true,
    @byteLength: bytes
  });

  return t;
}

function float(k, View, cast) {
  const bytes    = k / 8,
        typeName = 'float' + k;

  function t(x){
    if ($$IsConstruct()) {
      throw new TypeError(`cannot instantiate atomic type ${typeName}`);
    }

    const type = Type(x);
    if (type === 'String') {
      x = parseFloat(x);
      if (x !== x) {
        throw new TypeError(`invalid ${typeName} string`);
      }
    } else if (type !== 'Number') {
      throw new TypeError(`invalid ${typeName}: ${x}`);
    }

    return cast(x);
  }

  extend(t, {
    toString(){
      return `[type ${typeName}]`;
    },
    @@IsSame(u) {
      return u.@variant === this.@variant;
    },
    @@Reify(raw, i) {
      return new View(raw, i, 1)[0];
    },
    @@Convert(x, raw, i) {
      const view = new View(raw, i, 1);

      if (x === true) {
        view[0] = 1;
      } else if (x === false) {
        view[0] = 0;
      } else if (typeof x === 'number') {
        view[0] = cast(x);
      } else {
        throw new TypeError(`invalid ${typeName} value: ${x}`);
      }
    },
    @class     : TYPE,
    @shortName : typeName,
    @variant   : typeName,
    @container : false,
    @fixedSize : true,
    @allowCasts: true,
    @byteLength: bytes
  });

  return t;
}


const uint8   = int(1,           0,       0xff, Uint8Array,  'uint8',  ToUint(8)),
      uint16  = int(2,           0,     0xffff, Uint16Array, 'uint16', ToUint(16)),
      uint32  = int(4,           0, 0xffffffff, Uint32Array, 'uint32', x => x >>> 0),
      int8    = int(1,       -0x80,       0x7f, Int8Array,   'int8',   ToInt(8)),
      int16   = int(2,     -0x8000,     0x7fff, Int16Array,  'int16',  ToInt(16)),
      int32   = int(4, -0x80000000, 0x7fffffff, Int32Array,  'int32',  x => x >> 0),
      float32 = float(32, Float32Array, x => new Float32Array([x])[0]),
      float64 = float(64, Float64Array, x => x);

function struct(desc, name) {
  const t = class {
    constructor(x, byteOffset) {
      if (!IsConstruct()) {
        return new t(x, byteOffset);
      }

      let update = false;
      this.@class = VIEW;
      this.@type = t;

      switch (typeof x) {
        case 'object':
          if (x instanceof ArrayBuffer) {
            if (!t.@allowCasts) {
              throw new TypeError(`cannot create ArrayBuffer view for type ${t.shortName}`);
            }
            this.@buffer = x;
            this.@raw = x;
            this.@byteOffset = byteOffset || 0;
            break;
          }
          update = true;

        case 'undefined':
          this.@buffer = new TypedBuffer(t);
          this.@raw = this.@buffer.@raw;
          this.@byteOffset = 0;
          break;

        default:
          throw new TypeError('expected ArrayBuffer or optional descriptor');
      }

      if (update) {
        this.update(x);
      }
    }

    update(x){
      for (var fieldName in this.@type.@fieldTypes) {
        this[fieldName] = x[fieldName];
      }
    }

    toString(){
      const structName = this.@type.@structName;
      return `[struct${structName ? ' ' + structName : ''}]`;
    }

    get buffer(){
      return this.@buffer;
    }

    get byteLength(){
      if (this.@buffer instanceof TypedBuffer) {
        throw new TypeError('byteLength not defined for TypedBuffer views');
      }

      return this.@type.@byteLength;
    }

    get byteOffset(){
      if (this.@buffer instanceof TypedBuffer) {
        throw new TypeError('byteOffset not defined for TypedBuffer views');
      }

      return this.@byteOffset;
    }
  }

  const fieldTypes = ObjectCreate(null);


  let bytes      = 0,
      offset     = 0,
      lastName   = null,
      lastType   = null,
      allowCasts = true;

  for (let [fieldName, fieldType] of entries(desc)) {
    if (lastType && !lastType.@fixedSize) {
      throw new TypeError(`field ${lastName} is not of fixed size`);
    }

    fieldTypes[fieldName] = fieldType;

    const i = offset;

    defineProperty(t.prototype, fieldName, {
      configurable: false,
      enumerable: true,
      get(){
        return fieldType.@@Reify(this.@raw, this.@byteOffset + i, this.@buffer);
      },
      set(x){
        return fieldType.@@Convert(x, this.@raw, this.@byteOffset + i, this.@buffer);
      }
    });

    offset += fieldType.@byteLength;
    bytes += fieldType.@byteLength;
    lastName = fieldName;
    lastType = fieldType;
    allowCasts = allowCasts && fieldType.@allowCasts;
  }

  extend(t, {
    toString(){
      return '[type Struct]';
    },
    @@IsSame(u){
      return u === t;
    },
    @@Reify(raw, i, buf){
      return extend(ObjectCreate(p), {
        @class: VIEW,
        @type: t,
        @buffer: buf,
        @raw: raw,
        @byteOffset: i
      });
    },
    @@Convert:(x, raw, i, buf){
      if (typeof x !== 'object' || x === null)
        throw new TypeError(`expected object, got ${x}`);
      for (var fieldName in this.@fieldTypes) {
        var fieldType = this.@fieldTypes[fieldName];
        fieldType.@@Convert(x[fieldName], raw, i, buf);
        i += fieldType.@byteLength;
      }
    },
    @class     : TYPE,
    @variant   : 'struct',
    @container : true,
    @structName: name || '',
    @shortName : name || 'struct',
    @fixedSize : lastFieldType.@fixedSize,
    @allowCasts: allowCasts,
    @byteLength: t.@fixedSize ? bytes : undefined,
    @fieldTypes: fieldTypes
  });

  return t;
}

function array(elementType, length) {
  const fixedSize = typeof length !== 'undefined';

  const t = class {
    constructor(x, byteOffset, instanceLength){
      if (!$$IsConstruct()) {
        return new t(x, byteOffset, instanceLength);
      }

      let update = false;

      this.@class = VIEW;
      this.@type = t;

      switch (typeof x) {
        case 'number':
          if (fixedSize) {
            throw new TypeError(`array type already has length ${length}`);
          }
          this.length = instanceLength = x;
          this.@buffer = new TypedBuffer(array(t.@elementType, instanceLength));
          this.@raw = this.@buffer.@raw;
          this.@byteOffset = 0;
          break;

        case 'undefined':
          this.length = fixedSize ? length : instanceLength = 0;
          this.@buffer = new TypedBuffer(t);
          this.@raw = this.@buffer.@raw;
          this.@byteOffset = 0;
          break;

        case 'object':
          if (x instanceof ArrayBuffer) {
            if (!elementType.@allowCasts)
              throw new TypeError(`cannot create ArrayBuffer view for type ${elementType.shortName}`);
            this.@buffer = x;
            this.@raw = x;
            this.@byteOffset = byteOffset || 0;
            if (typeof instanceLength === 'number') {
              if (fixedSize) {
                throw new TypeError(`array type already has length ${length}`);
              }
            }
          } else {
            update = true;
          }
          break;

        default:
          throw new TypeError(`expected optional number or object, got ${x}`);
      }

      if (!fixedSize) {
        for (let i = 0; i < instanceLength; i++) {
          const off = i * elementType.@byteLength;
          Object.defineProperty(this, i, {
            enumerable: true,
            configurable: false,
            get() {
              var elementType = this.@type.@elementType;
              return elementType.@@Reify(this.@raw, this.@byteOffset + off, this.@buffer);
            },
            set(x) {
              var elementType = this.@type.@elementType;
              elementType.@@Convert(x, this.@raw, this.@byteOffset + off, this.@buffer);
            }
          });
        }
      }

      if (update) {
        this.update(x);
      }
    }

    update(x){
      for (var i = 0; i < n; i++) {
        this[i] = x[i];
      }
    }

    toString(){
      return `[array ${this.@type.@elementType.@shortName}]`;
    }

    fill(x){
      for (var i = 0; i < n; i++)
        this[i] = x;
    }

    subarray(begin, end){
      if (typeof end === 'undefined') {
        end = this.length;
      }
      if (begin < 0 || begin >= end) {
        throw new RangeError(`begin index ${begin} out of range`);
      }
      if (end <= 0 || end > this.length) {
        throw new RangeError(`end index ${end} out of range`);
      }

      const type = this.@type.@elementType,
            u = array(type, end - begin);

      return u.@@Reify(this.@raw, this.@byteOffset + begin * type.@byteLength, this.@buffer);
    }

    toArray(){
      var result = [];
      for (var i = 0; i < n; i++)
        result[i] = this[i];
      return result;
    }

    map(f){
      const type   = this.@type.@elementType,
            A      = array(type),
            n      = this.length,
            result = new A(n);

      for (var i = 0; i < n; i++) {
        result[i] = this[i];
      }

      return result;
    }

    get buffer(){
      return this.@buffer;
    }

    get byteLength(){
      if (this.@buffer instanceof TypedBuffer) {
        throw new TypeError('byteLength not defined for TypedBuffer views');
      }

      return this.@type.@byteLength;
    }

    get byteOffset(){
      if (this.@buffer instanceof TypedBuffer) {
        throw new TypeError('byteOffset not defined for TypedBuffer views');
      }

      return this.@byteOffset;
    }

    get BYTES_PER_ELEMENT(){
      if (this.@buffer instanceof TypedBuffer) {
        throw new TypeError('BYTES_PER_ELEMENT not defined for TypedBuffer views');
      }

      return this.@type.@elementType.@byteLength;
    }
  }

  if (!elementType.@fixedSize) {
    throw new TypeError(`${elementType.@shortName} type is not of fixed size`);
  }

  extend(t, {
    toString(){
      return '[type Array]';
    },
    @@IsSame(u){
      return u.@variant === 'array' && this.@elementType.@IsSame(u.@elementType) && this.@length === u.@length;
    },
    @@Reify(raw, i, buf){
      return extend(ObjectCreate(t.prototype), {
        @class     : VIEW,
        @type      : t,
        @buffer    : buf,
        @raw       : raw,
        @byteOffset: i
      });
    },
    @@Convert(a, raw, i, buf){
      const type = this.@type.@elementType,
            incr = type.@byteLength;

      for (var j = 0; j < n; j++) {
        type.@@Convert(a[j], raw, i, buf);
        i += incr;
      }
    },
    @class      : TYPE,
    @variant    : 'array',
    @shortName  : 'array',
    @container  : true,
    @fixedSize  : fixedSize,
    @allowCasts : elementType.@allowCasts,
    @byteLength : fixedSize ? length * elementType.@byteLength : undefined,
    @elementType: elementType
  });

  extend(t.prototype, {
    forEach: Array.prototype.forEach,
    reduce: Array.prototype.reduce
  });

  if (fixedSize) {
    extend(t.prototype, {
      length: length
    });

    for (let i = 0; i < length; i++) {
      const off = i * elementType.@byteLength;
      defineProperty(t.prototype, i, {
        enumerable: true,
        configurable: false,
        get(){
          var elementType = this.@type.@elementType;
          return elementType.@@Reify(this.@raw, this.@byteOffset + off, this.@buffer);
        },
        set(x) {
          var elementType = this.@type.@elementType;
          elementType.@@Convert(x, this.@raw, this.@byteOffset + off, this.@buffer);
        }
      });
    }
  }

  extend(t, {
    @length: length
  });

  return t;
}

var object = {
  toString(){
    return '[type Object]';
  },
  toSource(){
    return 'object';
  },
  @@IsSame(u){
    return u.@variant === 'object';
  },
  @@Reify(raw, i, buf){
    return buf.@pointers[i] || null;
  },
  @@Convert(obj, raw, i, buf){
    if (typeof obj !== 'object') {
      throw new TypeError('expected object, got ' + obj);
    }
    buf.@pointers[i] = obj;
  },
  @class: TYPE,
  @variant: 'object',
  @container: false,
  @shortName: 'object',
  @fixedSize: true,
  @allowCasts: false,
  @byteLength: 4
};

var any = {
  toString(){
    return '[type Any]';
  },
  toSource(){
    return 'any';
  },
  @IsSame(u){
    return u.@variant === 'any';
  },
  @Reify(raw, i, buf){
    return buf.@pointers[i];
  },
  @Convert(x, raw, i, buf){
    buf.@pointers[i] = x;
  },
  @class: TYPE,
  @variant: 'any',
  @container: false,
  @shortName: 'any',
  @fixedSize: true,
  @allowCasts: false,
  @byteLength: 4
};

function pointer(targetType){
  if (!targetType.@container) {
    throw new TypeError('target type is not a container type');
  }

  return {
    toString(){
      return '[type Pointer]';
    },
    toSource(){
      return 'pointer(' + this.@targetType.toSource() + ')';
    },
    @IsSame(u){
      return u.@variant === 'pointer' && this.@targetType.@IsSame(u.@targetType);
    },
    @Reify(raw, i, buf) {
      return buf.@pointers[i] || null;
    },
    @Convert(obj, raw, i, buf) {
      if (typeof obj !== 'object' || obj !== null && (obj.@class !== VIEW || !obj.@type.@IsSame(this.@targetType))) {
        throw new TypeError('expected object of type ' + this.@targetType.@shortName + ', got ' + obj);
      }
      buf.@pointers[i] = obj;
    },
    @class: TYPE,
    @variant: 'pointer',
    @container: false,
    @shortName: targetType.@shortName + '*',
    @fixedSize: true,
    @allowCasts: false,
    @byteLength: 4,
    @targetType: targetType
  };
}
