
var messages = {
  UnexpectedToken:  'Unexpected token %0',
  UnexpectedNumber:  'Unexpected number',
  UnexpectedString:  'Unexpected string',
  UnexpectedIdentifier:  'Unexpected identifier',
  UnexpectedReserved:  'Unexpected reserved word',
  UnexpectedEOS:  'Unexpected end of input',
  NewlineAfterThrow:  'Illegal newline after throw',
  InvalidRegExp: 'Invalid regular expression',
  UnterminatedRegExp:  'Invalid regular expression: missing /',
  InvalidLHSInAssignment:  'Invalid left-hand side in assignment',
  InvalidLHSInForIn:  'Invalid left-hand side in for-in',
  NoCatchOrFinally:  'Missing catch or finally after try',
  UnknownLabel: 'Undefined label \'%0\'',
  Redeclaration: '%0 \'%1\' has already been declared',
  IllegalContinue: 'Illegal continue statement',
  IllegalBreak: 'Illegal break statement',
  IllegalReturn: 'Illegal return statement',
  StrictModeWith:  'Strict mode code may not include a with statement',
  StrictCatchVariable:  'Catch variable may not be eval or arguments in strict mode',
  StrictVarName:  'Variable name may not be eval or arguments in strict mode',
  StrictParamName:  'Parameter name eval or arguments is not allowed in strict mode',
  StrictParamDupe: 'Strict mode function may not have duplicate parameter names',
  StrictFunctionName:  'Function name may not be eval or arguments in strict mode',
  StrictOctalLiteral:  'Octal literals are not allowed in strict mode.',
  StrictDelete:  'Delete of an unqualified identifier in strict mode.',
  StrictDuplicateProperty:  'Duplicate data property in object literal not allowed in strict mode',
  AccessorDataProperty:  'Object literal may not have data and accessor property with the same name',
  AccessorGetSet:  'Object literal may not have multiple get/set accessors with the same name',
  StrictLHSAssignment:  'Assignment to eval or arguments is not allowed in strict mode',
  StrictLHSPostfix:  'Postfix increment/decrement may not have eval or arguments operand in strict mode',
  StrictLHSPrefix:  'Prefix increment/decrement may not have eval or arguments operand in strict mode',
  StrictReservedWord:  'Use of future reserved word in strict mode',
  UnmatchedDelimiter: 'Unmatched Delimiter'
};

function nextChar() {
  return source[index++];
}

function current() {
  return source[index];
}

function isIn(el, list) {
  return list.indexOf(el) !== -1;
}

function code(c){
  return c.charCodeAt(0);
}

var isLineTerminator = (function(){
  var newline = /[\n\r\u2028\u2029]/;
  return function isLineTerminator(c) {
    return newline.test(c);
  };
})();


var isWhiteSpace = (function(){
  var ws = /[\u1680\u180e\u2000-\u200a\u2028\u2029\u202f\u205f\u3000\ufeff]/;
  return function isWhiteSpace(c) {
    return c === '\x20' || c === '\x09' || c === '\x0B' || c === '\x0C' || c === '\xA0' || code(c) >= 0x1680 && ws.test(ch);
  };
})();

  var ws = /[\u1680\u180e\u2000-\u200a\u2028\u2029\u202f\u205f\u3000\ufeff]/;

function skipComment() {
  var blockComment = false;
      lineComment = false;

  while (index < length) {
    var ch = source[index];

    if (lineComment) {
      ch = nextChar();
      if (isLineTerminator(ch)) {
        lineComment = false;
        if (ch === '\r' && source[index] === '\n') {
          ++index;
        }
        ++lineNumber;
        lineStart = index;
      }
    } else if (blockComment) {
      if (isLineTerminator(ch)) {
        if (ch === '\r' && source[index + 1] === '\n') {
          ++index;
        }
        ++lineNumber;
        ++index;
        lineStart = index;
        if (index >= length) {
          throwError({}, messages.UnexpectedToken, 'ILLEGAL');
        }
      } else {
        ch = nextChar();
        if (index >= length) {
          throwError({}, messages.UnexpectedToken, 'ILLEGAL');
        }
        if (ch === '*') {
          ch = source[index];
          if (ch === '/') {
            ++index;
            blockComment = false;
          }
        }
      }
    } else if (ch === '/') {
      ch = source[index + 1];
      if (ch === '/') {
        index += 2;
        lineComment = true;
      } else if (ch === '*') {
        index += 2;
        blockComment = true;
        if (index >= length) {
          throwError({}, messages.UnexpectedToken, 'ILLEGAL');
        }
      } else {
        break;
      }
    } else if (isWhiteSpace(ch)) {
      ++index;
    } else if (isLineTerminator(ch)) {
      ++index;
      if (ch ===  '\r' && source[index] === '\n') {
        ++index;
      }
      ++lineNumber;
      lineStart = index;
    } else {
      break;
    }
  }
}

function scanRegExp(){
  buffer = null;
  skipComment();
  var start = index,
      ch = source[index],
      str = nextChar(),
      classMarker = false;

  while (index < length) {
    ch = nextChar();
    str += ch;
    if (classMarker) {
      if (ch === ']') {
        classMarker = false;
      }
    } else {
      if (ch === '\\') {
        ch = nextChar();
        if (isLineTerminator(ch)) {
          throwError({}, messages.UnterminatedRegExp);
        }
        str += ch;
      } else if (ch === '/') {
        var terminated = true;
        break;
      } else if (ch === '[') {
        classMarker = true;
      } else if (isLineTerminator(ch)) {
        throwError({}, messages.UnterminatedRegExp);
      }
    }
  }

  if (!terminated) {
    throwError({}, messages.UnterminatedRegExp);
  }

  var pattern = str.substr(1, str.length - 2),
      flags = '';

  while (index < length) {
    ch = source[index];
    if (!isIdentifierPart(ch)) {
      break;
    }
    ++index;
    if (ch === '\\' && index < length) {
      ch = source[index];
      if (ch === 'u') {
        ++index;
        var restore = index;
        ch = scanHexEscape('u');
        if (ch) {
          flags += ch;
          str += '\\u';
          for (; restore < index; ++restore) {
            str += source[restore];
          }
        } else {
          index = restore;
          flags += 'u';
          str += '\\u';
        }
      } else {
        str += '\\';
      }
    } else {
      flags += ch;
      str += ch;
    }
  }

  try {
    var value = new RegExp(pattern, flags);
  } catch (e) {
    throwError({}, messages.InvalidRegExp);
  }

  return { literal: str, value: value, range: [start, index] };
}


function readLoop(toks, inExprDelim) {
  var delimiters = ['(', '{', '['];
  var parenIdents = ["if", "while", "for", "with"];
  var last = toks.length - 1;


  var fnExprTokens = [
    "(", "{", "[", "in", "typeof", "instanceof", "new", "return",
    "case", "delete", "throw", "void",
    // assignment operators
    "=", "+=", "-=", "*=", "/=", "%=", "<<=", ">>=", ">>>=", "&=", "|=", "^=",
    ",",
    // binary/unary operators
    "+", "-", "*", "/", "%", "++", "--", "<<", ">>", ">>>", "&", "|", "^", "!", "~",
    "&&", "||", "?", ":",
    "===", "==", ">=", "<=", "<", ">", "!=", "!=="
  ];
  // var fnDeclTokens = [";", "}", ")", "]", ident, literal, "debugger", "break", "continue", "else"];
  // don't need since these are all implicit in the else branch

  function back(n) {
    var idx = (toks.length - n > 0) ? (toks.length - n) : 0;
    return toks[idx];
  }


  skipComment();

  if(isIn(current(), delimiters)) {
    return readDelim();
  }

  if(current() === "/") {
    var prev = back(1);
    if (prev) {
      if (prev.value === "()") {
        if(isIn(back(2).value, parenIdents)) {
          return scanRegExp();
        }
        return nextChar();
      }
      if(prev.value === "{}") {
        // named function
        if(back(4).value === "function") {
          if(isIn(back(5).value, fnExprTokens)) {
            return nextChar();
          }
          if ((toks.length - 5 <= 0) && inExprDelim) {
            // case where: (function foo() {} /asdf/) or [function foo() {} /asdf]
            return nextChar();
          }
        }
        // unnamed function
        if(back(3).value === "function") {
          if(isIn(back(4).value, fnExprTokens)) {
            return nextChar();
          }
          if ((toks.length - 4 <= 0) && inExprDelim) {
            // case where: (function foo() {} /asdf/) or [function foo() {} /asdf]
            return nextChar();
          }
        }
        return scanRegExp();
      }
      if(prev.type === Token.Punctuator) {
        return scanRegExp();
      }
      if(isKeyword(toks[toks.length - 1].value)) {
        return scanRegExp();
      }
      return nextChar();
    }
    return scanRegExp();
  }
  return nextChar();
}

function readDelim() {
  var startDelim = nextChar(),
    matchDelim = {
      '(': ')',
      '{': '}',
      '[': ']'
    },
    inner = [];

  var delimiters = ['(', '{', '['];
  var token = startDelim;

  assert(delimiters.indexOf(startDelim.value) !== -1, "Need to begin at the delimiter");

  var startLineNumber = token.lineNumber;
  var startLineStart = token.lineStart;
  var startRange = token.range;
  while(index <= length) {
    token = readLoop(inner, (startDelim.value === "(" || startDelim.value === "["));
    if((token.type === Token.Punctuator) && (token.value === matchDelim[startDelim.value])) {
      break;
    } else {
      inner.push(token);
    }
  }

  // at the end of the stream but the very last char wasn't the closing delimiter
  if(index >= length && matchDelim[startDelim.value] !== source[length-1]) {
    throwError({}, Messages.UnexpectedEOS);
  }

  var endLineNumber = token.lineNumber;
  var endLineStart = token.lineStart;
  var endRange = token.range;
  return {
    type: Token.Delimiter,
    value: startDelim.value + matchDelim[startDelim.value],
    inner: inner,
    startLineNumber: startLineNumber,
    startLineStart: startLineStart,
    startRange: startRange,
    endLineNumber: endLineNumber,
    endLineStart: endLineStart,
    endRange: endRange
  };
};



// (Str) -> [...CSyntax]
function read(code) {
  var token, tokenTree = [];

  source = code;
  index = 0;
  lineNumber = (source.length > 0) ? 1 : 0;
  lineStart = 0;
  length = source.length;
  buffer = null;
  state = {
    allowIn: true,
    labelSet: {},
    lastParenthesized: null,
    inFunctionBody: false,
    inIteration: false,
    inSwitch: false
  };


  while(index < length) {
    tokenTree.push(readLoop(tokenTree));
  }
  var last = tokenTree[tokenTree.length-1];
  if(last && last.type !== Token.EOF) {
    tokenTree.push({
      type: Token.EOF,
      value: "",
      lineNumber: last.lineNumber,
      lineStart: last.lineStart,
      range: [index, index]
    });
  }

  return expander.tokensToSyntax(tokenTree);
}












function isDecimalDigit(char){
  const code = $$CharCode(char);
  return code >= 0x30 && code <= 0x39;
}



function isWhiteSpace(char) {
  const code = $$CharCode(char);
  return code === 0x20 || code === 0x09 || code === 0x0B || code === 0x0C || code === 0xA0
      || code >= 0x1680 && code === 0x1680 || code === 0x180E || code >= 0x2000 && code <= 0x200A
      || code === 0x202F || code === 0x205F || code === 0x3000 || code === 0xFEFF;
}

  return code === 0x20 || code === 0x09 || code === 0x0B || code === 0x0C || code === 0xA0
      || code >= 0x1680 && code === 0x1680 || code === 0x180E || code >= 0x2000 && code <= 0x200A
      || code === 0x202F || code === 0x205F || code === 0x3000 || code === 0xFEFF;

function scanNumber(index, string, callback){
}


[
  ['\u0020', '0x0020'],
  ['\u0009', '0x0009'],
  ['\u000B', '0x000B'],
  ['\u000C', '0x000C'],
  ['\u00A0', '0x00A0'],
  ['\u2000', '0x2000'],
  ['\u2001', '0x2001'],
  ['\u2002', '0x2002'], ['\u2003', '0x2003'], ['\u2004', '0x2004'], ['\u2005', '0x2005'], ['\u2006', '0x2006'], ['\u2007', '0x2007'], ['\u2008', '0x2008'], ['\u2009', '0x2009'], ['\u200A', '0x200A'], ['\u202F', '0x202F'], ['\u205F', '0x205F'], ['\u3000', '0x3000'], ['\uFEFF', '0xFEFF']
  ]

function hex(char){
  if (isDecimalDigit(char)) {
    return true;
  }

  const code = $$CharCode(char);
  return (code >= 0x41 && code <= 0x46) || (code >= 0x61 && code <= 0x66);
}

function octal(char){
  const code = $$CharCode(char);
  return code >= 0x30 && code <= 0x37;
}

function binary(char){
  return char === '0' || char === '1';
}


hex.base = 16;
octal.base = 8;
binary.base = 2;

function StringToNumber(argument){
  const len = $$StringLength(argument);
  let index  = 0,
      number = '',
      char;

  while (index < len && isWhiteSpace(argument[index])) {
    char = argument[index++];
  }

  if (len === 0 || index === len) {
    return 0;
  }


  if (char !== '.') {
    number = argument[index++];
    char = argument[index];
    if (number === '0') {
      let type;
      if (char === 'X' || char === 'x') {
        type = hex;
      } else if (char !== 'o' && char !== 'O') {
        type = octal;
      } else if (octal(char)) {
        index--;
        type = octal;
      } else if (char === 'B' || char === 'b') {
        type = binary;
      }

      if (type) {
        const number = '';

        while (index++ < len) {
          char = argument[index];
          number += char;
          if (!type(char)) {
            return NaN;
          }
        }

        if (number === '') {
          return NaN;
        }

        return $$atoi(number, type.base);
      }
    }

    while (index < length) {
      char = argument[index];
      if (!isDecimalDigit(char)) {
        break;
      }
      number += argument[index++];
    }
  }

  if (char !== '.') {
    while (index < length) {
      if (!isDecimalDigit(char)) {
        break;
      }
      char = argument[index++];
      number += char;
    }
  }

  if (char === 'e' || char === 'E') {
    number += char;
    char = argument[++index];
    if (char === '+' || char === '-') {
      number += char;
      index++;
    }

    char = argument[index];
    if (!isDecimalDigit(char)) {
      return NaN;
    }

    number += char;

    while (index++ < length) {
      char = argument[index];
      if (!isDecimalDigit(char)) {
        break;
      }
      number += char;
    }
  }

  if (index < length) {
    char = argument[index];
    if (!isDecimalDigit(char)) {
      return NaN;
    }
  }

  return $$atof(number, 10);
}

function parse(src){
  return continuum.parse(src, {
    loc     : true,
    range   : true,
    raw     : true,
    comment : true,
    tokens  : true,
    tolerant: true
  });
}

function log(o){
  if (typeof o === 'string') {
    console.log(o);
  } else {
    console.log(require('util').inspect(o, null, 10));
  }
}

function SourceLocation(line, column){
  this.line = line;
  this.column = column;
}

function LineRange(start, end){
  this.start = new SourceLocation(start.line, start.column);
  this.end = new SourceLocation(end.line, end.column);
}

var lastRange = [0, 0];
var lastLoc = { start: { line: 1, column: 0 }, end: { line: 1, column: 0 } };
var out = [];
var src = '('+arguments.callee+')';


parse(src).tokens.forEach(function(token){
  var end       = token.range[0],
      start     = lastRange[1],
      line      = token.loc.start.line,
      lastLine  = lastLoc.end.line,
      rangeDiff = end - start,
      locDiff   = line - lastLine;

  if (rangeDiff) {
    out.push({
      type: 'Whitespace',
      value: src.slice(start, end),
      range: [start, end],
      loc: new LineRange(lastLoc.end, token.loc.start)
    });
  }
  out.push(token);
  lastRange = token.range;
  lastLoc = token.loc;
});

log(out.map(function(token){
  return token.value;
}).join(''));
