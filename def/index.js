"use strict";

module.exports = {
  attach: attach,
  define: define,
  is: is
};

function attach(target, name){
  return Object.defineProperty(target, name || 'define', {
    configurable: true,
    value: function(a, b){ return define(this, a, b); }
  });
}

function is(type){
  type = typeof type === 'string' ? '[object '+type+']' : toClass(type);
  return function(obj){ return toClass(obj) === type }
}


var callbind = Function.prototype.call.bind.bind(Function.prototype.call)
  , toSource = callbind(Function.prototype.toString)
  , toClass = callbind(Object.prototype.toString)
  , autohide = ['toString', 'valueOf', 'constructor', 'prototype']

var isArguments = is('Arguments')
  , isArray = Array.isArray
  , isPrimitive = function(obj){ return Object(obj) !== obj }




function defineArgs(obj, fn, params){
  if (typeof fn !== 'function') {
    throw 'fn must be a function';
  }
  var code = toSource(fn);
  var args = code.slice(code.indexOf('(')+1, code.indexOf(')')).split(/\s*,\s*/);
  args[0] === '' && args.shift();
  var k = args.length;
  while (k--) {
    if (k in params) {
      define(obj, args[k], params[k]);
    }
  }
}

function define(obj, a, b){

  if (typeof a === 'function') {

    // match passed in args to named parameters
    if (isArguments(b)) {
      defineArgs(obj, a, b);
      return obj;
    }

    // named function
    else if (a.name.length) {
      b = a;
      a = a.name;
    }
  }

  // param/argument matching when obj is a function or has a constructor
  else if (isArguments(a)) {
    var func;
    if (typeof obj === 'function') {
      func = obj;
    } else if (typeof obj.constructor === 'function') {
      func = obj.constructor;
    }
    if (func) {
      defineArgs(obj, func, a);
      return obj;
    }
  }

  else if (isArray(a)) {
    var k = a.length;

    // array of names in a, array of properties in b
    if (isArray(b) && b.length === k) {
      while (k--) {
        define(obj, a[k], b[k]);
      }
    }

    // array of named functions
    else {
      while (k--) {
        if (typeof a[k] === 'function' && a[k].name.length) {
          define(obj, a[k].name, a[k]);
        }
      }
    }
    return obj;
  }

  // dict or an object to absorb properties from
  else if (!isPrimitive(a)) {
    Object.getOwnPropertyNames(a).forEach(function(name){
      define(obj, name, Object.getOwnPropertyDescriptor(a, name));
    });
    return obj;
  }

  // empty descriptor
  var desc = Object.create(null);

  // "_name" sets non-enumerable
  // "$name" sets non-configurable
  // "_$name" does both
  desc.enumerable = a[0] !== '_';
  desc.configurable = a[desc.enumerable ? 0 : 1] !== '$';

  // chop _ and $
  a = a.substring(2 - desc.enumerable - desc.configurable);

  // hide universally non-enumerable names like 'constructor'
  if (~autohide.indexOf(a)) {
    desc.enumerable = false;
  }

  // accessor descriptor or function as value
  if (typeof b === 'function') {
    // accessor descriptor when "getName" or "setName"
    if (/^[gs]et/.test(a)) {
      desc[a.substring(0, 3)] = b;
      a = a[3].toLowerCase() + a.substring(4);
    }
    // function as value
    else desc.value = b;
  }

  // descriptor or actual value
  else if (!isPrimitive(b)) {
    // accessor descriptor
    if ('get' in b || 'set' in b) {
      if ((b.get && b.get.length === 1) || (b.set && b.set.length === 2)) {
        desc.value = { get: b.get, set: b.set };
      } else {
        desc.get = b.get;
        desc.set = b.set;
      }
    }
    // data descriptor or actual value
    else desc.value = 'value' in b ? b.value : b;
  }

  // primitive
  else desc.value = b;

  // "NAME" sets non-writable for data descriptors
  if ('value' in desc) {
    desc.writable = a !== a.toUpperCase();
  }

  // getter/setter pair with a shared private scope not exposed on the object.
  // They each take an extra parameter which will be the private shared value.

  // The extra parameters cause the property to be interpreted as a data descriptor with a
  // value of an object with get and set members. This is good because Firefox errors if
  // you try to pass accessors with extra parameters.

  if (desc.value && ((desc.value.get && desc.value.get.length === 1) ||
                     (desc.value.set && desc.value.set.length === 2))) {

    // create a private scope
    desc = (function(desc){
      // shared private
      var _hiddenShared = Object.create(null)
        , get = desc.value.get
        , set = desc.value.set;

      if (get) {
        // wrap getter to maintain the binding and pass in the private
        desc.get = function _getter(){
          // get(privateValue) where `this` is the parent object
          return get.call(this, _hiddenShared);
        };
      }

      if (set) {
        // setter semantics change to requiring the new value to be returned
        // setter gets current (private) value and the setter value
        desc.set = function _setter(newValue){
          // set(privateValue, newValue) where `this` is the parent object
          _hiddenShared = set.call(this, _hiddenShared, newValue);
        };
      }

      // errors abound if either of these are left on the descriptor
      delete desc.writable;
      delete desc.value;

      return desc;
    })(desc);
  }

  // returns obj
  return Object.defineProperty(obj, a, desc);
}