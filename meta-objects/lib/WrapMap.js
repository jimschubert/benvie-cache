if (typeof WeakMap === 'undefined') throw new Error('WeakMaps unsupported')

var WrapMap = function(O){
  "use strict";

  function lookup(o){
    if (!wrapmaps.has(o))
      throw new TypeError('WrapMaps are not generic');
    else
      return wrapmaps.get(o);
  }


  var wrapmaps = new WeakMap;

  function WrapMap(wrapper) {
    var wrapped = new WeakMap;
    var unwrapped = new WeakMap;

    if (typeof wrapper === 'function') {
      var wrap = function wrap(o, isDescriptor){
        if (isDescriptor === true)
          return wrapDescriptor(o);
        else if (Object.isPrimitive(o) || wrapped.has(o))
          return o;
        else if (unwrapped.has(o))
          return unwrapped.get(o);
        else {
          var p = wrapper(o);
          if (Object.isObject(p)) {
            wrapped.set(p, o);
            unwrapped.set(o, p);
          }
          return p;
        }
      }
    } else {
      var wrap = function wrap(o, p){
        if (Object.isPrimitive(o) || wrapped.has(o))
          return o;
        else if (unwrapped.has(o))
          return unwrapped.get(o);
        else {
          if (Object.isObject(p)) {
            wrapped.set(p, o);
            unwrapped.set(o, p);
          }
          return p;
        }
      }
    }

    function unwrap(o, isDescriptor){
      if (isDescriptor === true)
        return unwrapDescriptor(o);
      else if (Object.isPrimitive(o) || !wrapped.has(o))
        return o;
      else
        return wrapped.get(o);
    }

    function has(o){
      return Object.isObject(o) && wrapped.has(o);
    }

    function remove(o){
      var p = unwrap(o);
      if (o !== p)
        wrapped.delete(o);
      return p;
    }

    function wrapDescriptor(o){
      if (Object.isObject(o) && !wrapped.has(o)) {
        if (o.value) o.value = wrap(o.value);
        if (o.set) o.set = wrap(o.set);
        if (o.get) o.get = wrap(o.get);
      }
      return o;
    }

    function unwrapDescriptor(o){
      if (Object.isObject(o) && wrapped.has(o)) {
        if (o.value) o.value = unwrap(o.value);
        if (o.set) o.set = unwrap(o.set);
        if (o.get) o.get = unwrap(o.get);
      }
      return o;
    }


    var self = this === global ? Object.create(WrapMap.prototype) : this;
    self.wrap = wrap;
    self.unwrap = unwrap;
    self.remove = remove;
    self.has = has;

    wrapmaps.set(self, {
      wrap: wrap,
      unwrap: unwrap,
      remove: remove,
      has: has
    });

    return self;
  }

  // less efficient prototype versions that must first lookup `this`
  // allows this to work: `WrapMap.prototype.wrap.call(wrapmapInstance, o)`
  WrapMap.prototype = {
    constructor: WrapMap,
    wrap: function wrap(o, p){
      return lookup(this).wrap(o, p);
    },
    unwrap: function unwrap(o){
      return lookup(this).unwrap(o);
    },
    remove: function remove(o){
      return lookup(this).remove(o);
    },
    has: function has(o){
      return lookup(this).has(o);
    },
  };

  return WrapMap;
}(typeof O === 'undefined' ? require('./utility') : O);

if (typeof module !== 'undefined')
  module.exports = WrapMap;
