if (typeof Proxy !== 'object') throw new Error('Proxies unsupported');

var proxy = function(O){
  "use strict";

  var createProxyObject = Proxy.create,
      createProxyFunction = Proxy.createFunction;

  var wmhas = Function.callbind(WeakMap.prototype.has),
      wmget = Function.callbind(WeakMap.prototype.get),
      wmset = Function.callbind(WeakMap.prototype.set),
      wmdelete = Function.callbind(WeakMap.prototype.delete),
      wmhasget = function hasget(wm, obj){
        return (Object.isObject(obj) && wmhas(wm, obj)) ? wmget(wm, obj) : obj;
      };

  var checkArray = function(){
    var wrapped = new WeakMap,
        isArr = Array.isArray;

    Array.isArray = function isArray(){
      var a = arguments[0];
      if (isArr(a)) return true;
      if (Object.isPrimitive(a)) return false;
      return wmhas(wrapped, a);
    }

    return function(o,p){
      if (isArr(o))
        wmset(wrapped, o, true);
    }
  }();

  var checkToString = function(){
    var wrapped = new WeakMap;

    var FtoString = function toString(){
      return Function.stringify(wmhasget(wrapped, this));
    }

    var OtoString = function toString(){
      return Object.stringify(wmhasget(wrapped, this));
    }

    var DtoString = function toString(){
      return Date.stringify(wmhasget(wrapped, this));
    }


    wmset(wrapped, FtoString, Function.prototype.toString);
    wmset(wrapped, OtoString, Object.prototype.toString);
    wmset(wrapped, DtoString, Date.prototype.toString);

    Function.prototype.toString = FtoString;
    Object.prototype.toString = OtoString;
    Date.prototype.toString = DtoString;

    return function(o,p){
      wmset(wrapped, p, o);
    }
  }();


  var definer = function(valueOnly, normal){
    return function definer(o, n, v, newDesc){
      if (newDesc && !Object.isExtensible(o))
        return false;

      var desc = newDesc ? normal : valueOnly;
      desc.value = v;
      O.define(o, n, desc);
      desc.value = null;
      return true;
    }
  }({}, Descriptor.value());

  function setter(desc, rcvr, val){
    var exists = desc.set != null;
    exists && Function.calling(desc.set, rcvr, val);
    return exists;
  }

  function configurable(desc){
    if (desc)
      desc.configurable = true;
    return desc;
  }



  var trapMap = {
    getOwnPropertyDescriptor: 'describe',
    getOwnPropertyNames: 'names',
    getPropertyNames: 'names',
    defineProperty: 'define',
    delete: 'delete',
    fix: 'fix',
    keys: 'keys',
    enumerate: 'enumerate',
    hasOwn: 'owns',
    has: 'has',
    get: 'get',
    set: 'set',
    apply: 'apply',
    construct: 'construct',
  };

  var forwarder = {
    proto:     function(T    ){ return O.getProto(T) },
    describe:  function(T,N  ){ return configurable(O.describe(T,N)) },
    define:    function(T,N,D){ return O.define(T,N,D), true },
    delete:    function(T,N  ){ return delete T[N] },
    fix:       function(T    ){ return Object.freeze(T) },
    keys:      function(T    ){ return O.keys(T) },
    names:     function(T    ){ return O.names(T) },
    enumerate: function(T    ){ var i=0, a=[]; for (a[i++] in T); return a },
    owns:      function(T,N  ){ return O.has(T,N) },
    has:       function(T,N  ){ return N in T },
    get: function(T,N,R){
      var handler = wmget(proxies, T);
      if (handler != null)
        return wmget(handler, R,N);

      if (N === '__proto__')
        return O.getProto(T);

      var desc = O.describe(T,N);
      if (desc == null) {
        var proto = O.getProto(T);
        if (proto != null)
          return forwarder.get(proto, N, R);// || T[N];
      } else if (Descriptor.isValue(desc))
        return desc.value;
      else if (Descriptor.isAccessor(desc)) {
        if (typeof desc.get === 'function')
          return Function.calling(desc.get, R);
      }
      return undefined;
    },
    set: function(T,N,V,R){
      var handler = wmget(proxies, T);
      if (handler != null)
        // target is a proxy, invoke its setter
        return handler.set(R,N,V);

      if (N === '__proto__')
        return Object.isObject(V) ? (T.__proto__ = V) && true : false;

      var oDesc = O.describe(T,N);
      if (oDesc) {
        // existing own desc
        if (Descriptor.isAccessor(oDesc))
          // is an accessor
          return setter(oDesc,R,V);
        else if (!oDesc.writable)
          // is readonly
          return false;
        else
          // set it
          return definer(R,N,V, R !== T);
      } else {
        var proto = O.getProto(T);
        if (proto === null)
          // no proto
          return definer(R,N,V, true);
        else
          // recurse to proto
          return forwarder.set(proto,N,V,R);
      }
    },
    apply: function(T,A,R){
      return Function.applying(T,R,A);
    },
    construct: function(T,A) {
      var handler = wmget(proxies, T);
      if (handler != null)
        return handler.construct(T, A);

      var result = new (Function.applying(Function.bind, T, [null].concat(A)));
      return Object.isObject(result) ? result : O.create(T.prototype);
    }
  };

  var proxies = new WeakMap,
      targets = new WeakMap;

  function proxy(target, handler, callable){
    if (!Object.isObject(target))
      throw new TypeError('Target must be an object');
    if (!Object.isObject(handler))
      throw new TypeError('Handler must be an object');

    function makeFwd(args, trap){
      var fwd = function(){
        return Function.applying(forwarder[fwd.trap], handler, [fwd.target].concat(fwd.args));
      };
      fwd.target = target;
      fwd.args = args;
      fwd.trap = trap;
      return fwd;
    }

    var metaHandler = createProxyObject({
      get: function get(R, trap){
        if (trap === 'getPropertyDescriptor')
          return function(n){
            return configurable(get(null, 'getOwnPropertyDescriptor')(n) || O.describeAny(O.getProto(target)));
          };

        if (trap === 'getPropertyNames')
          return function(){
            return Array.unique(get(null, 'getOwnPropertyNames')().concat(O.namesAny(O.getProto(target))));
          };

        trap = trapMap[trap];
        var trapHandler = handler[trap];
        return function(){
          var args = Array.slice(arguments);

          if (trap === 'get' || trap === 'set') {
            args = Array.concat(args.slice(1), args[0]);
            if (args[0] === 'splice')
              return Function.applying(forwarder[trap], handler, [target].concat(args));
            if (args[0] === '__proto__')
              trapHandler = handler.proto;
          }

          if (typeof trapHandler !== 'function')
            return Function.applying(forwarder[trap], handler, [target].concat(args));
          else
            return Function.applying(trapHandler, handler, [makeFwd(args, trap), target].concat(args));
        }
      }
    });

    if (callable === true || callable == null && typeof target === 'function') {
      var reflectProxy = createProxyFunction(metaHandler,
        function(){ return metaHandler.apply(Array.slice(arguments), this) },
        function(){ return metaHandler.construct(Array.slice(arguments)) }
      );
    } else {
      var reflectProxy = createProxyObject(metaHandler, O.getProto(target));
    }

    wmset(proxies, reflectProxy, metaHandler);
    wmset(targets, reflectProxy, target);

    checkArray(target, reflectProxy);
    checkToString(target, reflectProxy);

    return reflectProxy;
  };

  return proxy;
}(typeof O === 'undefined' ? require('./utility') : O);


if (typeof module !== 'undefined')
  module.exports = proxy;
