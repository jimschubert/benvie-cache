var interceptor = function(O, proxy, WrapMap){
  "use strict";

  if (proxy === null)
    proxy = require('./proxy');
  if (WrapMap === null)
    WrapMap = require('./WrapMap');

  var store = new WrapMap;
  var wrap = store.wrap;
  var unwrap = store.unwrap;



  function listAccessors(o, p){
    var out = p || O.create(null);
    do {
      O.names(o).forEach(function(prop){
        var desc = O.describe(o, prop);
        if (desc.get || desc.set)
          out[prop] = desc;
      });
      o = O.getProto(o);
    } while (o);
    return out;
  }


  var Interceptor = function(){
    function list(fwd, target){
      return fwd().concat(this.properties);
    }
    function owns(fwd, target, prop){
      return fwd() || prop in this.accessors;
    }

    function Interceptor(name, accessors, proto){
      var brand = '[object '+name+']';
      this.accessors = accessors;
      this.prototype = proto;
      this.stringifier = Descriptor.hiddenValue(
        Object.hasOwn(proto, 'toString')
           ? proto.toString
           : function toString(){ return brand }
      );

      this.properties = O.keys(accessors);
    }

    Interceptor.keys = Object.freeze(Object.create(null));

    Interceptor.prototype = {
      getter: function(target, prop){
        return this.accessors[prop].get.call(unwrap(target));
      },
      setter: function(target, prop, value){
        return this.accessors[prop].set.call(unwrap(target), value);
      },
      names: list,
      keys: list,
      enumerate: list,
      has: owns,
      owns: owns,
      proto: function(fwd, target){
        return this.fakeproto;
      },
      describe: function(fwd, target, prop){
        if (prop === 'toString' && !Object.hasOwn(target, 'toString'))
          return this.stringifier;
        else if (prop in this.accessors) {
          var desc = O.describe(this.prototype, prop);
          return {
            enumerable: desc.enumerable,
            configurable: true,
            writable: Boolean(desc.set),
            value: desc.get.call(unwrap(target))
          };
        } else
          return fwd();
      },
      define: function(fwd, target, prop, desc){
        if (prop in this.accessors)
          return this.setter(target, prop, desc.value);
        else
          return fwd();
      },
      get: function(fwd, target, prop, rcvr){
        if (prop === 'toString' && !Object.hasOwn(target, 'toString'))
          return this.stringifier.value;
        else if (prop in this.accessors)
          return this.getter(target, prop);
        else
          return fwd();
      },
      set: function(fwd, target, prop, value, rcvr){
        if (prop in this.accessors)
          return this.setter(target, prop, value);
        else
          return fwd();
        }
    };

    return Interceptor;
  }();

  var IndexedInterceptor = function(){
    var Ceptor = Interceptor.prototype;

    function numbers(start, end){
      if (!isFinite(end)) {
        end = start;
        start = 0;
      }
      var length = end - start;
      if (end > numbers.cache.length) {
        while (length--) numbers.cache[length+start] = String(length+start);
      }
      return numbers.cache.slice(start, end);
    }

    numbers.cache = [];

    function list(fwd, target){
      return numbers(this.prototype.length.call(unwrap(target))).concat(this.properties, fwd());
    }

    function has(fwd, target, prop){
      return isFinite(prop) ? prop < this.prototype.length.call(unwrap(target)) : prop in this.accessors || fwd();
    }

    function IndexedInterceptor(name, accessors, proto){
      Interceptor.apply(this, arguments);
      this.properties.push('length');
    }

    var indexed = IndexedInterceptor.keys = Object.create(null);
    indexed.get = true;
    indexed.set = true;
    indexed.length = true;
    Object.freeze(indexed);

    IndexedInterceptor.prototype = {
      __proto__: Ceptor,
      names: list,
      keys: list,
      enumerate: list,
      has: has,
      owns: has,
      describe: function(fwd, target, prop){
        if (isFinite(prop) && this.prototype.get)
          return Descriptor.value(this.prototype.get.call(unwrap(target), prop));
        else if (prop === 'length')
          return Descriptor.hiddenValue(this.prototype.length.call(unwrap(target)), true);
        else
          return Ceptor.describe.apply(this, arguments);
      },
      define: function(fwd, target, prop, desc){
        if (isFinite(prop) && this.prototype.set)
          return this.prototype.set.call(unwrap(target), prop, value);
        else
          return Ceptor.define.apply(this, arguments);
      },
      get: function(fwd, target, prop, rcvr){
        if (isFinite(prop) && this.prototype.get)
          return this.prototype.get.call(unwrap(target), prop);
        else if (prop === 'length')
          return this.prototype.length.call(unwrap(target));
        else
          return Ceptor.get.apply(this, arguments);
      },
      set: function(fwd, target, prop, value, rcvr){
        if (isFinite(prop) && this.prototype.set)
          return this.prototype.set.call(unwrap(target), prop, value);
        else
          return Ceptor.set.apply(this, arguments);
      },
    };

    return IndexedInterceptor;
  }();

  var NamedInterceptor = function(){
    var Ceptor = Interceptor.prototype;

    function list(fwd, target){
      return this.prototype.list.call(unwrap(target)).concat(this.properties, fwd());
    }

    function has(fwd, target, prop){
      return fwd() || this.prototype.has.call(unwrap(target), prop);
    }

    function NamedInterceptor(name, accessors, proto){
      Interceptor.apply(this, arguments);
    }

    var named = NamedInterceptor.keys = Object.create(null);
    named.get = true;
    named.set = true;
    named.has = true;
    named.list = true;
    Object.freeze(named);

    NamedInterceptor.prototype = {
      __proto__: Ceptor,
      names: list,
      keys: list,
      enumerate: list,
      has: has,
      owns: has,
      describe: function(fwd, target, prop){
        var inst = unwrap(target);
        if (this.prototype.has.call(inst, prop))
          return Descriptor.value(this.prototype.get.call(inst, prop));
        else if (prop in this.accessors)
          return Ceptor.describe.apply(this, arguments);
        else if (prop in this.fakeproto)
          return fwd();
        else
          return Descriptor.value(this.prototype.get.call(unwrap(target), prop));
      },
      define: function(fwd, target, prop, desc){
        var inst = unwrap(target);
        if (this.prototype.has.call(inst, prop))
          return this.prototype.set.call(inst, prop, desc.value);
        else if (prop in this.accessors)
          return this.setter(target, prop, desc.value);
        else
          return this.prototype.set.call(unwrap(target), prop, value);
      },
      get: function(fwd, target, prop, rcvr){
        var inst = unwrap(target);
        if (this.prototype.has.call(inst, prop))
          return this.prototype.get.call(inst, prop);
        else if (prop in this.accessors)
          return this.getter(target, prop);
        else
          return fwd();
      },
      set: function(fwd, target, prop, value, rcvr){
        var inst = unwrap(target);
        if (this.prototype.has.call(inst, prop))
          return this.prototype.set.call(inst, prop, value);
        else if (prop in this.accessors)
          return this.setter(target, prop, value);
        else
          return this.prototype.set.call(unwrap(target), prop, value);
      },
    };

    return NamedInterceptor;
  }();


  var Constructor = function(){
    var stringifier = Descriptor.hiddenValue(function toString(){
      return 'function '+this.name+'() { [native code] }';
    });

    function Constructor(name, ctor, template){
      this.template = template;
      this.ctor = ctor;
      return proxy(new Function('return function '+name+'(){}')(), this);
    }

    Constructor.prototype = {
      describe: function(fwd, target, prop){
        if (prop === 'prototype')
          return Descriptor.hiddenValue(this.template.fakeproto);
        else if (prop === 'toString')
          return stringifier;
        else
          return fwd();
      },
      get: function(fwd, target, prop, rcvr){
        if (prop === 'prototype')
          return this.template.fakeproto;
        else if (prop === 'toString')
          return stringifier.value;
        else
          return fwd();
      },
      apply: function(fwd, target, args, rcvr){
        var inst = Object.create(this.template.fakeproto);
        var out = proxy(inst, this.template);
        wrap(out, inst);
        this.ctor.apply(out, args);
        return out;
      },
      construct: function(fwd, target, args){
        var inst = Object.create(this.template.fakeproto);
        var out = proxy(inst, this.template);
        wrap(out, inst);
        this.ctor.apply(out, args);
        return out;
      }
    };

    return Constructor;
  }();

  var protos = new WeakMap;



  function interceptor(name, Ctor, inherits){
    if (typeof name === 'function') {
      inherits = Ctor;
      Ctor = name;
      name = Ctor.name;
    }

    var proto = Ctor.prototype;
    var accessors = listAccessors(proto);
    if (inherits)
      listAccessors(protos.get(inherits), accessors);
    var Template = 'length' in proto ? IndexedInterceptor : 'list' in proto ? NamedInterceptor : Interceptor;
    var template = new Template(name, accessors, proto);
    var ctor = new Constructor(name, Ctor, template);

    template.fakeproto = O.keys(proto).reduce(function(ret, key){
      if (!(key in accessors || key in Template.keys)) {
        var desc = O.describe(proto, key);
        if (key === 'constructor')
          desc.value = ctor;
        desc.enumerable = false;
        O.define(ret, key, desc);
      }
      return ret;
    }, O.create(O.getProto(proto)));

    protos.set(template.fakeproto, proto);
    return ctor;
  }



  // var _ = function(){
  //   var bags = new WeakMap;
  //   var _ = Descriptor.hiddenValue;

  //   function Namespace(){}
  //   var N = Namespace.prototype = O({
  //     set: _(function set(key, value){
  //       if (Object.isObject(key)) {
  //         value = key;
  //         O.keys(key).forEach(function(key){
  //           this[key] = value[key];
  //         }, this);
  //       } else {
  //         this[key] = value;
  //       }
  //     }),
  //     toString: _(function toString(){
  //       return '[object Namespace]';
  //     }),
  //     valueOf: _(function valueOf(){
  //       return this;
  //     })
  //   });

  //   return function privates(obj, values){
  //     var bag = bags.has(obj) ? bags.get(obj) : bags.set(obj, new Namespace);
  //     if (values !== undefined)
  //       bag.set(values);
  //     return bag;
  //   }
  // }();


  return interceptor;
}(typeof O === 'undefined' ? require('./utility') : O, typeof proxy === 'undefined' ? null : proxy, typeof WrapMap === 'undefined' ? null : WrapMap);

if (typeof module !== 'undefined')
  module.exports = interceptor;
