if (typeof WeakMap === 'undefined') throw new Error('WeakMaps unsupported');

var WeakSet = function(O, global){
  "use strict";

  var weaksets = new WeakMap;

  function lookup(o, p){
    if (!weaksets.has(o))
      throw new TypeError('WeakSets are not generic');
    return weaksets.get(o)[+Object.isObject(p)];
  }

  function WeakSet(){
    var self = WeakSet.prototype.isPrototypeOf(this) ? this : Object.create(WeakSet.prototype);
    weaksets.set(self, [new Map, new WeakMap]);
    return self;
  }

  WeakSet.prototype = {
    constructor: WeakSet,
    add: function add(o){
      lookup(this, o).set(o, true);
      return o;
    },
    has: function has(o){
      return lookup(this, o).has(o);
    },
    delete: function delet(o){
      lookup(this, o).delete(o);
    }
  };

  return WeakSet;
}(typeof O === 'undefined' ? require('./utility') : O, new Function('return this')());
