if (typeof module !== 'undefined' && module.O)
  return module.exports = module.O;

var FP = Function.prototype,
    OP = Object.prototype,
    AP = Array.prototype,

    bindbind = FP.bind.bind(FP.bind),
    callbind = bindbind(FP.call),
    applybind = bindbind(FP.apply),
    calling = callbind(FP.call),
    binding = callbind(FP.bind),
    applying = callbind(FP.apply),

    hasOwn = callbind(OP.hasOwnProperty),

    flatten = applybind(AP.concat, []),
    concat = callbind(AP.concat),
    map = callbind(AP.map),

    define = Object.defineProperty,
    defines = Object.defineProperties,
    describe = Object.getOwnPropertyDescriptor,
    describeAny = getPropertyDescriptor,
    names = Object.getOwnPropertyNames,
    namesAny = getPropertyNames,
    keys = Object.keys,
    getProto = Object.getPrototypeOf,
    create = Object.create;

var types = [ Array, Boolean, Date, Function, Map, Number, Object, RegExp, Set, String, WeakMap ];


function Descriptor(type, valueOrGet, readonlyOrSet, hidden, frozen){
  this[type ? 'setAccessors' : 'setValue'](valueOrGet, readonlyOrSet);
  if (hidden)
    this.enumerable = false;
  if (frozen)
    this.configurable = false;
}

Descriptor.VALUE = 0;
Descriptor.ACCESSOR = 1;

Descriptor.prototype = {
  constructor: Descriptor,
  configurable: true,
  enumerable: true,
  setValue: function setValue(value, readonly){
    this.value = value;
    this.writable = !readonly;
  },
  setAccessors: function setAccessors(get, set){
    this.get = get;
    this.set = set;
  }
};


function value(v, h, r, f){
  return new Descriptor(0, v, r, h, f);
}

function hiddenValue(v, r, f){
  return new Descriptor(0, v, r, true, f);
}

function accessor(g, s, h, f){
  return new Descriptor(1, g, s, h, f);
}

function hiddenAccessor(g, s, f){
  return new Descriptor(1, g, s, true, f);
}

function isAccessor(desc){
  return isObject(desc) && ('get' in desc || 'set' in desc) && !('value' in desc);
}

function isValue(desc){
  return isObject(desc) && !('get' in desc || 'set' in desc) && 'value' in desc;
}


function merge(to, from){
  if (~types.indexOf(to) || keys(to).length === 0 && names(to) > 0)
    var desc = hiddenValue();
  else
    var desc = value();

  keys(from).forEach(function(key){
    desc.value = from[key];
    define(to, key, desc);
  });
  return to;
}

function isObject(o){
  return Object(o) === o;
}

function isPrimitive(o){
   return Object(o) !== o;
}

function getPropertyDescriptor(o,n){
  while (isObject(o)) {
    var desc = describe(o,n);
    if (desc)
      return desc;
    o = getProto(o);
  }
  return undefined;
}

function getPropertyNames(o){
  var out = [];
  while (isObject(o)) {
    out.push(names(o));
    o = getProto(o);
  }
  return unique(flatten(out));
}

function parameters(fn){
  return (fn+='').slice(fn.indexOf('(') + 1, fn.indexOf(')')).split(/\s*,\s*/);
}

function unique(a){
  return keys(a.reduce(function(r,s){ r[s] = 1; return r }, {}));
}

var slice = function(){
  var _slice = [].slice;

  return function slice(a,o,p){
    switch (a.length) {
             case  0: return [];
             case  1: return o ? [] : [a[0]];
            default: return _slice.call(a,o,p);
             case  2: a = [a[0],a[1]];
      break; case  3: a = [a[0],a[1],a[2]];
      break; case  4: a = [a[0],a[1],a[2],a[3]];
      break; case  5: a = [a[0],a[1],a[2],a[3],a[4]];
      break; case  6: a = [a[0],a[1],a[2],a[3],a[4],a[5]];
      break; case  7: a = [a[0],a[1],a[2],a[3],a[4],a[5],a[6]];
      break; case  8: a = [a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7]];
      break; case  9: a = [a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8]];
      break; case 10: a = [a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9]];
      break; case 11: a = [a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10]];
      break; case 12: a = [a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11]];
      break; case 13: a = [a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12]];
      break; case 14: a = [a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13]];
    }
    return (o || p) ? a.slice(o,p) : a;
  }
}();



var brandname = function(){
  var brands = {};

  types.forEach(function(Ctor){
    if (hasOwn(Ctor.prototype, 'toString'))
      define(Ctor, 'stringify', hiddenValue(callbind(Ctor.prototype.toString)));
    brands['[object '+Ctor.name+']'] = Ctor.prototype;
  });

  return function brandname(o){
    var brand = Object.stringify(o);
    return brand in brands ? brands[brand] : brand;
  }
}();


define(global, 'Descriptor', hiddenValue(Descriptor));

merge(Descriptor, {
  value: value,
  hiddenValue: hiddenValue,
  accessor: accessor,
  hiddenAccessor: hiddenAccessor,
  isValue: isValue,
  isAccessor: isAccessor
});

merge(Object, {
  isObject: isObject,
  isPrimitive: isPrimitive,
  getPropertyDescriptor: getPropertyDescriptor,
  getPropertyNames: getPropertyNames,
  hasOwn: hasOwn,
  merge: merge,
  brandname: brandname
});

merge(Function, {
  parameters: parameters,
  bindbind: bindbind,
  callbind: callbind,
  applybind: applybind,
  calling: calling,
  binding: binding,
  applying: applying
});

merge(Array, {
  unique: unique,
  slice: slice,
  flatten: flatten,
  concat: concat,
  map: map
});


function O(v){
  var out = Object.create(null);
  if (isObject(v))
    merge(out, v);
  return out;
}

Object.merge(O, {
  isobj: Object.isObject,
  notobj: Object.isPrimitive,
  create: Object.create,
  define: Object.defineProperty,
  defines: Object.defineProperties,
  getProto: Object.getPrototypeOf,
  describeAny: Object.getPropertyDescriptor,
  describe: Object.getOwnPropertyDescriptor,
  namesAny: Object.getPropertyNames,
  names: Object.getOwnPropertyNames,
  keys: Object.keys,
  has: Object.hasOwn,
});


if (typeof module !== 'undefined')
  module.O = module.exports = O;