var doppelganger = function(O, proxy){
  "use strict";

  if (proxy === null)
    proxy = require('./proxy');


  function doppelganger(callable){
    var handler = new Doppelganger;
    var doppel = proxy(handler.target, handler, callable);

    return function changeInfo(into){
      handler.target = Object(into);
      if (doppel)
        return [doppel, doppel = null][0];
    }
  }

  function Doppelganger(){
    this.target = {};
  }

  function targeter(fwd){
    fwd.target = this.target;
    return fwd();
  }

  Doppelganger.prototype = {
    proto: targeter,
    names: targeter,
    describe: targeter,
    define: targeter,
    delete: targeter,
    fix: targeter,
    keys: targeter,
    enumerate: targeter,
    owns: targeter,
    has: targeter,
    get: targeter,
    set: targeter,
    apply: targeter,
    construct: targeter,
  };

  return doppelganger;
}(typeof O === 'undefined' ? require('./utility') : O, typeof proxy === 'undefined' ? null : proxy);

if (typeof module !== 'undefined')
  module.exports = doppelganger;
