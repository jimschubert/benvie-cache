var multiherit = function(O, proxy){
  "use strict";

  if (proxy === null)
    proxy = require('./proxy');

  function multiherit(o){
    o.params = o.params || [];
    var arglist = o.ctors.map(function(ctor, i){
      if (Array.isArray(o.params[i])) {
        return o.params[i]
      } else {
        return parameters(ctor).map(function(param){
          return o.params.indexOf(param);
        });
      }
    });

    var protos = o.ctors.map(function(ctor){
      return ctor.prototype;
    });

    return new MultiCtor(new Multiproto(protos), arglist, o);
  }




  function MultiCtor(proto, params, o){
    var ctors = o.ctors;
    function Ctor(){
      var args = arguments;
      for (var i=0; i < ctors.length; i++) {
        apply(ctors[i], this, params[i].map(function(i){ return args[i] }));
      }
      return this;
    }
    Ctor.prototype = proto;
    proto.constructor = Ctor;
    this.name = o.name || ctors.map(function(ctor){ return ctor.name }).join('');
    this.createInstance = instanceCreator(proto, o.onCall, o.onConstruct);
    return proxy(Ctor, this);
  }

  function toString(){ return 'function '+this.name+'() { [native code] }' }

  MultiCtor.prototype = {
    get: function(fwd, target, name, rcvr){
      if (name === 'name') {
        return this.name;
      } else if (name === 'toString') {
        return toString;
      } else {
        return fwd();
      }
    },
    describe: function(fwd, target, name){
      if (name === 'name') {
        var desc = fwd();
        desc.value = this.name;
        return desc;
      } else {
        return fwd();
      }
    },
    call: function(fwd, target, args, rcvr){
      return this.construct(fwd, target, args);
    },
    construct: function(fwd, target, args){
      return apply(target, this.createInstance(), args);
    }
  };

  function instanceCreator(proto, call, construct){
    if (call || construct) {
      var handler = {
        apply: function(fwd, target, args, rcvr){
          return apply(call, rcvr, args);
        },
        construct: function(fwd, target, args){
          if (construct) {
            return apply(construct, Object.create(target.prototype), args);
          } else {
            return apply(call, global, args);
          }
        }
      };
      return function(){
        var fake = function(){};
        fake.__proto__ = proto;
        return proxy(fake, handler);
      }
    } else {
      return function(){
        return Object.create(proto);
      }
    }
  }

  function Multiproto(protos){
    this.protos = protos = Object.freeze(protos.slice());
    var proto = Object.create(null, { inherits: { configurable: true, value: protos } });
    return proxy(proto, this);
  }

  Multiproto.prototype = function(){
    function list(fwd, target){
      return unique(this.protos.reduce(function(ret, proto){
        fwd.target = proto;
        return ret.concat(fwd());
      }, fwd())).filter(function(s){ return !(s in Object.prototype) });
    }

    function has(fwd, target, name){
      if (fwd()) return true;
      for (var i=0; i < this.protos.length; i++) {
        fwd.target = this.protos[i];
        if (fwd()) return true;
      }
      return false;
    }

    return {
      names: list,
      keys: list,
      enumerate: list,
      hasOwn: has,
      has: has,
      describe: function(fwd, target, name){
        var desc = fwd();
        if (desc) return desc;
        for (var i=0; i < this.protos.length; i++) {
          fwd.target = this.protos[i];
          desc = fwd();
          if (desc)
            return desc;
        }
        return undefined;
      },
      get: function(fwd, target, name, rcvr){
        var ret, i=0;
        while (typeof ret === 'undefined' && fwd.target) {
          ret = fwd();
          fwd.target = this.protos[i++];
        }
        return ret;
      }
    };
  }();


  return multiherit;
}(typeof O === 'undefined' ? require('./utility') : O, typeof proxy === 'undefined' ? null : proxy);

if (typeof module !== 'undefined')
  module.exports = multiherit;
