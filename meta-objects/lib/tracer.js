var tracer = function(O, proxy, membrane, Emitter){
  "use strict";

  if (proxy === null)
    proxy = require('./proxy');
  if (membrane === null)
    membrane = require('./membrane');
  if (Emitter === null)
    Emitter = require('./Emitter');



  var builtins = new WeakMap;
  var ids = new WeakMap;
  ids.id = 0;
  function tag(obj){
    return ids.has(obj) ? ids.get(obj) : ids.set(obj, ids.id++);
  }

  // function initBuiltins(){

  //   function addAll(obj, path){
  //     tag(obj);
  //     Object.getOwnPropertyNames(obj).forEach(function(name){
  //       if (name === 'arguments' || name === 'caller' || name ==='callee')
  //         return;
  //       var val = obj[name];
  //       if (Object.isObject(val) && !builtins.has(val))
  //         addAll(val, builtins.set(val, (path || []).concat(name)));
  //     });
  //   }

  //   [Error, EvalError, RangeError, ReferenceError, SyntaxError, TypeError, URIError,
  //     Array, Boolean, Date, Function, Map, Number, Object, Proxy, Set, String, WeakMap, Math, JSON,
  //     decodeURI, decodeURIComponent, encodeURI, encodeURIComponent, escape, eval,
  //     isFinite, isNaN, parseFloat, parseInt, unescape
  //   ].forEach(function(builtin){
  //     tag(builtin);
  //     var path = ['@'+(builtin.name || (builtin+'').slice(8, -1))];
  //     builtins.set(builtin, path);
  //     addAll(builtin, path);
  //   });

  //   ['document', 'navigator', 'location'].forEach(function(name){
  //     if (Object.isObject(global[name])) {
  //       tag(global[name]);
  //       builtins.set(global[name], ['@'+name]);
  //     }
  //   });
  // }

  //initBuiltins();

  function TraceEmitter(object, name){
    if (!(this instanceof TraceEmitter))
      return new TraceEmitter(object, name);

    var self = this;

    var names = new WeakMap;
    var instances = new WeakMap;
    name = name || 'root';
    names.set(object, [name]);

    Emitter.call(this);

    var managers = {
      get: function(target, prop, path, ret){
        if (Object.isObject(ret) && !names.has(ret))
          names.set(ret, path.concat(prop));
      },
      apply: function(target, args, path, ret){
        if (Object.isObject(ret) && !names.has(ret))
          names.set(ret, [path[path.length-1], '%']);
      },
      describe: function(target, prop, path, ret){
        if (Object.isObject(ret.value))
          names.set(ret.value, path.concat(prop));
        if (Object.isObject(ret.get))
          names.set(ret.get, path.concat(prop+'[getter]'));
        if (Object.isObject(ret.set))
          names.set(ret.set, path.concat(prop+'[setter]'));
      },
      construct: function(target, prop, path, ret){
        var num = instances.has(target) ? instances.get(target) + 1 : 0;
        instances.set(target, num);
        names.set(ret, (path || []).concat('^'+num));
      }
    };

    var wrapper = membrane(proxy({}, {
      get: function get(f,t,trap){
        return function(fwd, target, prop){
          var ret = fwd();
          var path = builtins.has(target) ? builtins.get(target) : names.get(target);
          var args = fwd.args;

          if (managers[trap])
            managers[trap](target, prop, path, ret);

          self.emit(new TraceEvent(trap, target, args, ret, path));
          return ret;
        }
      }
    }));

    this.unwrap = wrapper.unwrap;
    this[name] = wrapper.wrap(object);
  }

  TraceEmitter.prototype = Object.create(Emitter.prototype);
  TraceEmitter.prototype.constructor = TraceEmitter;

  function TraceEvent(type, target, args, result, path){
    Emitter.Event.apply(this, arguments);
    this.id = tag(target);
    this.result = result;
    this.path = path;
    if (type === 'construct' || type === 'apply') {
      this.args = args[0];
    } else if (type !== 'keys' && type !== 'names' && type !== 'enumerate' && type !== 'fix') {
      this.property = args[0];
    }
    if (type === 'define' || type === 'set') {
      this.value = args[1];
    }
    if (type === 'get' || type === 'apply') {
      this.receiver = args[1];
    } else if (type ===  'set') {
      this.receiver = args[2];
    }
  }

  TraceEvent.prototype = Object.create(Emitter.Event.prototype);
  TraceEvent.prototype.constructor = TraceEvent;


  function tracer(object, name){
    return new TraceEmitter(object, name);
  }

  return tracer;
}(typeof O === 'undefined' ? require('./utility') : O, typeof proxy === 'undefined' ? null : proxy, typeof membrane === 'undefined' ? null : membrane, typeof Emitter === 'undefined' ? null : Emitter);

if (typeof module !== 'undefined')
  module.exports = tracer;
