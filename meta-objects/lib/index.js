module.exports = {
  proxy: require('./proxy'),
  membrane: require('./membrane'),
  multiherit: require('./multiherit'),
  tracer: require('./tracer'),
  callable: require('./callable'),
  interceptor: require('./interceptor'),
  doppelganger: require('./doppelganger'),
  WrapMap: require('./WrapMap'),
  Emitter: require('./Emitter'),
};
