var membrane = function(O, proxy, WrapMap){
  "use strict";

  if (proxy === null)
    proxy = require('./proxy');
  if (WrapMap === null)
    WrapMap = require('./WrapMap');


  function membrane(handlers){
    if (Object.isPrimitive(handlers)) throw new Error('Handlers must be provided');

    var wrapper = new WrapMap(function(target){
      //var protoCeptor = typeof target === 'function' ? function(){} : Object.create(wrapper.wrap(Object.getPrototypeOf(target)));
      return proxy(target, proxy({}, {
        get: function(f, t, trap){
          return function(fwd, faketarget){
            var handler = handlers[trap];
            var args = Array.slice(arguments);
            args[1] = fwd.target = wrapper.unwrap(target);
            if (handler) {
              var origfwd = fwd;
              fwd = function fwd(){ return Function.applying(handler, handlers, args); }
              fwd.args = origfwd.args;
            }
            return Function.applying(wrapHandler[trap], null, Array.concat([fwd], Array.slice(args, 1)));
          }
        }
      }));
    });


    function forward(fwd){ return fwd() }

    var wrapHandler = {
      names: forward,
      enumerate: forward,
      keys: forward,
      delete: forward,
      has: forward,
      owns: forward,
      proto: function(fwd, target){
        return wrapper.wrap(fwd());
      },
      fix: function(fwd, target){
        throw target;
      },
      define: function(fwd, target, name, desc){
        wrapper.unwrap(desc, true);
        return fwd();
      },
      describe: function(fwd, target, name){
        return wrapper.wrap(fwd(), true);
      },
      get: function(fwd, target, name, rcvr){
        return wrapper.wrap(fwd());
      },
      set: function(fwd, target, name, val, rcvr){
        fwd.args[1] = wrapper.unwrap(val);
        fwd.args[2] = wrapper.unwrap(rcvr);
        return fwd();
      },
      apply: function(fwd, target, args, rcvr){
        fwd.args[0] = Array.map(args, wrapper.unwrap);
        fwd.args[1] = wrapper.unwrap(rcvr);
        return wrapper.wrap(fwd());
      },
      construct: function(fwd, target, args){
        fwd.args[0] = Array.map(args, wrapper.unwrap);
        return wrapper.wrap(fwd());
      }
    };

    return wrapper;
  }

  return membrane;
}(typeof O === 'undefined' ? require('./utility') : O, typeof proxy === 'undefined' ? null : proxy, typeof WrapMap === 'undefined' ? null : WrapMap);


if (typeof module !== 'undefined')
  module.exports = membrane;
