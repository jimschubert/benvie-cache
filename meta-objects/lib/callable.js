var callable = function(O, proxy){
  "use strict";

  if (proxy === null)
    proxy = require('./proxy');

  function callable(obj){
    return proxy(obj, handler, true);
  }

  function list(fwd){
    return fwd().filter(function(s){ return s !== '$$call' && s !== '$$construct' && !hasOwn(s) });
  }

  var handler = {
    enumerate: list,
    keys: list,
    names: list,
    apply: function(fwd, target, args, rcvr){
      var call = target.$$call;
      if (call) {
        return apply(call, target, args);
      }
    },
    construct: function(fwd, target, args){
      var construct = target.$$construct;
      var instance = proxy(create(target), handler, true);
      if (construct) {
        var result = apply(construct, instance, args);
        return isObject(result) ? result : instance;
      } else{
        return instance;
      }
    }
  };

  return callable;
}(typeof O === 'undefined' ? require('./utility') : O, typeof proxy === 'undefined' ? null : proxy);

if (typeof module !== 'undefined')
  module.exports = callable;