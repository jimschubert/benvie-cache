
  var Namespace = function(){
    var namespaces = new WeakMap;

    function unwrap(namespace, obj){
      namespace = namespaces.get(namespace);
      return namespace.has(obj) ? namespace.get(obj) : namespace.set(obj, Object.create(null));
    }

    function Namespace(){
      namespaces.set(this, new WeakMap;)
    }

    Namespace.prototype = {
      constructor: Namespace,
      set: function set(obj, key, value){
        var store = unwrap(this, obj);
        if (isObject(key)) {
          value = key;
          Object.keys(key).forEach(function(key){
            store[key] = value[key];
          });
        } else {
          store[key] = value;
        }
      },
      get: function get(obj, key){
        var store = unwrap(this, obj);
        if (Array.isArray(key)) {
          return key.map(function(key){
            return store[key];
          });
        } else if (isObject(key)) {
          var value = key;
          Object.keys(key).forEach(function(key){
            value[key] = store[key];
          });
          return value;
        } else {
          return store[key];
        }
      },
      setter: function setter(obj){
        return this.set.bind(this, obj);
      },
      getter: function getter(obj){
        return this.get.bind(this, obj);
      }
    };

    return Namespace;
  }();