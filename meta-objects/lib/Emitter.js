var Emitter = function(O){
  "use strict";

  // ### Generic Event ###

  function Event(type, target){
    this.type = type;
    this.target = target;
  }

  function ErrorEvent(error, event, target){
    Event.call(this, 'error', target)
    this.error = error;
    this.event = event;
  }


  var emitters = new WeakMap;
  var receivers = new WeakMap;


  function _(o){
    if (!emitters.has(o)) {
      receivers.set(o, o);
      return emitters.set(o, {});
    } else {
      return emitters.get(o);
    }
  }

  // ##############################################
  // ### Prototype and initializer for emitters ###
  // ##############################################

  function Emitter(){
    emitters.set(this, {});
    receivers.set(this, this);
  }

  Emitter.Event = Event;

  // forward on event subscriptions from one object to another
  Emitter.forward = function forward(from, to){
    if (!emitters.has(to)) {
      emitters.set(to, {});
      receivers.set(to, to);
    }
    emitters.set(from, emitters.get(to));
    from.on = Emitter.prototype.on.bind(to);
    from.off = Emitter.prototype.off.bind(to);
  }


  // standardish emitter that is able to be ignorant of QT needs
  Emitter.prototype = {
    constructor: Emitter,
    on: function on(events, listener){
      var listeners = _(this);
      events.split(' ').forEach(function(event){
        if (event in listeners){
          listeners[event].push(listener);
        } else {
          listeners[event] = [listener];
        }
      });
    },
    off: function off(events, listener){
      var listeners = _(this);
      events.split(' ').forEach(function(event){
        if (listeners[event]) {
          listeners[event].splice(listeners[event].indexOf(listener), 1);
        }
      });
    },
    offAll: function offAll(event){
      delete _(this)[event];
    },
    once: function once(event, listener){
      var self = this;
      this.on(event, function(){
        self.off(event, listener);
        return listener.apply(receivers.get(self), arguments);
      });
    },
    isListened: function isListened(type){
      var events = _(this);
      var listeners = events[type] || events['*'];
      return Boolean(listeners && listeners.length);
    },
    emit: function emit(type){
      var event, events = _(this);
      if (typeof type !== 'string') {
        event = type;
        type = event.type;
      }
      if (events['*']) {
        var listeners = events[type] ? events['*'].concat(events[type]) : events['*'];
      } else {
        var listeners = events[type];
      }
      if (listeners && listeners.length) {
        event = event || new Event(type, receivers.get(this));
        var args = [event].concat(Array.slice(arguments, 1));
        for (var i=0; i < listeners.length; i++) {
          try {
            listeners[i].apply(this, args);
          } catch (e) {
            this.emit(new ErrorEvent(e, event, listeners[i]));
          }
        }
      }
    }
  };


  return Emitter;
}(typeof O === 'undefined' ? require('./utility') : O);

if (typeof module !== 'undefined')
  module.exports = Emitter;
