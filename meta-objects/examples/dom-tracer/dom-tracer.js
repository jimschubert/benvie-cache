!function(global){
  "use strict";
  var tracer = meta.tracer;
  window.meta = null;

  var loadJS = function(){
    var loader = new XHR('', function(result){ callback(result); });
    var callback;

    return function loadJS(name, cb){
      callback = cb;
      loader.exec(name);
    }
  }();

  function shadow(code){
    var fake = tracer(global, 'window');
    var globals = Object.getOwnPropertyNames(global).filter(function(name){
      return /^[\w_$]*$/.test(name);
    });
    var fakes = globals.map(function(name){ return fake.window[name] });

    if (typeof code === 'function')
      code = '(' + code + ')()';

    globals.push('\n'+code+'\n');
    var compiled = Function.apply(null, globals);

    delete fake.window;
    fake.run = function run(){
      return compiled.apply(global, fakes);
    };

    return fake;
  }


  function trace(filename, callback){
    loadJS(filename, function(src){
      var emitter = shadow(src);
      emitter.on('*', callback);
      emitter.run();
    });
  }

  var skip = {
    get: true,
    describe: true,
    has: true,
    owns: true,
    describer: true,
    enumerate: true,
    keys: true,
    //apply: true,
    names: true,
    proto: true
  };

  // trace('jquery-1.7.2.min.js', function(e){
  //   if (e.trap === 'set' && e.property === 'innerHTML')
  //     return;

  //   if (e.type in skip)
  //     return;

  //   var log = [e.type, e.id, e.path.join('.')];

  //   if (e.property)
  //     log.push(e.property);

  //   if (e.type === 'apply' || e.type === 'construct')
  //     log.push(e.args)//.map(function(arg){
  //       //return typeof arg === 'function' ? arg.name || 'anonymous' : arg;
  //     }));
  //     log.push(e.result);

  //   if (e.target.nodeType)
  //     log.push(dompath(e.target));


  //   console.log.apply(console, log);
  // });

  trace('jquery-1.7.2.min.js', function(e){
    if (e.type === 'set' || e.type === 'get') {
      e.receiver = function(rcvr){ return function(){ return rcvr } }(e.receiver);
      console.log(e);
    }
  });

  function dompath(node){
    var path = [];
    do {
      path.push(NODETYPES[node.nodeType](node));
    } while (node !== document && (node = node.parentNode));

    return path.reverse();
  }

  var NODETYPES = [
    function Unknown(o){},
    function Element(o){
      var text = o.localName;
      if (o.id)
        text += '#' + o.id;
      else if (o.hasAttribute('class'))
        text += '.' + o.getAttribute('class');
      return text;
    },
    function Attribute(o){
      return '^attr(' + o.localName + ')';
    },
    function Text(o){
      return '^text('+o.textContent.length+')';
    },
    function CdataSection(o){},
    function EntityReference(o){},
    function Entity(o){},
    function ProcessingInstruction(o){},
    function Comment(o){
      return '^comment('+o.textContent.length+')';
    },
    function Document(o){
      return '^document';
    },
    function DocumentType(o){
      return '@'+o.name;
    },
    function DocumentFragment(o){
      return '^fragment'
    },
    function Notation(o){},
  ];
}(new Function('return this')());

