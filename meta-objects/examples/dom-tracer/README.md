# DOM Tracer

Loads jQuery (unmodified) onto a membrane DOM and logs what it does. 

__Example jQuery startup Output__

```javascript
get       | getElementsByTagName.%  length
get       | @document createElement ["^document"]
apply     | @document.createElement ["select"]
get       | createElement.% appendChild ["select"]
get       | @document createElement ["^document"]
apply     | @document.createElement ["option"]
apply     | createElement.%.appendChild [option]
get       | createElement.%  getElementsByTagName ["div"]
apply     | createElement.%.getElementsByTagName ["input"]
get       | getElementsByTagName.% 0
get       | createElement.% firstChild ["div"]
get       | createElement.%.firstChild nodeType ["div", "^text(3)"]
get       | createElement.%  getElementsByTagName ["div"]
apply     | createElement.%.getElementsByTagName ["tbody"]
get       | getElementsByTagName.% length
get       | createElement.%  getElementsByTagName ["div"]
apply     | createElement.%.getElementsByTagName ["link"]
get       | getElementsByTagName.% length
get       | getElementsByTagName.%.0 getAttribute ["div", "a"]
apply     | getElementsByTagName.%.0.getAttribute ["style"]
get       | getElementsByTagName.%.0 getAttribute ["div", "a"]
apply     | getElementsByTagName.%.0.getAttribute ["href"]
get       | getElementsByTagName.%.0 style ["div", "a"]
get       | getElementsByTagName.%.0.styleopacity
get       | getElementsByTagName.%.0 style ["div", "a"]
get       | getElementsByTagName.%.0.stylecssFloat
get       | getElementsByTagName.%.0 value ["div", "input"]
get       | createElement.% selected ["select", "option"]
get       | createElement.% className ["div"]
get       | @document createElement ["^document"]
apply     | @document.createElement ["form"]
get       | createElement.% enctype ["form"]
get       | @document createElement ["^document"]
apply     | @document.createElement ["nav"]
get       | createElement.% cloneNode ["nav"]
apply     | createElement.%.cloneNode [true]
get       | cloneNode.% outerHTML ["nav"]
get       | @documentcompatMode ["^document"]
set       | getElementsByTagName.%.0 checked ["div", "input"]
get       | getElementsByTagName.%.0 cloneNode ["div", "input"]
apply     | getElementsByTagName.%.0.cloneNode [true]
get       | cloneNode.% checked ["input"]
set       | createElement.% disabled ["select"]
get       | createElement.% disabled ["select", "option"]
delete    | createElement.% test ["div"]
get       | createElement.% addEventListener ["div"]
get       | @document createElement ["^document"]
apply     | @document.createElement ["input"]
set       | createElement.% value ["input"]
get       | createElement.% setAttribute ["input"]
apply     | createElement.%.setAttribute ["type", "radio"]
get       | createElement.% value ["input"]
get       | createElement.% setAttribute ["input"]
apply     | createElement.%.setAttribute ["checked", "checked"]
get       | createElement.% setAttribute ["input"]
apply     | createElement.%.setAttribute ["name", "t"]
get       | createElement.% appendChild ["div"]
apply     | createElement.%.appendChild [input t]
get       | @document createDocumentFragment ["^document"]
apply     | @document.createDocumentFragment []
get       | createDocumentFragment.% appendChild ["^fragment"]
get       | createElement.% lastChild ["div"]
apply     | createDocumentFragment.%.appendChild [input t]
get       | createDocumentFragment.% cloneNode ["^fragment"]
apply     | createDocumentFragment.%.cloneNode [true]
get       | cloneNode.% cloneNode ["^fragment"]
apply     | createDocumentFragment.%.cloneNode [true]
get       | cloneNode.% lastChild ["^fragment"]
get       | cloneNode.%.lastChildchecked ["^fragment", "input"]
get       | createElement.% checked ["^fragment", "input"]
get       | createDocumentFragment.% removeChild ["^fragment"]
apply     | createDocumentFragment.%.removeChild [input t]
get       | createDocumentFragment.% appendChild ["^fragment"]
apply     | createDocumentFragment.%.appendChild [div]
get       | createElement.% attachEvent ["^fragment", "div"]
get       | createDocumentFragment.% removeChild ["^fragment"]
apply     | createDocumentFragment.%.removeChild [div]
get       | toString call
apply     | call ["anonymous"]
get       | @document readyState ["^document"]
get       | @document addEventListener ["^document"]
get       | @document addEventListener ["^document"]
apply     | @document.addEventListener ["DOMContentLoaded", "anonymous", false]
get       | @addEventListener
apply     | @.addEventListener ["load", "anonymous", false]
get       | toString call
apply     | call ["anonymous"]
get       | @Math random
apply     | @Math.random []
get       | toString call
apply     | call [["radio", "checkbox"]]
get       | @document removeEventListener ["^document"]
get       | toString call
apply     | call [["blur", "focus", "focusin", 22 more...]]
get       | @Math random
apply     | @Math.random []
get       | @Object prototype
get       | @Object.prototype toString
construct | @.RegExp ["#((?: [\w\u00c0-\uFFFF\-...?! [^\ []*\])(?! [^\(]*\))"]
get       | @.RegExp.^0 source
construct | @.RegExp ["(^(?:.|\r|\n)*?)#((?: [\...?! [^\ []*\])(?! [^\(]*\))"]
construct | @.RegExp ["\.((?: [\w\u00c0-\uFFFF\...?! [^\ []*\])(?! [^\(]*\))"]
get       | @.RegExp.^2 source
construct | @.RegExp ["(^(?:.|\r|\n)*?)\.((?:[...?! [^\ []*\])(?! [^\(]*\))"]
construct | @.RegExp ["\ [name= ['"]*((?: [\w\u00...?! [^\ []*\])(?! [^\(]*\))"]
get       | @.RegExp.^4 source
construct | @.RegExp ["(^(?:.|\r|\n)*?)\ [name=...?! [^\ []*\])(?! [^\(]*\))"]
construct | @.RegExp ["\ [\s*((?: [\w\u00c0-\uFF...?! [^\ []*\])(?! [^\(]*\))"]
get       | @.RegExp.^6 source
construct | @.RegExp ["(^(?:.|\r|\n)*?)\ [\s*((...?! [^\ []*\])(?! [^\(]*\))"]
construct | @.RegExp ["^((?: [\w\u00c0-\uFFFF\*...?! [^\ []*\])(?! [^\(]*\))"]
get       | @.RegExp.^8 source
construct | @.RegExp ["(^(?:.|\r|\n)*?)^((?: [\...?! [^\ []*\])(?! [^\(]*\))"]
construct | @.RegExp [":(only|nth|last|first)-...?! [^\ []*\])(?! [^\(]*\))"]
get       | @.RegExp.^10 source
construct | @.RegExp ["(^(?:.|\r|\n)*?):(only|...?! [^\ []*\])(?! [^\(]*\))"]
construct | @.RegExp [":(nth|eq|gt|lt|first|la...?! [^\ []*\])(?! [^\(]*\))"]
get       | @.RegExp.^12 source
construct | @.RegExp ["(^(?:.|\r|\n)*?):(nth|e...?! [^\ []*\])(?! [^\(]*\))"]
construct | @.RegExp [":((?: [\w\u00c0-\uFFFF\-...?! [^\ []*\])(?! [^\(]*\))"]
get       | @.RegExp.^14 source
construct | @.RegExp ["(^(?:.|\r|\n)*?):((?: [\...?! [^\ []*\])(?! [^\(]*\))"]
get       | @Array prototype
get       | @Array.prototype slice
get       | slicecall
get       | @document document Element ["^document"]
get       | @document.document Element childNodes ["^document", "html"]
apply     | call [[head, <TextNode textContent="\n  ">, body], 0]
get       | call.% 0
get       | call.%.0nodeType ["^document", "html", "head"]
get       | @document documentElement ["^document"]
get       | @document.documentElement compareDocumentPosition ["^document", "html"]
get       | @document createElement ["^document"]
apply     | @document.createElement ["div"]
construct | @Date []
get       | @Date.^0getTime
apply     | getTime []
get       | @document documentElement ["^document"]
set       | createElement.% innerHTML ["div"]
get       | @document.documentElement insertBefore ["^document", "html"]
get       | @document.documentElement firstChild ["^document", "html"]
apply     | @document.documentElement.insertBefore [div, head]
get       | @document getElementById ["^document"]
apply     | @document.getElementById ["script1337186829145"]
get       | @document.documentElementremoveChild ["^document", "html"]
apply     | @document.documentElement.removeChild [div]
get       | @document createElement ["^document"]
apply     | @document.createElement ["div"]
get       | createElement.% appendChild ["div"]
get       | @documentcreateComment ["^document"]
apply     | @document.createComment [""]
apply     | createElement.%.appendChild [Comment { length=0,  nodeName="#comment",  nodeType=8,  more...}]
get       | createElement.% getElementsByTagName ["div"]
apply     | createElement.%.getElementsByTagName ["*"]
get       | getElementsByTagName.% length
set       | createElement.% innerHTML ["div"]
get       | createElement.% firstChild ["div"]
get       | createElement.% firstChild ["div"]
get       | createElement.%.firstChildgetAttribute ["div", "a"]
get       | createElement.% firstChild ["div"]
get       | createElement.%.firstChildgetAttribute ["div", "a"]
apply     | getElementsByTagName.%.0.getAttribute ["href"]
get       | @document querySelectorAll ["^document"]
get       | @document createElement ["^document"]
apply     | @document.createElement ["div"]
set       | createElement.% innerHTML ["div"]
get       | createElement.% querySelectorAll ["div"]
get       | createElement.% querySelectorAll ["div"]
apply     | createElement.%.querySelectorAll [".TEST"]
get       | querySelectorAll.% length
get       | @document documentElement ["^document"]
get       | @document.documentElement matchesSelector ["^document", "html"]
get       | @document.documentElement mozMatchesSelector ["^document", "html"]
get       | @document.documentElement.mozMatchesSelector call
get       | @document createElement ["^document"]
apply     | @document.createElement ["div"]
apply     | call [div, "div"]
get       | @document.documentElement.mozMatchesSelector call
get       | @document documentElement ["^document"]
get       | @document createElement ["^document"]
apply     | @document.createElement ["div"]
set       | createElement.% innerHTML ["div"]
get       | createElement.% getElementsByClassName ["div"]
get       | createElement.% getElementsByClassName ["div"]
apply     | createElement.%.getElementsByClassName ["e"]
get       | getElementsByClassName.% length
get       | createElement.% lastChild ["div"]
set       | createElement.%.lastChild className ["div", "div.e"]
get       | createElement.% getElementsByClassName ["div"]
apply     | createElement.%.getElementsByClassName ["e"]
get       | getElementsByClassName.% length
get       | @document documentElement ["^document"]
get       | @document.documentElementcontains ["^document", "html"]
get       | @Array prototype
get       | @Array.prototype slice
construct | @.RegExp ["<(?:abbr|article|aside|...mmary|time|video) [\s/>]", "i"]
get       | @document createDocumentFragment ["^document"]
apply     | @document.createDocumentFragment []
get       | createDocumentFragment.% createElement ["^fragment"]
get       | @document defaultView ["^document"]
get       | @document defaultView ["^document"]
get       | @getComputedStyle
get       | @document documentElement ["^document"]
get       | @document.documentElementcurrentStyle ["^document", "html"]
get       | toString call
apply     | call [["height", "width"]]
get       | toString call
apply     | call ["anonymous"]
get       | toString call
apply     | call ["anonymous"]
get       | @locationhref
get       | toString call
apply     | call [["ajaxStart", "ajaxStop", "ajaxComplete", 3 more...]]
get       | toString call
apply     | call [["get", "post"]]
get       | @String
construct | @Date []
get       | @Date.^1getTime
apply     | getTime []
get       | toString call
apply     | call ["callback"]
apply     | @Array.isArray ["callback"]
get       | toString call
apply     | call ["anonymous"]
apply     | @Array.isArray ["anonymous"]
get       | toString call
apply     | call ["anonymous"]
get       | toString call
apply     | call [Object { script="text/javascript, applic...pplication/x-ecmascript"}]
get       | hasOwnProperty call
apply     | call [Object { script="text/javascript, applic...pplication/x-ecmascript"}, "constructor"]
get       | hasOwnProperty call
apply     | call [Object {}, "isPrototypeOf"]
get       | hasOwnProperty call
apply     | call [Object { script="text/javascript, applic...pplication/x-ecmascript"}, "script"]
get       | toString call
apply     | call [Object { xml="application/xml, text/xml",  html="text/html",  text="text/plain",  more...}]
get       | hasOwnProperty call
apply     | call [Object { xml="application/xml, text/xml",  html="text/html",  text="text/plain",  more...}, "constructor"]
get       | hasOwnProperty call
apply     | call [Object {}, "isPrototypeOf"]
get       | hasOwnProperty call
apply     | call [Object { xml="application/xml, text/xml",  html="text/html",  text="text/plain",  more...}, "*"]
get       | toString call
apply     | call ["text/javascript, applic...pplication/x-ecmascript"]
apply     | @Array.isArray ["text/javascript, applic...pplication/x-ecmascript"]
get       | toString call
apply     | call [Object { script=RegExp}]
get       | hasOwnProperty call
apply     | call [Object { script=RegExp}, "constructor"]
get       | hasOwnProperty call
apply     | call [Object {}, "isPrototypeOf"]
get       | hasOwnProperty call
apply     | call [Object { script=RegExp}, "script"]
get       | toString call
apply     | call [Object { xml=RegExp,  html=RegExp,  json=RegExp,  more...}]
get       | hasOwnProperty call
apply     | call [Object { xml=RegExp,  html=RegExp,  json=RegExp,  more...}, "constructor"]
get       | hasOwnProperty call
apply     | call [Object {}, "isPrototypeOf"]
get       | hasOwnProperty call
apply     | call [Object { xml=RegExp,  html=RegExp,  json=RegExp,  more...}, "json"]
get       | toString call
apply     | call [RegExp /javascript|ecmascript/]
apply     | @Array.isArray [RegExp /javascript|ecmascript/]
get       | toString call
apply     | call [Object { text script=function()}]
get       | hasOwnProperty call
apply     | call [Object { text script=function()}, "constructor"]
get       | hasOwnProperty call
apply     | call [Object {}, "isPrototypeOf"]
get       | hasOwnProperty call
apply     | call [Object { text script=function()}, "text script"]
get       | toString call
apply     | call [Object { text html=true,  * text=String(),  text json=function(),  more...}]
get       | hasOwnProperty call
apply     | call [Object { text html=true,  * text=String(),  text json=function(),  more...}, "constructor"]
get       | hasOwnProperty call
apply     | call [Object {}, "isPrototypeOf"]
get       | hasOwnProperty call
apply     | call [Object { text html=true,  * text=String(),  text json=function(),  more...}, "text xml"]
get       | toString call
apply     | call ["anonymous"]
apply     | @Array.isArray ["anonymous"]
get       | toString call
apply     | call ["anonymous"]
get       | toString call
apply     | call ["anonymous"]
get       | @ActiveXObject
get       | @ActiveXObject
get       | @XMLHttpRequest
construct | @.XMLHttpRequest []
has       | @.XMLHttpRequest.^0 withCredentials
get       | toString call
apply     | call ["anonymous"]
get       | toString call
apply     | call [["height", "marginTop", "marginBottom", 2 more...]]
get       | toString call
apply     | call [["height", "marginTop", "marginBottom", 2 more...]]
get       | toString call
apply     | call [["height", "marginTop", "marginBottom", 2 more...]]
get       | toString call
apply     | call ["anonymous"]
get       | toString call
apply     | call [["height", "marginTop", "marginBottom", 8 more...]]
get       | @document documentElement ["^document"]
has       | @document.documentElementgetBoundingClientRect ["^document", "html"]
set       | @$
set       | @jQuery
get       | @document removeEventListener ["^document"]
apply     | @document.removeEventListener ["DOMContentLoaded", "anonymous", false]
get       | @document body ["^document"]
get       | @document getElementsByTagName ["^document"]
apply     | @document.getElementsByTagName ["body"]
get       | getElementsByTagName.% 0
get       | @document createElement ["^document"]
apply     | @document.createElement ["div"]
get       | createElement.% style ["div"]
set       | createElement.%.stylecssText
get       | @document.bodyinsertBefore ["^document", "html", "body"]
get       | @document.bodyfirstChild ["^document", "html", "body"]
apply     | @document.body.insertBefore [div, <TextNode textContent="\n    ">]
get       | @document createElement ["^document"]
apply     | @document.createElement ["div"]
get       | createElement.% appendChild ["^document", "html", "body", "div"]
apply     | createElement.%.appendChild [div]
set       | createElement.% innerHTML ["^document", "html", "body", "div", "div"]
get       | createElement.% getElementsByTagName ["^document", "html", "body", "div", "div"]
apply     | createElement.%.getElementsByTagName ["td"]
get       | getElementsByTagName.% 0
get       | getElementsByTagName.%.0 offsetHeight ["^document", "html", "body", "div", "div", "table", "tbody", "tr", "td"]
get       | getElementsByTagName.% 0
get       | getElementsByTagName.%.0 style ["^document", "html", "body", "div", "div", "table", "tbody", "tr", "td"]
set       | getElementsByTagName.%.0.styledisplay
get       | getElementsByTagName.% 1
get       | getElementsByTagName.%.1 style ["^document", "html", "body", "div", "div", "table", "tbody", "tr", "td"]
set       | getElementsByTagName.%.1.styledisplay
get       | getElementsByTagName.% 0
get       | getElementsByTagName.%.0offsetHeight ["^document", "html", "body", "div", "div", "table", "tbody", "tr", "td"]
get       | @getComputedStyle
set       | createElement.% innerHTML ["^document", "html", "body", "div", "div"]
get       | @document createElement ["^document"]
apply     | @document.createElement ["div"]
get       | createElement.% style ["div"]
set       | createElement.%.stylewidth
get       | createElement.% style ["div"]
set       | createElement.%.stylemarginRight
get       | createElement.% style ["^document", "html", "body", "div", "div"]
set       | createElement.%.stylewidth
get       | createElement.% appendChild ["^document", "html", "body", "div", "div"]
apply     | createElement.%.appendChild [div]
get       | @getComputedStyle
apply     | @.getComputedStyle [div, null]
get       | getComputedStyle.% marginRight
apply     | @parseInt ["0px", 10]
get       | createElement.% style ["^document", "html", "body", "div", "div"]
get       | createElement.%.stylezoom
get       | createElement.% style ["^document", "html", "body", "div", "div"]
set       | createElement.%.style cssText
set       | createElement.% innerHTML ["^document", "html", "body", "div", "div"]
get       | createElement.% firstChild ["^document", "html", "body", "div", "div"]
get       | createElement.%.firstChildfirstChild ["^document", "html", "body", "div", "div", "div"]
get       | createElement.%.firstChild nextSibling ["^document", "html", "body", "div", "div", "div"]
get       | createElement.%.firstChild.nextSibling firstChild ["^document", "html", "body", "div", "div", "table"]
get       | createElement.%.firstChild.nextSibling.firstChildfirstChild ["^document", "html", "body", "div", "div", "table", "tbody"]
get       | createElement.%.firstChild.firstChild offsetTop ["^document", "html", "body", "div", "div", "div", "div"]
get       | createElement.%.firstChild.nextSibling.firstChild.firstChildoffsetTop ["^document", "html", "body", "div", "div", "table", "tbody", "tr"]
get       | createElement.%.firstChild.firstChild style ["^document", "html", "body", "div", "div", "div", "div"]
set       | createElement.%.firstChild.firstChild.styleposition
get       | createElement.%.firstChild.firstChild style ["^document", "html", "body", "div", "div", "div", "div"]
set       | createElement.%.firstChild.firstChild.styletop
get       | createElement.%.firstChild.firstChild offsetTop ["^document", "html", "body", "div", "div", "div", "div"]
get       | createElement.%.firstChild.firstChild style ["^document", "html", "body", "div", "div", "div", "div"]
get       | createElement.%.firstChild.firstChild style ["^document", "html", "body", "div", "div", "div", "div"]
set       | createElement.%.firstChild.firstChild.styletop
set       | createElement.%.firstChild.firstChild.styleposition
get       | createElement.%.firstChild style ["^document", "html", "body", "div", "div", "div"]
set       | createElement.%.firstChild.style overflow
get       | createElement.%.firstChild style ["^document", "html", "body", "div", "div", "div"]
set       | createElement.%.firstChild.style position
get       | createElement.%.firstChild.firstChild offsetTop ["^document", "html", "body", "div", "div", "div", "div"]
get       | @document.body offsetTop ["^document", "html", "body"]
get       | @getComputedStyle
get       | createElement.% style ["^document", "html", "body", "div", "div"]
set       | createElement.%.stylemarginTop
get       | @getComputedStyle
apply     | @.getComputedStyle [div, null]
get       | getComputedStyle.% marginTop
get       | createElement.% style ["^document", "html", "body", "div"]
get       | createElement.%.stylezoom
get       | @document.bodyremoveChild ["^document", "html", "body"]
apply     | @document.body.removeChild [div]
get       | @document nodeType ["^document"]
get       | toString call
apply     | call [[HTMLDocument  {}]]
get       | @document nodeType ["^document"]
get       | @document nodeType ["^document"]
construct | @Date []
get       | @Date.^2getTime
apply     | getTime []
get       | @document window ["^document"]
get       | @document parentNode ["^document"]
get       | @document nodeName ["^document"]
get       | @document nodeName ["^document"]
get       | @document nodeType ["^document"]
get       | @document jQuery17209023800967826752 ["^document"]
get       | @document onready ["^document"]
get       | @document nodeName ["^document"]
get       | @document nodeName ["^document"]
get       | @documentready ["^document"]
get       | toString call
apply     | call [[HTMLDocument  {}]]
get       | @document nodeType ["^document"]
get       | @document jQuery17209023800967826752 ["^document"]
get       | @String constructor
get       | @String constructor
get       | @Function name
proto     | @String __proto__
get       | @String toString
apply     | toString []
get       | @String length
get       | @String length
get       | @Stringcallee
get       | @String constructor
get       | @String constructor
get       | @Function name
get       | @String name
get       | @String name
get       | @String constructor
get       | @String constructor
get       | @Function name
proto     | @String __proto__
get       | @String toString
apply     | toString []
get       | @String length
get       | @String length
get       | @String callee
get       | @String constructor
get       | @String constructor
get       | @Function name
get       | @String name
get       | @String name
get       | @String constructor
get       | @String constructor
get       | @Function name
proto     | @String __proto__
get       | @String toString
apply     | toString []
get       | @String length
get       | @String length
get       | @Stringcallee
get       | @String constructor
get       | @String constructor
get       | @Function name
get       | @String name
get       | @String name
get       | @document constructor ["^document"]
get       | @document constructor ["^document"]
get       | @.HTMLDocument toString
get       | @document constructor ["^document"]
get       | @.HTMLDocument toString
apply     | @.HTMLDocument .toString []
get       | @document constructor ["^document"]
get       | @document constructor ["^document"]
get       | @.HTMLDocument name
proto     | @document__proto__ ["^document"]
get       | @document toString ["^document"]
apply     | @document.toString []
get       | @document length ["^document"]
get       | @document length ["^document"]
get       | @document constructor ["^document"]
get       | @document constructor ["^document"]
get       | @.HTMLDocument name
get       | @document constructor ["^document"]
get       | @document constructor ["^document"]
get       | @document toString ["^document"]
get       | @document toString ["^document"]
apply     | @document.toString []
get       | @document constructor ["^document"]
get       | @document constructor ["^document"]
get       | @.HTMLDocument toString
get       | @document constructor ["^document"]
get       | @.HTMLDocument toString
apply     | @.HTMLDocument .toString []
get       | @document constructor ["^document"]
get       | @document constructor ["^document"]
get       | @.HTMLDocument name
proto     | @document__proto__ ["^document"]
get       | @document toString ["^document"]
apply     | @document.toString []
get       | @document length ["^document"]
get       | @document length ["^document"]
get       | @document constructor ["^document"]
get       | @document constructor ["^document"]
get       | @.HTMLDocument name
get       | @document constructor ["^document"]
get       | @document constructor ["^document"]
get       | @document toString ["^document"]
get       | @document toString ["^document"]
apply     | @document.toString []
```