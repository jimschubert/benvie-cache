var membrane = require('../').membrane;

function flattener(target){
  return membrane(flattenerHandler).wrap(target);
}

var flattenerHandler = {
  names: function(fwd, target){
    return props(target)
  },
  keys: function(fwd, target){
    fwd.trap = 'enumerate';
    return fwd().filter(function(s){
      return !(s in Object.prototype)
    });
  },
  owns: function(fwd, target, name){
    fwd.trap = 'has';
    return fwd();
  },
  describe: function(fwd, target, name){
    return Object.getPropertyDescriptor(target, name);
  }
};

function showAll(target){
  return membrane(showAllHandler).wrap(target);
}


function getList(fwd, target){
  var list = props(target).filter(function(s){
    return !Object.prototype.hasOwnProperty(s);
  });
  if (typeof target === 'function') {
    return list.filter(function(s){
      return !Function.prototype.hasOwnProperty(s);
    });
  } else {
    return list;
  }
}

var showAllHandler = {
  names: getList,
  keys: getList,
  enumerate: getList,
  owns: function(fwd, target, name){
    fwd.trap = 'has';
    return fwd();
  },
  describe: function(fwd, target, name){
    var v = Object.getPropertyDescriptor(target, name);
    if (v) return new Desc(v, true);
  },
};


Object.defineProperties(Object, {
  getPropertyDescriptor: new Desc(function getPropertyDescriptor(o,n){
    while (Object(o) === o) {
      var desc = Object.getOwnPropertyDescriptor(o,n);
      if (desc) return desc;
      o = Object.getPrototypeOf(o);
    }
  }),
  getPropertyNames: new Desc(function getPropertyNames(o){
    var names = [];
    while (Object(o) === o) {
      names = names.concat(Object.getOwnPropertyNames(o));
      o = Object.getPrototypeOf(o);
    }
    return unique(names);
  })
});

function Desc(v,e){
  if (e) this.enumerable = true;
  if (v && v.get || v.set) {
    this.get = v.get;
    this.set = v.set;
  } else if ('value' in v) {
    this.value = v.value;
    this.writable = true;
  } else {
    this.value = v;
    this.writable = true;
  }
}
Desc.prototype = { configurable: true }

function unique(a){
  return Object.keys(a.reduce(function(r,s){ r[s]=1; return r }, {}));
}

var desc = Object.getPropertyDescriptor;
var props = Object.getPropertyNames;


var x = showAll(global);
var z = new x.process.EventEmitter;
var o = {};
z.y = o;
z.y.a = {}
console.log(z.y === o);
console.log(z);
console.log(x);