var multiherit = require('../').multiherit;



function Talks(name, says){
  this.name = name;
  this.says = says;
}

Talks.prototype = {
  speak: function speak(at){
    at(this.name + ' says: ' + this.says);
  }
};


function Walks(name, stride){
  this.name = name;
  this.stride = stride;
}

Walks.prototype = {
  move: function move(where){
    var self = this;
    setTimeout(function(){
      where[self.name] = self;
      self.location = where;
    }, this.stride);
  }
};


function Fondles(desires){
  this.desires = desires;
}

Fondles.prototype = {
  touch: function touch(who){
    this.felt = who[this.desires];
  }
};




var WalksTalksFondles = multiherit({
  ctors: [Walks, Talks, Fondles],
  params: ['name', 'says', 'stride', 'desires']
});

WalksTalksFondles.prototype.type = 'man';

console.log(WalksTalksFondles);
console.log();
console.log(WalksTalksFondles.prototype);
Walks.prototype.speed = 1000;
console.log();
console.log(WalksTalksFondles.prototype);
// { type: 'man',
//   move: [Function: move],
//   speed: 1000,
//   speak: [Function: speak],
//   touch: [Function: touch] }
console.log();

var bob = new WalksTalksFondles('bob', 'hey guys', 100, 'name');
bob.touch(Object)
bob.speak(console.log);
console.log(bob);


function Bob(){
  this.newbob = true;
}

Bob.prototype = bob;
Bob.prototype.constructor = Bob;


var bobbins = new Bob;
bobbins.touch(Array)
console.log();
console.log(bobbins);