var tracer = require('../').tracer;

var emitter = tracer(function Test(){ this.name = 'bob' }, 'Test');
emitter.on('*', function(event){
  var args = [event.type, event.path.join('.')];
  if (event.property) args.push(event.property);
  console.log.apply(console, args);
});


var Test = emitter.Test;
var bob = new Test;
console.log(bob);

bob.stuff = { x: ['a',',b','c'] }
console.log(bob.stuff.x[0]);

if (typeof document !== 'undefined') {
    // the wrapper can be used with DOM elements provided you always work through the wrappers
    var emitter = tracer(document, 'document');
  emitter.on('*', function(event){
    var args = [event.type, event.path.join('.')];
    if (event.property) args.push(event.property);
    console.log.apply(console, args);
  });
  var doc = emitter.document;
  var div = doc.createElement('div');
  doc.body.appendChild(div);
}