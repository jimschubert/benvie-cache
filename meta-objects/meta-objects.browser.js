var meta = function(global){
if (typeof module !== 'undefined' && module.O)
  return module.exports = module.O;

var FP = Function.prototype,
    OP = Object.prototype,
    AP = Array.prototype,

    bindbind = FP.bind.bind(FP.bind),
    callbind = bindbind(FP.call),
    applybind = bindbind(FP.apply),
    calling = callbind(FP.call),
    binding = callbind(FP.bind),
    applying = callbind(FP.apply),

    hasOwn = callbind(OP.hasOwnProperty),

    flatten = applybind(AP.concat, []),
    concat = callbind(AP.concat),
    map = callbind(AP.map),

    define = Object.defineProperty,
    defines = Object.defineProperties,
    describe = Object.getOwnPropertyDescriptor,
    describeAny = getPropertyDescriptor,
    names = Object.getOwnPropertyNames,
    namesAny = getPropertyNames,
    keys = Object.keys,
    getProto = Object.getPrototypeOf,
    create = Object.create;

var types = [ Array, Boolean, Date, Function, Map, Number, Object, RegExp, Set, String, WeakMap ];


function Descriptor(type, valueOrGet, readonlyOrSet, hidden, frozen){
  this[type ? 'setAccessors' : 'setValue'](valueOrGet, readonlyOrSet);
  if (hidden)
    this.enumerable = false;
  if (frozen)
    this.configurable = false;
}

Descriptor.VALUE = 0;
Descriptor.ACCESSOR = 1;

Descriptor.prototype = {
  constructor: Descriptor,
  configurable: true,
  enumerable: true,
  setValue: function setValue(value, readonly){
    this.value = value;
    this.writable = !readonly;
  },
  setAccessors: function setAccessors(get, set){
    this.get = get;
    this.set = set;
  }
};


function value(v, h, r, f){
  return new Descriptor(0, v, r, h, f);
}

function hiddenValue(v, r, f){
  return new Descriptor(0, v, r, true, f);
}

function accessor(g, s, h, f){
  return new Descriptor(1, g, s, h, f);
}

function hiddenAccessor(g, s, f){
  return new Descriptor(1, g, s, true, f);
}

function isAccessor(desc){
  return isObject(desc) && ('get' in desc || 'set' in desc) && !('value' in desc);
}

function isValue(desc){
  return isObject(desc) && !('get' in desc || 'set' in desc) && 'value' in desc;
}


function merge(to, from){
  if (~types.indexOf(to) || keys(to).length === 0 && names(to) > 0)
    var desc = hiddenValue();
  else
    var desc = value();

  keys(from).forEach(function(key){
    desc.value = from[key];
    define(to, key, desc);
  });
  return to;
}

function isObject(o){
  return Object(o) === o;
}

function isPrimitive(o){
   return Object(o) !== o;
}

function getPropertyDescriptor(o,n){
  while (isObject(o)) {
    var desc = describe(o,n);
    if (desc)
      return desc;
    o = getProto(o);
  }
  return undefined;
}

function getPropertyNames(o){
  var out = [];
  while (isObject(o)) {
    out.push(names(o));
    o = getProto(o);
  }
  return unique(flatten(out));
}

function parameters(fn){
  return (fn+='').slice(fn.indexOf('(') + 1, fn.indexOf(')')).split(/\s*,\s*/);
}

function unique(a){
  return keys(a.reduce(function(r,s){ r[s] = 1; return r }, {}));
}

var slice = function(){
  var _slice = [].slice;

  return function slice(a,o,p){
    switch (a.length) {
             case  0: return [];
             case  1: return o ? [] : [a[0]];
            default: return _slice.call(a,o,p);
             case  2: a = [a[0],a[1]];
      break; case  3: a = [a[0],a[1],a[2]];
      break; case  4: a = [a[0],a[1],a[2],a[3]];
      break; case  5: a = [a[0],a[1],a[2],a[3],a[4]];
      break; case  6: a = [a[0],a[1],a[2],a[3],a[4],a[5]];
      break; case  7: a = [a[0],a[1],a[2],a[3],a[4],a[5],a[6]];
      break; case  8: a = [a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7]];
      break; case  9: a = [a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8]];
      break; case 10: a = [a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9]];
      break; case 11: a = [a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10]];
      break; case 12: a = [a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11]];
      break; case 13: a = [a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12]];
      break; case 14: a = [a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13]];
    }
    return (o || p) ? a.slice(o,p) : a;
  }
}();



var brandname = function(){
  var brands = {};

  types.forEach(function(Ctor){
    if (hasOwn(Ctor.prototype, 'toString'))
      define(Ctor, 'stringify', hiddenValue(callbind(Ctor.prototype.toString)));
    brands['[object '+Ctor.name+']'] = Ctor.prototype;
  });

  return function brandname(o){
    var brand = Object.stringify(o);
    return brand in brands ? brands[brand] : brand;
  }
}();


define(global, 'Descriptor', hiddenValue(Descriptor));

merge(Descriptor, {
  value: value,
  hiddenValue: hiddenValue,
  accessor: accessor,
  hiddenAccessor: hiddenAccessor,
  isValue: isValue,
  isAccessor: isAccessor
});

merge(Object, {
  isObject: isObject,
  isPrimitive: isPrimitive,
  getPropertyDescriptor: getPropertyDescriptor,
  getPropertyNames: getPropertyNames,
  hasOwn: hasOwn,
  merge: merge,
  brandname: brandname
});

merge(Function, {
  parameters: parameters,
  bindbind: bindbind,
  callbind: callbind,
  applybind: applybind,
  calling: calling,
  binding: binding,
  applying: applying
});

merge(Array, {
  unique: unique,
  slice: slice,
  flatten: flatten,
  concat: concat,
  map: map
});


function O(v){
  var out = Object.create(null);
  if (isObject(v))
    merge(out, v);
  return out;
}

Object.merge(O, {
  isobj: Object.isObject,
  notobj: Object.isPrimitive,
  create: Object.create,
  define: Object.defineProperty,
  defines: Object.defineProperties,
  getProto: Object.getPrototypeOf,
  describeAny: Object.getPropertyDescriptor,
  describe: Object.getOwnPropertyDescriptor,
  namesAny: Object.getPropertyNames,
  names: Object.getOwnPropertyNames,
  keys: Object.keys,
  has: Object.hasOwn,
});


if (typeof module !== 'undefined')
  module.O = module.exports = O;

if (typeof WeakMap === 'undefined') throw new Error('WeakMaps unsupported')

var WrapMap = function(O){
  "use strict";

  function lookup(o){
    if (!wrapmaps.has(o))
      throw new TypeError('WrapMaps are not generic');
    else
      return wrapmaps.get(o);
  }


  var wrapmaps = new WeakMap;

  function WrapMap(wrapper) {
    var wrapped = new WeakMap;
    var unwrapped = new WeakMap;

    if (typeof wrapper === 'function') {
      var wrap = function wrap(o, isDescriptor){
        if (isDescriptor === true)
          return wrapDescriptor(o);
        else if (Object.isPrimitive(o) || wrapped.has(o))
          return o;
        else if (unwrapped.has(o))
          return unwrapped.get(o);
        else {
          var p = wrapper(o);
          if (Object.isObject(p)) {
            wrapped.set(p, o);
            unwrapped.set(o, p);
          }
          return p;
        }
      }
    } else {
      var wrap = function wrap(o, p){
        if (Object.isPrimitive(o) || wrapped.has(o))
          return o;
        else if (unwrapped.has(o))
          return unwrapped.get(o);
        else {
          if (Object.isObject(p)) {
            wrapped.set(p, o);
            unwrapped.set(o, p);
          }
          return p;
        }
      }
    }

    function unwrap(o, isDescriptor){
      if (isDescriptor === true)
        return unwrapDescriptor(o);
      else if (Object.isPrimitive(o) || !wrapped.has(o))
        return o;
      else
        return wrapped.get(o);
    }

    function has(o){
      return Object.isObject(o) && wrapped.has(o);
    }

    function remove(o){
      var p = unwrap(o);
      if (o !== p)
        wrapped.delete(o);
      return p;
    }

    function wrapDescriptor(o){
      if (Object.isObject(o) && !wrapped.has(o)) {
        if (o.value) o.value = wrap(o.value);
        if (o.set) o.set = wrap(o.set);
        if (o.get) o.get = wrap(o.get);
      }
      return o;
    }

    function unwrapDescriptor(o){
      if (Object.isObject(o) && wrapped.has(o)) {
        if (o.value) o.value = unwrap(o.value);
        if (o.set) o.set = unwrap(o.set);
        if (o.get) o.get = unwrap(o.get);
      }
      return o;
    }


    var self = this === global ? Object.create(WrapMap.prototype) : this;
    self.wrap = wrap;
    self.unwrap = unwrap;
    self.remove = remove;
    self.has = has;

    wrapmaps.set(self, {
      wrap: wrap,
      unwrap: unwrap,
      remove: remove,
      has: has
    });

    return self;
  }

  // less efficient prototype versions that must first lookup `this`
  // allows this to work: `WrapMap.prototype.wrap.call(wrapmapInstance, o)`
  WrapMap.prototype = {
    constructor: WrapMap,
    wrap: function wrap(o, p){
      return lookup(this).wrap(o, p);
    },
    unwrap: function unwrap(o){
      return lookup(this).unwrap(o);
    },
    remove: function remove(o){
      return lookup(this).remove(o);
    },
    has: function has(o){
      return lookup(this).has(o);
    },
  };

  return WrapMap;
}(typeof O === 'undefined' ? require('./utility') : O);

if (typeof module !== 'undefined')
  module.exports = WrapMap;


var Emitter = function(O){
  "use strict";

  // ### Generic Event ###

  function Event(type, target){
    this.type = type;
    this.target = target;
  }

  function ErrorEvent(error, event, target){
    Event.call(this, 'error', target)
    this.error = error;
    this.event = event;
  }


  var emitters = new WeakMap;
  var receivers = new WeakMap;


  function _(o){
    if (!emitters.has(o)) {
      receivers.set(o, o);
      return emitters.set(o, {});
    } else {
      return emitters.get(o);
    }
  }

  // ##############################################
  // ### Prototype and initializer for emitters ###
  // ##############################################

  function Emitter(){
    emitters.set(this, {});
    receivers.set(this, this);
  }

  Emitter.Event = Event;

  // forward on event subscriptions from one object to another
  Emitter.forward = function forward(from, to){
    if (!emitters.has(to)) {
      emitters.set(to, {});
      receivers.set(to, to);
    }
    emitters.set(from, emitters.get(to));
    from.on = Emitter.prototype.on.bind(to);
    from.off = Emitter.prototype.off.bind(to);
  }


  // standardish emitter that is able to be ignorant of QT needs
  Emitter.prototype = {
    constructor: Emitter,
    on: function on(events, listener){
      var listeners = _(this);
      events.split(' ').forEach(function(event){
        if (event in listeners){
          listeners[event].push(listener);
        } else {
          listeners[event] = [listener];
        }
      });
    },
    off: function off(events, listener){
      var listeners = _(this);
      events.split(' ').forEach(function(event){
        if (listeners[event]) {
          listeners[event].splice(listeners[event].indexOf(listener), 1);
        }
      });
    },
    offAll: function offAll(event){
      delete _(this)[event];
    },
    once: function once(event, listener){
      var self = this;
      this.on(event, function(){
        self.off(event, listener);
        return listener.apply(receivers.get(self), arguments);
      });
    },
    isListened: function isListened(type){
      var events = _(this);
      var listeners = events[type] || events['*'];
      return Boolean(listeners && listeners.length);
    },
    emit: function emit(type){
      var event, events = _(this);
      if (typeof type !== 'string') {
        event = type;
        type = event.type;
      }
      if (events['*']) {
        var listeners = events[type] ? events['*'].concat(events[type]) : events['*'];
      } else {
        var listeners = events[type];
      }
      if (listeners && listeners.length) {
        event = event || new Event(type, receivers.get(this));
        var args = [event].concat(Array.slice(arguments, 1));
        for (var i=0; i < listeners.length; i++) {
          try {
            listeners[i].apply(this, args);
          } catch (e) {
            this.emit(new ErrorEvent(e, event, listeners[i]));
          }
        }
      }
    }
  };


  return Emitter;
}(typeof O === 'undefined' ? require('./utility') : O);

if (typeof module !== 'undefined')
  module.exports = Emitter;


if (typeof Proxy !== 'object') throw new Error('Proxies unsupported');

var proxy = function(O){
  "use strict";

  var createProxyObject = Proxy.create,
      createProxyFunction = Proxy.createFunction;

  var wmhas = Function.callbind(WeakMap.prototype.has),
      wmget = Function.callbind(WeakMap.prototype.get),
      wmset = Function.callbind(WeakMap.prototype.set),
      wmdelete = Function.callbind(WeakMap.prototype.delete),
      wmhasget = function hasget(wm, obj){
        return (Object.isObject(obj) && wmhas(wm, obj)) ? wmget(wm, obj) : obj;
      };

  var checkArray = function(){
    var wrapped = new WeakMap,
        isArr = Array.isArray;

    Array.isArray = function isArray(){
      var a = arguments[0];
      if (isArr(a)) return true;
      if (Object.isPrimitive(a)) return false;
      return wmhas(wrapped, a);
    }

    return function(o,p){
      if (isArr(o))
        wmset(wrapped, o, true);
    }
  }();

  var checkToString = function(){
    var wrapped = new WeakMap;

    var FtoString = function toString(){
      return Function.stringify(wmhasget(wrapped, this));
    }

    var OtoString = function toString(){
      return Object.stringify(wmhasget(wrapped, this));
    }

    var DtoString = function toString(){
      return Date.stringify(wmhasget(wrapped, this));
    }


    wmset(wrapped, FtoString, Function.prototype.toString);
    wmset(wrapped, OtoString, Object.prototype.toString);
    wmset(wrapped, DtoString, Date.prototype.toString);

    Function.prototype.toString = FtoString;
    Object.prototype.toString = OtoString;
    Date.prototype.toString = DtoString;

    return function(o,p){
      wmset(wrapped, p, o);
    }
  }();


  var definer = function(valueOnly, normal){
    return function definer(o, n, v, newDesc){
      if (newDesc && !Object.isExtensible(o))
        return false;

      var desc = newDesc ? normal : valueOnly;
      desc.value = v;
      O.define(o, n, desc);
      desc.value = null;
      return true;
    }
  }({}, Descriptor.value());

  function setter(desc, rcvr, val){
    var exists = desc.set != null;
    exists && Function.calling(desc.set, rcvr, val);
    return exists;
  }

  function configurable(desc){
    if (desc)
      desc.configurable = true;
    return desc;
  }



  var trapMap = {
    getOwnPropertyDescriptor: 'describe',
    getOwnPropertyNames: 'names',
    getPropertyNames: 'names',
    defineProperty: 'define',
    delete: 'delete',
    fix: 'fix',
    keys: 'keys',
    enumerate: 'enumerate',
    hasOwn: 'owns',
    has: 'has',
    get: 'get',
    set: 'set',
    apply: 'apply',
    construct: 'construct',
  };

  var forwarder = {
    proto:     function(T    ){ return O.getProto(T) },
    describe:  function(T,N  ){ return configurable(O.describe(T,N)) },
    define:    function(T,N,D){ return O.define(T,N,D), true },
    delete:    function(T,N  ){ return delete T[N] },
    fix:       function(T    ){ return Object.freeze(T) },
    keys:      function(T    ){ return O.keys(T) },
    names:     function(T    ){ return O.names(T) },
    enumerate: function(T    ){ var i=0, a=[]; for (a[i++] in T); return a },
    owns:      function(T,N  ){ return O.has(T,N) },
    has:       function(T,N  ){ return N in T },
    get: function(T,N,R){
      var handler = wmget(proxies, T);
      if (handler != null)
        return wmget(handler, R,N);

      if (N === '__proto__')
        return O.getProto(T);

      var desc = O.describe(T,N);
      if (desc == null) {
        var proto = O.getProto(T);
        if (proto != null)
          return forwarder.get(proto, N, R);// || T[N];
      } else if (Descriptor.isValue(desc))
        return desc.value;
      else if (Descriptor.isAccessor(desc)) {
        if (typeof desc.get === 'function')
          return Function.calling(desc.get, R);
      }
      return undefined;
    },
    set: function(T,N,V,R){
      var handler = wmget(proxies, T);
      if (handler != null)
        // target is a proxy, invoke its setter
        return handler.set(R,N,V);

      if (N === '__proto__')
        return Object.isObject(V) ? (T.__proto__ = V) && true : false;

      var oDesc = O.describe(T,N);
      if (oDesc) {
        // existing own desc
        if (Descriptor.isAccessor(oDesc))
          // is an accessor
          return setter(oDesc,R,V);
        else if (!oDesc.writable)
          // is readonly
          return false;
        else
          // set it
          return definer(R,N,V, R !== T);
      } else {
        var proto = O.getProto(T);
        if (proto === null)
          // no proto
          return definer(R,N,V, true);
        else
          // recurse to proto
          return forwarder.set(proto,N,V,R);
      }
    },
    apply: function(T,A,R){
      return Function.applying(T,R,A);
    },
    construct: function(T,A) {
      var handler = wmget(proxies, T);
      if (handler != null)
        return handler.construct(T, A);

      var result = new (Function.applying(Function.bind, T, [null].concat(A)));
      return Object.isObject(result) ? result : O.create(T.prototype);
    }
  };

  var proxies = new WeakMap,
      targets = new WeakMap;

  function proxy(target, handler, callable){
    if (!Object.isObject(target))
      throw new TypeError('Target must be an object');
    if (!Object.isObject(handler))
      throw new TypeError('Handler must be an object');

    function makeFwd(args, trap){
      var fwd = function(){
        return Function.applying(forwarder[fwd.trap], handler, [fwd.target].concat(fwd.args));
      };
      fwd.target = target;
      fwd.args = args;
      fwd.trap = trap;
      return fwd;
    }

    var metaHandler = createProxyObject({
      get: function get(R, trap){
        if (trap === 'getPropertyDescriptor')
          return function(n){
            return configurable(get(null, 'getOwnPropertyDescriptor')(n) || O.describeAny(O.getProto(target)));
          };

        if (trap === 'getPropertyNames')
          return function(){
            return Array.unique(get(null, 'getOwnPropertyNames')().concat(O.namesAny(O.getProto(target))));
          };

        trap = trapMap[trap];
        var trapHandler = handler[trap];
        return function(){
          var args = Array.slice(arguments);

          if (trap === 'get' || trap === 'set') {
            args = Array.concat(args.slice(1), args[0]);
            if (args[0] === 'splice')
              return Function.applying(forwarder[trap], handler, [target].concat(args));
            if (args[0] === '__proto__')
              trapHandler = handler.proto;
          }

          if (typeof trapHandler !== 'function')
            return Function.applying(forwarder[trap], handler, [target].concat(args));
          else
            return Function.applying(trapHandler, handler, [makeFwd(args, trap), target].concat(args));
        }
      }
    });

    if (callable === true || callable == null && typeof target === 'function') {
      var reflectProxy = createProxyFunction(metaHandler,
        function(){ return metaHandler.apply(Array.slice(arguments), this) },
        function(){ return metaHandler.construct(Array.slice(arguments)) }
      );
    } else {
      var reflectProxy = createProxyObject(metaHandler, O.getProto(target));
    }

    wmset(proxies, reflectProxy, metaHandler);
    wmset(targets, reflectProxy, target);

    checkArray(target, reflectProxy);
    checkToString(target, reflectProxy);

    return reflectProxy;
  };

  return proxy;
}(typeof O === 'undefined' ? require('./utility') : O);


if (typeof module !== 'undefined')
  module.exports = proxy;


var membrane = function(O, proxy, WrapMap){
  "use strict";

  if (proxy === null)
    proxy = require('./proxy');
  if (WrapMap === null)
    WrapMap = require('./WrapMap');


  function membrane(handlers){
    if (Object.isPrimitive(handlers)) throw new Error('Handlers must be provided');

    var wrapper = new WrapMap(function(target){
      //var protoCeptor = typeof target === 'function' ? function(){} : Object.create(wrapper.wrap(Object.getPrototypeOf(target)));
      return proxy(target, proxy({}, {
        get: function(f, t, trap){
          return function(fwd, faketarget){
            var handler = handlers[trap];
            var args = Array.slice(arguments);
            args[1] = fwd.target = wrapper.unwrap(target);
            if (handler) {
              var origfwd = fwd;
              fwd = function fwd(){ return Function.applying(handler, handlers, args); }
              fwd.args = origfwd.args;
            }
            return Function.applying(wrapHandler[trap], null, Array.concat([fwd], Array.slice(args, 1)));
          }
        }
      }));
    });


    function forward(fwd){ return fwd() }

    var wrapHandler = {
      names: forward,
      enumerate: forward,
      keys: forward,
      delete: forward,
      has: forward,
      owns: forward,
      proto: function(fwd, target){
        return wrapper.wrap(fwd());
      },
      fix: function(fwd, target){
        throw target;
      },
      define: function(fwd, target, name, desc){
        wrapper.unwrap(desc, true);
        return fwd();
      },
      describe: function(fwd, target, name){
        return wrapper.wrap(fwd(), true);
      },
      get: function(fwd, target, name, rcvr){
        return wrapper.wrap(fwd());
      },
      set: function(fwd, target, name, val, rcvr){
        fwd.args[1] = wrapper.unwrap(val);
        fwd.args[2] = wrapper.unwrap(rcvr);
        return fwd();
      },
      apply: function(fwd, target, args, rcvr){
        fwd.args[0] = Array.map(args, wrapper.unwrap);
        fwd.args[1] = wrapper.unwrap(rcvr);
        return wrapper.wrap(fwd());
      },
      construct: function(fwd, target, args){
        fwd.args[0] = Array.map(args, wrapper.unwrap);
        return wrapper.wrap(fwd());
      }
    };

    return wrapper;
  }

  return membrane;
}(typeof O === 'undefined' ? require('./utility') : O, typeof proxy === 'undefined' ? null : proxy, typeof WrapMap === 'undefined' ? null : WrapMap);


if (typeof module !== 'undefined')
  module.exports = membrane;


var multiherit = function(O, proxy){
  "use strict";

  if (proxy === null)
    proxy = require('./proxy');

  function multiherit(o){
    o.params = o.params || [];
    var arglist = o.ctors.map(function(ctor, i){
      if (Array.isArray(o.params[i])) {
        return o.params[i]
      } else {
        return parameters(ctor).map(function(param){
          return o.params.indexOf(param);
        });
      }
    });

    var protos = o.ctors.map(function(ctor){
      return ctor.prototype;
    });

    return new MultiCtor(new Multiproto(protos), arglist, o);
  }




  function MultiCtor(proto, params, o){
    var ctors = o.ctors;
    function Ctor(){
      var args = arguments;
      for (var i=0; i < ctors.length; i++) {
        apply(ctors[i], this, params[i].map(function(i){ return args[i] }));
      }
      return this;
    }
    Ctor.prototype = proto;
    proto.constructor = Ctor;
    this.name = o.name || ctors.map(function(ctor){ return ctor.name }).join('');
    this.createInstance = instanceCreator(proto, o.onCall, o.onConstruct);
    return proxy(Ctor, this);
  }

  function toString(){ return 'function '+this.name+'() { [native code] }' }

  MultiCtor.prototype = {
    get: function(fwd, target, name, rcvr){
      if (name === 'name') {
        return this.name;
      } else if (name === 'toString') {
        return toString;
      } else {
        return fwd();
      }
    },
    describe: function(fwd, target, name){
      if (name === 'name') {
        var desc = fwd();
        desc.value = this.name;
        return desc;
      } else {
        return fwd();
      }
    },
    call: function(fwd, target, args, rcvr){
      return this.construct(fwd, target, args);
    },
    construct: function(fwd, target, args){
      return apply(target, this.createInstance(), args);
    }
  };

  function instanceCreator(proto, call, construct){
    if (call || construct) {
      var handler = {
        apply: function(fwd, target, args, rcvr){
          return apply(call, rcvr, args);
        },
        construct: function(fwd, target, args){
          if (construct) {
            return apply(construct, Object.create(target.prototype), args);
          } else {
            return apply(call, global, args);
          }
        }
      };
      return function(){
        var fake = function(){};
        fake.__proto__ = proto;
        return proxy(fake, handler);
      }
    } else {
      return function(){
        return Object.create(proto);
      }
    }
  }

  function Multiproto(protos){
    this.protos = protos = Object.freeze(protos.slice());
    var proto = Object.create(null, { inherits: { configurable: true, value: protos } });
    return proxy(proto, this);
  }

  Multiproto.prototype = function(){
    function list(fwd, target){
      return unique(this.protos.reduce(function(ret, proto){
        fwd.target = proto;
        return ret.concat(fwd());
      }, fwd())).filter(function(s){ return !(s in Object.prototype) });
    }

    function has(fwd, target, name){
      if (fwd()) return true;
      for (var i=0; i < this.protos.length; i++) {
        fwd.target = this.protos[i];
        if (fwd()) return true;
      }
      return false;
    }

    return {
      names: list,
      keys: list,
      enumerate: list,
      hasOwn: has,
      has: has,
      describe: function(fwd, target, name){
        var desc = fwd();
        if (desc) return desc;
        for (var i=0; i < this.protos.length; i++) {
          fwd.target = this.protos[i];
          desc = fwd();
          if (desc)
            return desc;
        }
        return undefined;
      },
      get: function(fwd, target, name, rcvr){
        var ret, i=0;
        while (typeof ret === 'undefined' && fwd.target) {
          ret = fwd();
          fwd.target = this.protos[i++];
        }
        return ret;
      }
    };
  }();


  return multiherit;
}(typeof O === 'undefined' ? require('./utility') : O, typeof proxy === 'undefined' ? null : proxy);

if (typeof module !== 'undefined')
  module.exports = multiherit;


var tracer = function(O, proxy, membrane, Emitter){
  "use strict";

  if (proxy === null)
    proxy = require('./proxy');
  if (membrane === null)
    membrane = require('./membrane');
  if (Emitter === null)
    Emitter = require('./Emitter');



  var builtins = new WeakMap;
  var ids = new WeakMap;
  ids.id = 0;
  function tag(obj){
    return ids.has(obj) ? ids.get(obj) : ids.set(obj, ids.id++);
  }

  // function initBuiltins(){

  //   function addAll(obj, path){
  //     tag(obj);
  //     Object.getOwnPropertyNames(obj).forEach(function(name){
  //       if (name === 'arguments' || name === 'caller' || name ==='callee')
  //         return;
  //       var val = obj[name];
  //       if (Object.isObject(val) && !builtins.has(val))
  //         addAll(val, builtins.set(val, (path || []).concat(name)));
  //     });
  //   }

  //   [Error, EvalError, RangeError, ReferenceError, SyntaxError, TypeError, URIError,
  //     Array, Boolean, Date, Function, Map, Number, Object, Proxy, Set, String, WeakMap, Math, JSON,
  //     decodeURI, decodeURIComponent, encodeURI, encodeURIComponent, escape, eval,
  //     isFinite, isNaN, parseFloat, parseInt, unescape
  //   ].forEach(function(builtin){
  //     tag(builtin);
  //     var path = ['@'+(builtin.name || (builtin+'').slice(8, -1))];
  //     builtins.set(builtin, path);
  //     addAll(builtin, path);
  //   });

  //   ['document', 'navigator', 'location'].forEach(function(name){
  //     if (Object.isObject(global[name])) {
  //       tag(global[name]);
  //       builtins.set(global[name], ['@'+name]);
  //     }
  //   });
  // }

  //initBuiltins();

  function TraceEmitter(object, name){
    if (!(this instanceof TraceEmitter))
      return new TraceEmitter(object, name);

    var self = this;

    var names = new WeakMap;
    var instances = new WeakMap;
    name = name || 'root';
    names.set(object, [name]);

    Emitter.call(this);

    var managers = {
      get: function(target, prop, path, ret){
        if (Object.isObject(ret) && !names.has(ret))
          names.set(ret, path.concat(prop));
      },
      apply: function(target, args, path, ret){
        if (Object.isObject(ret) && !names.has(ret))
          names.set(ret, [path[path.length-1], '%']);
      },
      describe: function(target, prop, path, ret){
        if (Object.isObject(ret.value))
          names.set(ret.value, path.concat(prop));
        if (Object.isObject(ret.get))
          names.set(ret.get, path.concat(prop+'[getter]'));
        if (Object.isObject(ret.set))
          names.set(ret.set, path.concat(prop+'[setter]'));
      },
      construct: function(target, prop, path, ret){
        var num = instances.has(target) ? instances.get(target) + 1 : 0;
        instances.set(target, num);
        names.set(ret, (path || []).concat('^'+num));
      }
    };

    var wrapper = membrane(proxy({}, {
      get: function get(f,t,trap){
        return function(fwd, target, prop){
          var ret = fwd();
          var path = builtins.has(target) ? builtins.get(target) : names.get(target);
          var args = fwd.args;

          if (managers[trap])
            managers[trap](target, prop, path, ret);

          self.emit(new TraceEvent(trap, target, args, ret, path));
          return ret;
        }
      }
    }));

    this.unwrap = wrapper.unwrap;
    this[name] = wrapper.wrap(object);
  }

  TraceEmitter.prototype = Object.create(Emitter.prototype);
  TraceEmitter.prototype.constructor = TraceEmitter;

  function TraceEvent(type, target, args, result, path){
    Emitter.Event.apply(this, arguments);
    this.id = tag(target);
    this.result = result;
    this.path = path;
    if (type === 'construct' || type === 'apply') {
      this.args = args[0];
    } else if (type !== 'keys' && type !== 'names' && type !== 'enumerate' && type !== 'fix') {
      this.property = args[0];
    }
    if (type === 'define' || type === 'set') {
      this.value = args[1];
    }
    if (type === 'get' || type === 'apply') {
      this.receiver = args[1];
    } else if (type ===  'set') {
      this.receiver = args[2];
    }
  }

  TraceEvent.prototype = Object.create(Emitter.Event.prototype);
  TraceEvent.prototype.constructor = TraceEvent;


  function tracer(object, name){
    return new TraceEmitter(object, name);
  }

  return tracer;
}(typeof O === 'undefined' ? require('./utility') : O, typeof proxy === 'undefined' ? null : proxy, typeof membrane === 'undefined' ? null : membrane, typeof Emitter === 'undefined' ? null : Emitter);

if (typeof module !== 'undefined')
  module.exports = tracer;


var callable = function(O, proxy){
  "use strict";

  if (proxy === null)
    proxy = require('./proxy');

  function callable(obj){
    return proxy(obj, handler, true);
  }

  function list(fwd){
    return fwd().filter(function(s){ return s !== '$$call' && s !== '$$construct' && !hasOwn(s) });
  }

  var handler = {
    enumerate: list,
    keys: list,
    names: list,
    apply: function(fwd, target, args, rcvr){
      var call = target.$$call;
      if (call) {
        return apply(call, target, args);
      }
    },
    construct: function(fwd, target, args){
      var construct = target.$$construct;
      var instance = proxy(create(target), handler, true);
      if (construct) {
        var result = apply(construct, instance, args);
        return isObject(result) ? result : instance;
      } else{
        return instance;
      }
    }
  };

  return callable;
}(typeof O === 'undefined' ? require('./utility') : O, typeof proxy === 'undefined' ? null : proxy);

if (typeof module !== 'undefined')
  module.exports = callable;

var interceptor = function(O, proxy, WrapMap){
  "use strict";

  if (proxy === null)
    proxy = require('./proxy');
  if (WrapMap === null)
    WrapMap = require('./WrapMap');

  var store = new WrapMap;
  var wrap = store.wrap;
  var unwrap = store.unwrap;



  function listAccessors(o, p){
    var out = p || O.create(null);
    do {
      O.names(o).forEach(function(prop){
        var desc = O.describe(o, prop);
        if (desc.get || desc.set)
          out[prop] = desc;
      });
      o = O.getProto(o);
    } while (o);
    return out;
  }


  var Interceptor = function(){
    function list(fwd, target){
      return fwd().concat(this.properties);
    }
    function owns(fwd, target, prop){
      return fwd() || prop in this.accessors;
    }

    function Interceptor(name, accessors, proto){
      var brand = '[object '+name+']';
      this.accessors = accessors;
      this.prototype = proto;
      this.stringifier = Descriptor.hiddenValue(
        Object.hasOwn(proto, 'toString')
           ? proto.toString
           : function toString(){ return brand }
      );

      this.properties = O.keys(accessors);
    }

    Interceptor.keys = Object.freeze(Object.create(null));

    Interceptor.prototype = {
      getter: function(target, prop){
        return this.accessors[prop].get.call(unwrap(target));
      },
      setter: function(target, prop, value){
        return this.accessors[prop].set.call(unwrap(target), value);
      },
      names: list,
      keys: list,
      enumerate: list,
      has: owns,
      owns: owns,
      proto: function(fwd, target){
        return this.fakeproto;
      },
      describe: function(fwd, target, prop){
        if (prop === 'toString' && !Object.hasOwn(target, 'toString'))
          return this.stringifier;
        else if (prop in this.accessors) {
          var desc = O.describe(this.prototype, prop);
          return {
            enumerable: desc.enumerable,
            configurable: true,
            writable: Boolean(desc.set),
            value: desc.get.call(unwrap(target))
          };
        } else
          return fwd();
      },
      define: function(fwd, target, prop, desc){
        if (prop in this.accessors)
          return this.setter(target, prop, desc.value);
        else
          return fwd();
      },
      get: function(fwd, target, prop, rcvr){
        if (prop === 'toString' && !Object.hasOwn(target, 'toString'))
          return this.stringifier.value;
        else if (prop in this.accessors)
          return this.getter(target, prop);
        else
          return fwd();
      },
      set: function(fwd, target, prop, value, rcvr){
        if (prop in this.accessors)
          return this.setter(target, prop, value);
        else
          return fwd();
        }
    };

    return Interceptor;
  }();

  var IndexedInterceptor = function(){
    var Ceptor = Interceptor.prototype;

    function numbers(start, end){
      if (!isFinite(end)) {
        end = start;
        start = 0;
      }
      var length = end - start;
      if (end > numbers.cache.length) {
        while (length--) numbers.cache[length+start] = String(length+start);
      }
      return numbers.cache.slice(start, end);
    }

    numbers.cache = [];

    function list(fwd, target){
      return numbers(this.prototype.length.call(unwrap(target))).concat(this.properties, fwd());
    }

    function has(fwd, target, prop){
      return isFinite(prop) ? prop < this.prototype.length.call(unwrap(target)) : prop in this.accessors || fwd();
    }

    function IndexedInterceptor(name, accessors, proto){
      Interceptor.apply(this, arguments);
      this.properties.push('length');
    }

    var indexed = IndexedInterceptor.keys = Object.create(null);
    indexed.get = true;
    indexed.set = true;
    indexed.length = true;
    Object.freeze(indexed);

    IndexedInterceptor.prototype = {
      __proto__: Ceptor,
      names: list,
      keys: list,
      enumerate: list,
      has: has,
      owns: has,
      describe: function(fwd, target, prop){
        if (isFinite(prop) && this.prototype.get)
          return Descriptor.value(this.prototype.get.call(unwrap(target), prop));
        else if (prop === 'length')
          return Descriptor.hiddenValue(this.prototype.length.call(unwrap(target)), true);
        else
          return Ceptor.describe.apply(this, arguments);
      },
      define: function(fwd, target, prop, desc){
        if (isFinite(prop) && this.prototype.set)
          return this.prototype.set.call(unwrap(target), prop, value);
        else
          return Ceptor.define.apply(this, arguments);
      },
      get: function(fwd, target, prop, rcvr){
        if (isFinite(prop) && this.prototype.get)
          return this.prototype.get.call(unwrap(target), prop);
        else if (prop === 'length')
          return this.prototype.length.call(unwrap(target));
        else
          return Ceptor.get.apply(this, arguments);
      },
      set: function(fwd, target, prop, value, rcvr){
        if (isFinite(prop) && this.prototype.set)
          return this.prototype.set.call(unwrap(target), prop, value);
        else
          return Ceptor.set.apply(this, arguments);
      },
    };

    return IndexedInterceptor;
  }();

  var NamedInterceptor = function(){
    var Ceptor = Interceptor.prototype;

    function list(fwd, target){
      return this.prototype.list.call(unwrap(target)).concat(this.properties, fwd());
    }

    function has(fwd, target, prop){
      return fwd() || this.prototype.has.call(unwrap(target), prop);
    }

    function NamedInterceptor(name, accessors, proto){
      Interceptor.apply(this, arguments);
    }

    var named = NamedInterceptor.keys = Object.create(null);
    named.get = true;
    named.set = true;
    named.has = true;
    named.list = true;
    Object.freeze(named);

    NamedInterceptor.prototype = {
      __proto__: Ceptor,
      names: list,
      keys: list,
      enumerate: list,
      has: has,
      owns: has,
      describe: function(fwd, target, prop){
        var inst = unwrap(target);
        if (this.prototype.has.call(inst, prop))
          return Descriptor.value(this.prototype.get.call(inst, prop));
        else if (prop in this.accessors)
          return Ceptor.describe.apply(this, arguments);
        else if (prop in this.fakeproto)
          return fwd();
        else
          return Descriptor.value(this.prototype.get.call(unwrap(target), prop));
      },
      define: function(fwd, target, prop, desc){
        var inst = unwrap(target);
        if (this.prototype.has.call(inst, prop))
          return this.prototype.set.call(inst, prop, desc.value);
        else if (prop in this.accessors)
          return this.setter(target, prop, desc.value);
        else
          return this.prototype.set.call(unwrap(target), prop, value);
      },
      get: function(fwd, target, prop, rcvr){
        var inst = unwrap(target);
        if (this.prototype.has.call(inst, prop))
          return this.prototype.get.call(inst, prop);
        else if (prop in this.accessors)
          return this.getter(target, prop);
        else
          return fwd();
      },
      set: function(fwd, target, prop, value, rcvr){
        var inst = unwrap(target);
        if (this.prototype.has.call(inst, prop))
          return this.prototype.set.call(inst, prop, value);
        else if (prop in this.accessors)
          return this.setter(target, prop, value);
        else
          return this.prototype.set.call(unwrap(target), prop, value);
      },
    };

    return NamedInterceptor;
  }();


  var Constructor = function(){
    var stringifier = Descriptor.hiddenValue(function toString(){
      return 'function '+this.name+'() { [native code] }';
    });

    function Constructor(name, ctor, template){
      this.template = template;
      this.ctor = ctor;
      return proxy(new Function('return function '+name+'(){}')(), this);
    }

    Constructor.prototype = {
      describe: function(fwd, target, prop){
        if (prop === 'prototype')
          return Descriptor.hiddenValue(this.template.fakeproto);
        else if (prop === 'toString')
          return stringifier;
        else
          return fwd();
      },
      get: function(fwd, target, prop, rcvr){
        if (prop === 'prototype')
          return this.template.fakeproto;
        else if (prop === 'toString')
          return stringifier.value;
        else
          return fwd();
      },
      apply: function(fwd, target, args, rcvr){
        var inst = Object.create(this.template.fakeproto);
        var out = proxy(inst, this.template);
        wrap(out, inst);
        this.ctor.apply(out, args);
        return out;
      },
      construct: function(fwd, target, args){
        var inst = Object.create(this.template.fakeproto);
        var out = proxy(inst, this.template);
        wrap(out, inst);
        this.ctor.apply(out, args);
        return out;
      }
    };

    return Constructor;
  }();

  var protos = new WeakMap;



  function interceptor(name, Ctor, inherits){
    if (typeof name === 'function') {
      inherits = Ctor;
      Ctor = name;
      name = Ctor.name;
    }

    var proto = Ctor.prototype;
    var accessors = listAccessors(proto);
    if (inherits)
      listAccessors(protos.get(inherits), accessors);
    var Template = 'length' in proto ? IndexedInterceptor : 'list' in proto ? NamedInterceptor : Interceptor;
    var template = new Template(name, accessors, proto);
    var ctor = new Constructor(name, Ctor, template);

    template.fakeproto = O.keys(proto).reduce(function(ret, key){
      if (!(key in accessors || key in Template.keys)) {
        var desc = O.describe(proto, key);
        if (key === 'constructor')
          desc.value = ctor;
        desc.enumerable = false;
        O.define(ret, key, desc);
      }
      return ret;
    }, O.create(O.getProto(proto)));

    protos.set(template.fakeproto, proto);
    return ctor;
  }



  // var _ = function(){
  //   var bags = new WeakMap;
  //   var _ = Descriptor.hiddenValue;

  //   function Namespace(){}
  //   var N = Namespace.prototype = O({
  //     set: _(function set(key, value){
  //       if (Object.isObject(key)) {
  //         value = key;
  //         O.keys(key).forEach(function(key){
  //           this[key] = value[key];
  //         }, this);
  //       } else {
  //         this[key] = value;
  //       }
  //     }),
  //     toString: _(function toString(){
  //       return '[object Namespace]';
  //     }),
  //     valueOf: _(function valueOf(){
  //       return this;
  //     })
  //   });

  //   return function privates(obj, values){
  //     var bag = bags.has(obj) ? bags.get(obj) : bags.set(obj, new Namespace);
  //     if (values !== undefined)
  //       bag.set(values);
  //     return bag;
  //   }
  // }();


  return interceptor;
}(typeof O === 'undefined' ? require('./utility') : O, typeof proxy === 'undefined' ? null : proxy, typeof WrapMap === 'undefined' ? null : WrapMap);

if (typeof module !== 'undefined')
  module.exports = interceptor;


var doppelganger = function(O, proxy){
  "use strict";

  if (proxy === null)
    proxy = require('./proxy');


  function doppelganger(callable){
    var handler = new Doppelganger;
    var doppel = proxy(handler.target, handler, callable);

    return function changeInfo(into){
      handler.target = Object(into);
      if (doppel)
        return [doppel, doppel = null][0];
    }
  }

  function Doppelganger(){
    this.target = {};
  }

  function targeter(fwd){
    fwd.target = this.target;
    return fwd();
  }

  Doppelganger.prototype = {
    proto: targeter,
    names: targeter,
    describe: targeter,
    define: targeter,
    delete: targeter,
    fix: targeter,
    keys: targeter,
    enumerate: targeter,
    owns: targeter,
    has: targeter,
    get: targeter,
    set: targeter,
    apply: targeter,
    construct: targeter,
  };

  return doppelganger;
}(typeof O === 'undefined' ? require('./utility') : O, typeof proxy === 'undefined' ? null : proxy);

if (typeof module !== 'undefined')
  module.exports = doppelganger;

return { WrapMap: WrapMap, Emitter: Emitter, proxy: proxy, membrane: membrane, multiherit: multiherit, tracer: tracer, callable: callable, interceptor: interceptor, doppelganger: doppelganger }
}(Function("return this")());