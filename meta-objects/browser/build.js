var fs = require('fs');

function load(name){ return fs.readFileSync(name, 'utf8') }


var names = [
  'WrapMap',
  'Emitter',
  'proxy',
  'membrane',
  'multiherit',
  'tracer',
  'callable',
  'interceptor',
  'doppelganger',
];
var ret = names.map(function(name){
  return name+': '+name;
});

names = ['utility'].concat(names);

var libs = names.map(function(name){
  return load('../lib/'+name+'.js');
});

var output = [
  'var meta = function(global){',
  libs.join('\n\n'),  
  'return { ' + ret.join(', ') + ' }',
  '}(Function("return this")());'
].join('\n');

fs.writeFileSync('../meta-objects.browser.js', output);
fs.writeFileSync('../meta-objects.browser.min.js', compress(output));

function compress(src) {
  var parse = require('uglify-js').parser.parse;
  var ug = require('uglify-js').uglify;
  var opts = { make_seqs: true };
  return ug.gen_code(ug.ast_squeeze_more(ug.ast_squeeze((parse(src)), opts)));
}
