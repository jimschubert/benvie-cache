var Style = require('./colors');

var bindbind = Function.bind.bind(Function.bind);
var callbind = bindbind(Function.call);
var applybind = bindbind(Function.apply);
var push = applybind([].push);
var each = callbind([].forEach);

function Matrix(w, h){
  this.length = 0;
  var matrix = this;
  push(this, Array(h));
  each(this, function(x,i){
    matrix[i] = Array.apply(null, Array(w)).map(Number.bind(null, 0));
  });
  this.columns = Array.apply(null, Array(w)).map(function(x,i){
    return new Column(matrix, i, Style('#ffffff'));
  });
}
Matrix.prototype = {
  prototype: Matrix,
}


var backings = new WeakMap;
var cellTypes = new WeakMap;

function Column(backing, x, style, width){
  this.x = x;
  this.style = style;
  backings.set(this, backing);
  if (width) {
    this.width = width;
  }
  cellTypes.set(this, function Cell(y){
    this.y = y;
    backings.set(this, backing)
  }).prototype = new Cell(x, null, style);
}


Column.prototype = {
  constructor: Column,
  get: function get(y){
    return new (cellTypes.get(this))(y);
  },
  toString: function toString(){
    return backings.get(this).map(function(row){
      return row[this.x];
    }, this).join('\n');
  }
}


function Cell(x, y, style){
  this.x = x;
  this.y = y;
  this.style = style;
}

Cell.prototype = {
  constructor: Cell,
  toString: function toString(){
    return this.style(backings.get(this)[this.x][this.y]);
  },
  valueOf: function valueOf(){
    return backings.get(this)[this.x][this.y];
  }
}

var M = require('view-buffer/matrix');

var test = new M(5,5);


console.log(test.backing);