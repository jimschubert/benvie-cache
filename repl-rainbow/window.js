function Box(width, height){
  this.width = width;
  this.height = height;
}


Box.prototype = {
  constructor: Box,
  border: 'inset',
  chars: 'thin',
  toString: function toString(){
    var c = borders[this.chars];
    var width = this.width;
    var height = this.height;
    if (this.border === 'inset') {
      width -= 2;
      height -= 2;
    }
    var top =    c['┏']+repeat(c['━'], width)+c['┓'];
    var row =    c['┃']+repeat(' ',    width)+c['┃'];
    var bottom = c['┗']+repeat(c['━'], width)+c['┛'];
    return top+'\n'+repeat(row+'\n', height)+bottom;
  }
}

function repeat(s,n){ return new Array(n+1).join(s) }
var space = repeat.bind(null, ' ');

var borders = {
  thin:       '│─┌┐└┘┬┴├┤┼│─',
  thick:      '┃━┏┓┗┛┳┻┣┫╋┃━',
  thinXthick: '│─┌┐└┘┰┸┝┥╋┃━',
  thickXthin: '┃━┏┓┗┛┯┷┠┨┼│─'
};

var key = '┃━┏┓┗┛┳┻┣┫╋│─'.split('');
Object.keys(borders).forEach(function(name){
  var border = borders[name];
  borders[name] = {};
  key.forEach(function(char, i){
    borders[name][char] = border[i];
  });
});


var test = new Box(10,10);


console.log(test+'');