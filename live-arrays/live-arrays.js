if (typeof Proxy === 'undefined')
  throw new Error('subarray and ArrayView require Proxy');

void function(exports){
var _hasOwn = {}.hasOwnProperty,
    slice = [].slice,
    _apply = Function.apply


var hiddenDesc = { value: undefined,
                   writable: true,
                   enumerable: false,
                   configurable: true};
var normalDesc = { value: undefined,
                   writable: true,
                   enumerable: true,
                   configurable: true };
var lengthDesc = { value: undefined,
                   writable: false,
                   enumerable: false,
                   configurable: true }
var cache = [];

function numbers(start, end){
  if (!isFinite(end)) {
    end = start;
    start = 0;
  }
  var length = end - start,
      curr;

  if (end > cache.length) {
    while (length--)
      cache[curr = length + start] = '' + curr;
  }
  return cache.slice(start, end);
}


function isObject(o){
  return o != null && typeof o === 'object' || typeof o === 'function';
}

function define(o, p, v){
  o = Object(o);
  if (p instanceof Array) {
    p.forEach(function(f, i){
      if (typeof f === 'function' && f.name) {
        var name = f.name === 'delete_' ? 'delete' : f.name;
      } else if (typeof f === 'string' && typeof p[i+1] !== 'function' || !p[i+1].name) {
        var name = f;
        f = p[i+1];
      }
      if (name) {
        hiddenDesc.value = f;
        Object.defineProperty(o, name, hiddenDesc);
      }
    });
  } else if (isObject(p)) {
    Object.keys(p).forEach(function(k){
      var desc = Object.getOwnPropertyDescriptor(p, k);
      if (desc) {
        desc.enumerable = 'get' in desc;
        Object.defineProperty(o, k, desc);
      }
    });
  } else if (typeof p === 'string') {
    hiddenDesc.value = v;
    Object.defineProperty(o, p, hiddenDesc);
  }
  hiddenDesc.value = null;
}

if (typeof process !== 'undefined' && process.nextTick)
  var nextTick = process.nextTick;
else
  var nextTick = function nextTick(f){ setTimeout(f, 1) };




exports.subarray = subarray;

function subarray(parent, start, end){
  var prototype = Object.getPrototypeOf(Object(parent));
  var handler = new SubarrayHandler(Object.create(prototype), parent, start, end);
  return Proxy.create(handler, prototype);
}



function SubarrayHandler(expando, parent, start, end){
  this.parent = parent;
  this.offset = -start;
  this.end = end;
  this.length = end - start;
  this.expando = expando;
  lengthDesc.value = this.length;
  Object.defineProperty(expando, 'length', lengthDesc);
  numbers(this.length);
}


define(SubarrayHandler.prototype, [
  function getOwnPropertyNames(){
    return numbers(this.length).concat(Object.getOwnPropertyNames(this.expando));
  },
  function keys(){
    return numbers(this.length).concat(Object.keys(this.expando));
  },
  function enumerate(){
    var k = numbers(this.length),
        i = this.length;
    for (k[i++] in this.expando);
    return k;
  },
  function has(key){
    return ~~key === +key ? key >= 0 && key < this.length : key in this.expando;
  },
  function hasOwn(key){
    if (key === '!_') return true;
    return ~~key === +key ? key >= 0 && key < this.length : _hasOwn.call(this.expando, key);
  },
  function delete_(key){
    if (~~key === +key) {
      key -= this.offset;
      if (key >= 0 && key < this.end) {
        delete this.parent[key];
      }
    } else if (key !== 'length') {
      delete this.expando[key];
    }
    return true;
  },
  function get(rcvr, key){
    if (~~key === +key) {
      key -= this.offset;
      if (key >= 0 && key < this.end) {
        return this.parent[key];
      }
    } else {
      return this.expando[key];
    }
  },
  function set(rcvr, key, value){
    if (~~key === +key) {
      key -= this.offset;
      if (key >= 0 && key < this.end) {
        this.parent[key] = value;
      }
    } else {
      this.expando[key] = value;
    }
  },
  function getOwnPropertyDescriptor(key){
    if (~~key === +key) {
      key -= this.offset;
      if (key >= 0 && key < this.end) {
        normalDesc.value = this.parent[key];
        return normalDesc;
      }
    } else {
      var desc = Object.getOwnPropertyDescriptor(this.expando, key);
      desc && (desc.configurable = true);
      return desc
    }
  },
  function defineProperty(key, desc){
    if (~~key === +key) {
      if ('value' in desc) {
        key -= this.offset;
        if (key >= 0 && key < this.end) {
          this.parent[key] = desc.value;
        }
      }
    } else if (key === 'length') {
      Object.defineProperty(this.expando, key, { enumerable: desc.enumerable });
    } else {
      Object.defineProperty(this.expando, key, desc);
    }
  }
]);




exports.ArrayView = ArrayView;

function ArrayView(input, transformer, untransformer){
  if (!(this instanceof ArrayView))
    return new ArrayView(input, transformer, untransformer);

  if (!transformer && 'transformer' in input) {
    untransformer = input.untransformer;
    transformer = input.transformer;
    input = input.input || input;
  }
  var handler = new ArrayViewHandler(this, input, transformer, untransformer);
  return Proxy.create(handler, ArrayView.prototype);
}

ArrayView.prototype = [];
define(ArrayView.prototype, {
  constructor: ArrayView,
});



function ArrayViewHandler(expando, input, transformer, untransformer){
  this.input = input;
  this.expando = expando
  this.transformer = transformer;
  this.transformCache = new Map;
  if (untransformer) {
    this.untransformer = untransformer;
    this.untransformCache = new Map;
  } else {
    this.set = function set(rcvr, key, value){
      if (!(~~key === +key || key === 'length'))
        this.expando[key] = value;
    };
    this.defineProperty = function defineProperty(key, desc){
      if (!(~~key === +key || key === 'length'))
        Object.defineProperty(this.expando, key, desc);
    };
  }
}

define(ArrayViewHandler.prototype, [
  function transform(item, index){
    if (!isObject(item) && this.transformCache.has(item)) {
      return this.transformCache.get(item);
    } else {
      var transformed = this.transformer(item, index);
      this.transformCache.set(item, transformed);
      return transformed;
    }
  },
  function untransform(item, index){
    if (!isObject(item) && this.untransformCache.has(item)) {
      return this.untransformCache.get(item);
    } else {
      var untransformed = this.untransformer(item, index);
      this.untransformCache.set(item, untransformed);
      return untransformed;
    }
  },
  function set(rcvr, key, value){
    console.log(arguments.callee.name)
    if (~~key === +key) {
      if (key > -1) {
        this.input[key] = this.untransform(value, +key)
      }
    } else if (key === 'length') {
    } else {
      this.expando[key] = value;
    }
  },
  function defineProperty(key, desc){
    console.log(arguments.callee.name)
    if (~~key === +key) {
      if ('value' in desc && key > -1) {
        this.input[key] = this.untransform(desc.value, +key);
      }
    } else if (key === 'length') {
    } else {
      Object.defineProperty(this.expando, key, desc);
    }
  },
  function has(key){
    console.log(arguments.callee.name)
    return key === 'length' || (key > -1 && key < this.input.length) || key in this.expando;
  },
  function hasOwn(key){
    console.log(arguments.callee.name)
    if (key === '!_') return true;
    return key === 'length' || (key > -1 && key < this.input.length) || _hasOwn.call(this.expando, key);
  },
  function delete_(key){
    console.log(arguments.callee.name)
    if (~~key === +key) {
      if (key > -1 && key < this.input.length) {
        this.untransform(undefined, +key);
      }
    } else {
      delete this.expando[key];
    }
    return true;
  },
  function get(rcvr, key){
    if (~~key === +key) {
      if (key > -1 && key < this.input.length) {
        return this.transform(this.input[key], +key);
      }
    } else if (key === 'length') {
      return this.input.length;
    } else {
      return this.expando[key];
    }
  },
  function getOwnPropertyDescriptor(key){
    if (~~key === +key) {
      if (key > -1 && key < this.input.length) {
        normalDesc.value = this.transform(this.input[key], +key);
        return normalDesc;
      }
    } else if (key === 'length') {
      hiddenDesc.value = this.input.length;
      return hiddenDesc;
    } else {
      var desc = Object.getOwnPropertyDescriptor(this.expando, key);
      desc && (desc.configurable = true);
      return desc;
    }
  },
  function getOwnPropertyNames(){
    return numbers(this.input.length).concat(Object.getOwnPropertyNames(this.expando), 'length');
  },
  function keys(){
    return numbers(this.input.length).concat(Object.keys(this.expando));
  },
  function enumerate(){
    var k = numbers(this.input.length),
        i = this.input.length;
    for (k[i++] in this.expando);
    return k;
  },
]);







function BoundArray(onchange, onresize){
  var handlers = (typeof onchange === 'function') + (typeof onresize === 'function');
  var expando = [].slice.call(arguments, handlers);
  expando.__proto__ = BoundArray.prototype;
  var proxy = Proxy.create(new BoundArrayHandler(expando), BoundArray.prototype);
  proxy.onchange = onchange;
  proxy.onresize = onresize;
  return proxy;
}

exports.BoundArray = BoundArray;

BoundArray.prototype = [];
define(BoundArray.prototype, 'constructor', BoundArray);



function BoundArrayHandler(expando){
  this.expando = expando;
}

var changes = [],
    resizes = [],
    scheduled = false;


function dispatcher(){
  for (var i=0; i < resizes.length; i++) {
    var item = resizes[i];
    if (item[0].onresize)
      item[0].onresize.call(item[0], item[1], item[2]);
  }
  resizes.length = 0;
  for (var i=0; i < changes.length; i++) {
    var item = changes[i];
    if (i < item[0].length && item[0].onchange)
      item[0].onchange.call(item[0], item[1], item[2], item[3]);
  }
  scheduled = false;
  changes.length = 0;
}


define(BoundArrayHandler.prototype, [
  function change(key, value){
    changes[changes.length] = [this.expando, +key, value, this.expando[key]];
    if (value !== undefined && key >= this.expando.length) {
      resizes[resizes.length] = [this.expando, 1 + +key, this.expando.length];
    }
    if (!scheduled) {
      scheduled = true;
      nextTick(dispatcher);
    }
  },
  function resize(size){
    size = ~~size;
    if (size >= 0 && size !== this.expando.length) {
      resizes[resizes.length] = [this.expando, size, this.expando.length];
      if (!scheduled) {
        scheduled = true;
        nextTick(dispatcher);
      }
    }
  },
  function set(rcvr, key, value){
    if (~~key === +key && key >= 0)
      this.change(key, value);
    else if (key === 'length')
      this.resize(value);
    else if (key === 'onchange' || key === 'onresize')
      value = typeof value === 'function' ? value : null;
    this.expando[key] = value;
  },
  function defineProperty(key, desc){
    if (~~key === +key && key >= 0) {
      if ('value' in desc) {
        this.change(key, desc.value);
        this.expando[key] = desc.value;
      }
    } else if (key === 'onchange' || key === 'onresize') {
      if ('value' in desc) {
        desc.value = typeof desc.value === 'function' ? desc.value : null;
        Object.defineProperty(this.expando, key, desc);
      }
    } else if (key === 'length') {
      if ('value' in desc) {
        this.resize(desc.value);
        this.expando.length = desc.value;
      }
    } else {
      Object.defineProperty(this.expando, key, desc);
    }
  },
  function has(key){
    return key in this.expando;
  },
  function hasOwn(key){
    return _hasOwn.call(this.expando, key);
  },
  function delete_(key){
    if (~~key === +key && key >= 0)
      this.change(key, undefined);
    delete this.expando[key];
    return true;
  },
  function get(rcvr, key){
    return this.expando[key];
  },
  function getOwnPropertyDescriptor(key){
    var desc = Object.getOwnPropertyDescriptor(this.expando, key);
    desc && (desc.configurable = true);
    return desc;
  },
  function getOwnPropertyNames(){
    return Object.getOwnPropertyNames(this.expando);
  },
  function keys(){
    return Object.keys(this.expando);
  },
  function enumerate(){
    var k = [], i = 0;
    for (k[i++] in this.expando);
    return k;
  },
]);


if (!Array.isArray.prototype) {
  var isArr = Array.isArray;
  define(Array, 'isArray', function isArray(o){
    return isArr(o) || o instanceof BoundArray || _hasOwn.call(o, '!_');
  });
}

}(typeof exports === 'undefined' ? this : exports);
