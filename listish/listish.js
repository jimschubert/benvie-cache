/* (The MIT License)
 *
 * Copyright (c) 2012 Brandon Benvie <http://bbenvie.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the 'Software'), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included with all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


var listish = (function(global, exports, undefined){
  // #################
  // ### Utilities ###
  // #################

  var hasOwn = {}.hasOwnProperty,
      toSource = function(){}.toString;

  var isES5 = !(!Object.getOwnPropertyNames || 'prototype' in Object.getOwnPropertyNames);

  var hidden = { configurable: true,
                 enumerable: false,
                 writable: true,
                 value: undefined };



  function isObject(v){
    var type = typeof v;
    return type === 'object' ? v !== null : type === 'function';
  }

  var fname = (function(){
    if (Function.name === 'Function') {
      return function fname(f){
        return f ? f.name || '' : '';
      };
    }
    return function fname(f){
      if (typeof f !== 'function') {
        return '';
      }

      if (!hasOwn.call(f, 'name')) {
        var match = toSource.call(f).match(/^\n?function\s?(\w*)?_?\(/);
        if (match) {
          hidden.value = match[1];
          defineProperty(f, 'name', hidden);
        }
      }

      return f.name || '';
    };
  })();

  var create = isES5
    ? Object.create
    : (function(F, Null){
        var iframe = document.createElement('iframe'),
            hiddens = ['constructor', 'hasOwnProperty', 'propertyIsEnumerable',
                       'isPrototypeOf', 'toLocaleString', 'toString', 'valueOf'];
        iframe.style.display = 'none';
        document.body.appendChild(iframe);
        iframe.src = 'javascript:';
        Null.prototype = iframe.contentWindow.Object.prototype;
        document.body.removeChild(iframe);

        while (hiddens.length) {
          delete Null.prototype[hiddens.pop()];
        }

        return function create(object){
          if (object === null) {
            return new Null;
          } else {
            F.prototype = object;
            object = new F;
            F.prototype = null;
            return object;
          }
        };
      })(function(){}, function(){});

  var ownKeys = isES5
    ? Object.keys :
      function keys(o){
        var out = [], i=0;
        for (var k in o) {
          if (hasOwn.call(o, k)) {
            out[i++] = k;
          }
        }
        return out;
      };

  var defineProperty = isES5
    ? Object.defineProperty
    : function defineProperty(o, k, desc){
        try { o[k] = desc.value } catch (e) {}
        return o;
      };


  var describeProperty = isES5
    ? Object.getOwnPropertyDescriptor
    : function getOwnPropertyDescriptor(o, k){
        if (hasOwn.call(o, k)) {
          return { value: o[k] };
        }
      };


  function define(object, p){
    var keys = ownKeys(p);

    for (var i=0; i < keys.length; i++) {
      var desc = describeProperty(p, keys[i]);
      if (desc) {
        desc.enumerable = 'get' in desc;
        defineProperty(object, keys[i], desc);
      }
    }

    hidden.value = undefined;
    return object;
  }

  function defineMethods(object, methods){
    for (var i=0; i < methods.length; i++) {
      hidden.value = methods[i];
      defineProperty(object, fname(methods[i]), hidden);
    }
  }

  function inherit(Ctor, Super, methods){
    Ctor.prototype = create(Super.prototype);
    define(Ctor.prototype, { constructor: Ctor });
    methods && defineMethods(Ctor.prototype, methods);
    return Ctor;
  }


  function exporter(f){
    f = f();
    exports[fname(f)] = f;
    defineMethods(f, [
      function toString(){
        return '[constructor '+f.name+']';
      }
    ]);
  }


  function Hash(){}
  Hash.prototype = create(null);
  exporter(function(){ return Hash });



  // #################
  // ### Iteration ###
  // #################

  function Iterator(){}

  defineMethods(Iterator.prototype, [
    function __iterator__(){
      return this;
    }
  ]);

  function Item(key, value){
    this[0] = key;
    this[1] = value;
  }

  define(Item.prototype, {
    isItem: true,
    length: 2
  });

  defineMethods(Item.prototype, [
    function toString(){
      return this[0] + '';
    },
    function valueOf(){
      return this[1];
    }
  ]);

  var StopIteration = exports.StopIteration = global.StopIteration || {};

  function iterate(o, callback, context){
    if (o == null) return;
    var type = typeof o;
    context = context || o;
    if (type === 'number' || type === 'boolean') {
      callback.call(context, new Item(0, o));
    } else {
      o = Object(o);
      var iterator = o.__iterator__ || o.iterator;

      if (typeof iterator === 'function') {
        var iter = iterator.call(o);
        if (iter && typeof iter.next === 'function') {
          try {
            while (1) callback.call(context, iter.next());
          } catch (e) {
            if (e === StopIteration) return;
            throw e;
          }
        }
      }

      if (type !== 'function' && o.length) {
        try {
          for (var i=0; i < o.length; i++) {
            callback.call(context, new Item(i, o[i]));
          }
        } catch (e) {
          if (e === StopIteration) return;
          throw e;
        }
      } else {
        var keys = ownKeys(o);
        try {
          for (var i=0; i < keys.length; i++) {
            var key = keys[i];
            callback.call(context, new Item(key, o[key]));
          }
        } catch (e) {
          if (e === StopIteration) return;
          throw e;
        }
      }
    }
  }

  exports.iterate = iterate;

  // ##################
  // ### LinkedList ###
  // ##################

  exporter(function(){
    function LinkedListIterator(list){
      this.item = list.sentinel;
      this.sentinel = list.sentinel;
    }

    inherit(LinkedListIterator, Iterator, [
      function next(){
        this.item = this.item.next;
        if (this.item === this.sentinel) {
          throw StopIteration;
        }
        return this.item.data;
      }
    ]);


    function Item(data){
      this.data = data;
    }

    defineMethods(Item.prototype, [
      function link(item){
        item.next = this;
        return item;
      },
      function unlink(){
        var next = this.next;
        this.next = next.next;
        next.next = null;
        return this;
      },
      function empty(){
        var data = this.data;
        this.data = undefined;
        this.next = null;
        return data;
      }
    ]);

    function Sentinel(){
      this.next = null;
    }

    inherit(Sentinel, Item, [
      function unlink(){
        return this;
      },
      function empty(){}
    ]);

    function find(list, value){
      if (list.lastFind && list.lastFind.next.data === value) {
        return list.lastFind;
      }

      var item = list.tail,
          i = 0;

      while ((item = item.next) !== list.sentinel) {
        if (item.next.data === value) {
          return list.lastFind = item;
        }
      }
    }

    function LinkedList(iterable){
      var sentinel = new Sentinel;
      this.size = 0;
      define(this, {
        sentinel: sentinel,
        tail: sentinel,
        lastFind: null
      });

      iterable && iterate(iterable, this.append, this);
    }

    defineMethods(LinkedList.prototype, [
      /**
       * Insert a new item at end of the list.
       * @return {Number} New list length.
       */
      function append(value){
        this.tail = this.tail.link(new Item(value));
        return ++this.size;
      },
      /**
       * Remove an item from end of the list and return it.
       * @return {Any} Removed item.
       */
      function pop() {
        var tail = this.tail,
            data = tail.data;
        this.tail = tail.next;
        tail.next = null;
        tail.data = undefined;
        return data;
      },
      /**
       * Insert item before the given value
       * @param  {Any}            value  Value to insert.
       * @param  {Any}            before Value to search for
       * @return {Number|Boolean} New list length or false if item not found.
       */
      function insert(value, before){
        var item = find(this, before);
        if (item) {
          var inserted = new Item(value);
          inserted.next = item.next;
          item.next = inserted;
          return ++this.size;
        }
        return false;
      },
      /**
       * Remove an item by value.
       * @return {Number}  new list size
       */
      function remove(value){
        var item = find(this, value);
        if (item) {
          item.unlink();
          this.size--;
        }
        return this.size;
      },
      /**
       * Add an item to the list directly after the given item
       * @param  {Any}     value  value to replace with
       * @param  {Any}     search value to be replaced
       * @return {Boolean} whether the search was found and replaced
       */
      function replace(value, replacement){
        var item = find(this, value);
        if (item) {
          var replacer = new Item(replacement);
          replacer.next = item.next.next;
          item.next.next = null;
          item.next = replacer;
          return true;
        }
        return false;
      },
      /**
       * Check if a value is in the list
       * @param  {Any}     search  value to find
       * @return {Boolean}
       */
      function has(value) {
        return !!find(this, value);
      },
      function items(){
        var item = this.tail,
            array = [];

        while (item !== this.sentinel) {
          array.push(item.data);
          item = item.next;
        }

        return array;
      },
      /**
       * Removed all the items from the list
       */
      function empty(){
        var next,
            item = this.tail;

        while (item !== this.sentinel) {
          next = item.next;
          item.empty();
          item = next;
        }

        this.tail = this.sentinel;
        this.size = 0;
        return this;
      },
      /**
       * Create a new list and add all the items from the current list into it
       * @return {DoublyLinkedList}
       */
      function clone(){
        var items = this.items(),
            list = new LinkedList,
            i = items.length;

        while (i--) {
          list.push(items[i]);
        }
        return list;
      },
      function __iterator__(){
        return new LinkedListIterator(this);
      }
    ]);

    return LinkedList;
  });

  // ########################
  // ### DoublyLinkedList ###
  // ########################

  exporter(function(){
    function DoublyLinkedListIterator(list){
      this.item = list.sentinel;
      this.sentinel = list.sentinel;
    }

    inherit(DoublyLinkedListIterator, Iterator, [
      function next(){
        this.item = this.item.next;
        if (this.item === this.sentinel) {
          throw StopIteration;
        }
        return this.item.data;
      }
    ]);


    function Item(data, prev){
      this.data = data;
      this.after(prev);
    }

    defineMethods(Item.prototype, [
      function after(item){
        this.relink(item);
        return this;
      },
      function before(item){
        this.prev.relink(item);
        return this;
      },
      function relink(prev){
        if (this.next) {
          this.next.prev = this.prev;
          this.prev.next = this.next;
        }
        this.prev = prev;
        this.next = prev.next;
        prev.next.prev = this;
        prev.next = this;
        return this;
      },
      function unlink(){
        if (this.next) {
          this.next.prev = this.prev;
        }
        if (this.prev) {
          this.prev.next = this.next;
        }
        this.prev = this.next = null;
        return this;
      },
      function empty(){
        var data = this.data;
        this.next = this.prev = this.data = null;
        return data;
      }
    ]);

    function Sentinel(list){
      this.next = this;
      this.prev = this;
    }

    inherit(Sentinel, Item, [
      function unlink(){
        return this;
      }
    ]);

    function find(list, value){
      if (list.lastFind && list.lastFind.data === value) {
        return list.lastFind;
      }

      var item = list.sentinel,
          i = 0;

      while ((item = item.next) !== list.sentinel) {
        if (item.data === value) {
          return list.lastFind = item;
        }
      }
    }


    function DoublyLinkedList(iterable){
      this.size = 0;
      define(this, {
        sentinel: new Sentinel,
        lastFind: null
      });

      iterable && iterate(iterable, this.append, this);
    }

    defineMethods(DoublyLinkedList.prototype, [
      /**
       * Peak at the first item in the list
       * @return {Any}
       */
      function first() {
        return this.sentinel.next.data;
      },
      /**
       * Peak at the last item in the list
       * @return {Any}
       */
      function last() {
        return this.sentinel.prev.data;
      },
      /**
       * Add an item to the beginning of the list
       * @param  {Any}     value  new beginning of the list
       * @return {Number}         new list size
       */
      function prepend(value){
        var item = new Item(value, this.sentinel);
        return this.size++;
      },
      /**
       * Add an item to the end of the list
       * @param  {Any}     value  new end of the list
       * @return {Number}         new list size
       */
      function append(value){
        var item = new Item(value, this.sentinel.prev);
        return this.size++;
      },
      /**
       * Add an item to the list directly after the given item
       * @param  {Any}     value  value to insert
       * @param  {Any}     search value to search for
       * @return {Number|Boolean} new list size or false if search not found
       */
      function insertAfter(value, search){
        var item = find(this, search);
        if (item) {
          item = new Item(value, item);
          return this.size++;
        }
        return false;
      },
      /**
       * Add an item to the list directly before the given item
       * @param  {Any}     value  value to insert
       * @param  {Any}     search value to search for
       * @return {Number|Boolean} new list size or false if search not found
       */
      function insertBefore(value, search){
        var item = find(this, search);
        if (item) {
          item = new Item(value, item.prev);
          return this.size++;
        }
        return false;
      },
      /**
       * Add an item to the list directly after the given item
       * @param  {Any}     value  value to replace with
       * @param  {Any}     search value to be replaced
       * @return {Boolean} whether the search was found and replaced
       */
      function replace(value, search){
        var item = find(this, search);
        if (item) {
          new Item(value, item);
          item.unlink();
          return true;
        }
        return false;
      },
      /**
       * Remove the end of the list of and return it
       * @return {Any} The old end of the list
       */
      function pop(){
        if (this.size) {
          this.size--;
          return this.sentinel.prev.unlink().data;
        }
      },
      /**
       * Remove the beginning of the list of and return it
       * @return {Any} The old beginning of the list
       */
      function shift() {
        if (this.size) {
          this.size--;
          return this.sentinel.next.unlink().data;
        }
      },
      /**
       * Remove an item from the list
       * @param  {Any}     search  value to be removed
       * @return {Boolean} whether the search was found and removed
       */
      function remove(search){
        var item = find(this, search);
        if (item) {
          item.unlink();
          return true;
        }
        return false;
      },
      /**
       * Check if a value is in the list
       * @param  {Any}     search  value to find
       * @return {Boolean}
       */
      function has(search) {
        return !!find(this, search);
      },
      function items(){
        var item = this.sentinel,
            array = [];

        while ((item = item.next) !== this.sentinel) {
          array.push(item.data);
        }

        return array;
      },
      /**
       * Removed all the items from the list
       */
      function empty(){
        var next,
            item = this.sentinel.next;

        while (item !== this.sentinel) {
          next = item.next;
          item.empty();
          item = next;
        }

        this.lastFind = null;
        this.size = 0;
        return this;
      },
      /**
       * Create a new list and add all the items from the current list into it
       * @return {DoublyLinkedList}
       */
      function clone(){
        var item = this.sentinel,
            list = new DoublyLinkedList;

        while ((item = item.next) !== this.sentinel) {
          list.push(item.data);
        }
        return list;
      },
      /**
       * Creates an iterator that will iterate the list beginning to end
       * @return {DoublyLinkedListIterator}
       */
      function __iterator__(){
        return new DoublyLinkedListIterator(this);
      }
    ]);

    return DoublyLinkedList;
  });



  /**
   * A Queue is a "first in, first out" list structure.
   *
   * dequeue
   *  \___\    front
   *       ^--|____|____|____|
   *                          ^-- \___\
   *                             enqueue
   *          |-----size-----|
   */

  // #############
  // ### Queue ###
  // #############

  exporter(function(){
    function QueueIterator(queue){
      this.queue = queue;
      this.index = queue.index;
    }

    inherit(QueueIterator, Iterator, [
      function next(){
        if (this.index === this.queue.items.length) {
          throw StopIteration;
        }
        return this.queue.items[this.index++];
      }
    ]);


    function Queue(iterable){
      this.index = this.size = 0;
      this.front = undefined;
      if (iterable != null) {
        if (iterable instanceof Queue) {
          this.items = iterable.items.slice(iterable.front);
          this.size = this.items.size;
        } else {
          this.items = [];
          iterate(iterable, this.enqueue, this);
        }
      } else {
        this.items = [];
      }
    }

    defineMethods(Queue.prototype, [
      /**
       * Add an item to the end of the queue.
       * @param  {Any}     item  new end of the queue
       * @return {Number}        the queue's new size
       */
      function enqueue(item){
        this.items.push(item);
        if (!this.size) {
          this.front = item;
        }
        return ++this.size;
      },
      /**
       * Remove an item from the front of the queue and return it.
       * @return {Any}   Old queue front
       */
      function dequeue(){
        if (this.size) {
          var item = this.items[this.index];
          this.items[this.index++] = null;
          this.front = this.items[this.index];
          this.size--;
          if (this.index === 500) {
            this.items = this.items.slice(this.index);
            this.index = 0;
          }
          return item;
        }
      },
      /**
       * Reset the queue
       */
      function empty(){
        this.size = 0;
        this.index = 0;
        this.items = [];
        return this;
      },
      /**
       * Peak at an item.
       * @param  {Number}  depth  Distance from the front of the queue
       * @return {Any}
       */
      function item(depth){
        return this.items[this.index + depth];
      },
      /**
       * Creates an iterator that will iterate the queue front to back
       * @return {QueueIterator}
       */
      function __iterator__(){
        return new QueueIterator(this);
      }
    ]);

    return Queue;
  });



  /**
   * A Stack is a "last in, first out" list structure.
   *
   *  push          pop
   *  \__\         /__/
   *     --> top --^   _
   *        |___|       |
   *        |___|     size
   *        |___|      _|
   */

  // #############
  // ### Stack ###
  // #############

  exporter(function(){
    function StackIterator(stack){
      this.stack = stack;
      this.index = stack.size;
    }

    inherit(StackIterator, Iterator, [
      function next(){
        if (!this.index) {
          throw StopIteration;
        }
        return this.stack[--this.index];
      }
    ]);

    function Stack(iterable){
      this.empty();
      if (iterable != null) {
        iterate(iterable, this.push, this);
      }
    }

    defineMethods(Stack.prototype, [
      /**
       * Add an item to the top of the stack
       * @param  {Any}     item  new stack top
       * @return {Number}        the stack's new size
       */
      function push(item){
        this.items[this.size++] = item;
        this.top = item;
        return this;
      },
      /**
       * Remove an item from the top of the stack and return it
       * @return {Any}   old stack top
       */
      function pop(){
        this.size--;
        this.top = this.items[this.size - 1];
        return this.items.pop();
      },
      /**
       * Reset the stack
       */
      function empty(){
        this.size = 0;
        this.items = [];
        this.top = undefined;
      },
      /**
       * Creates an iterator that will iterate the stack top to bottom
       * @return {StackIterator}
       */
      function __iterator__(){
        return new StackIterator(this);
      }
    ]);

    return Stack;
  });


  return exports;
})((0,eval)('this'), typeof exports === 'undefined' ? {} : exports);

