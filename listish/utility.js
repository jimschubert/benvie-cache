exports.define = define;
function define(object, props, methods){
  for (var k in props)
    Object.defineProperty(object, k, Object.getOwnPropertyDescriptor(props, k));

  methods.forEach(function(method){
    Object.defineProperty(object, method.name, {
      configurable: true,
      writable: true,
      value: method
    });
  });

  return object;
}

exports.nil = define(Object.create(null), {}, [
  function toString(){
    return '[nil]';
  },
  function inspect(){
    return '[nil]';
  }
]);


exports.wrapper = wrapper;

function wrapper(){
  var items = new WeakMap;
  return function(o){
    var map = items.get(o);
    if (!map) items.set(o, map = new Map);
    return map;
  };
}


exports.wrap = wrap;

function wrap(val){
  return { inspect: function(){ return val } };
}

