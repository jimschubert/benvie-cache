# Listish

Basic data structures implemented efficiently in JavaScript. Don't just use an array for everything, use the right tool for the job!

Linked lists are superior to arrays for data where items need to be removed, inserted, concatenated, or split. They are inferior to arrays for indexed lookup and require more memory per item. They are equivalent for iteration.

Doubly linked lists and less space efficient but provide more capabilities than a singly linked list.

A queue is much better than an array when a first-in first-out structure is needed.

A stack is similar to an array but gives direct access to the top of the stack at all times.

## Install

    npm install listish

Or include in browser (IE8 and modern browsers, potentially IE6 and 7 but untested).

## Common properties and functions

All of the below data structures share the following:

* ____iterator()____ Creates an iterator to cycle through the items
* __empty()__    Clear all the items
* __size__     Number of items contained

## LinkedList

* __append(value)__           Insert a new item at end of the list
* __pop()__                   Remove item from end of the list and return it
* __insert(value, search)__   Find a value and insert a new one after it
* __remove(search)__          Find and remove a value
* __replace(value, search)__  Find and replace a value with another one
* __has(search)__             Check if an item is in the list
* __clone()__                 Create a new list and copy every item to it

## DoublyLinkedList

* __first()__                     Peak at the first item in the list
* __last()__                      Peak at the last item in the list
* __prepend(value)__              Add an item to the beginning of the list
* __append(value)__               Add an item to the end of the list
* __insertAfter(value, search)__  Add an item to the list directly after the given item
* __insertBefore(value, search)__ Add an item to the list directly before the given item
* __replace(value, search)__      Replace an value with another value
* __pop()__                       Remove the end of the list of and return it
* __shift()__                     Remove the beginning of the list of and return it
* __remove(search)__              Remove an item from the list
* __has(search)__                 Check if a value is in the list
* __clone()__                     Create a new list and add all the items from the current list into it


##  Stack

FILO (first in, last out) data structure.

    push          pop
    \__\         /__/
       --> top --^   _
          |___|       |
          |___|     length
          |___|      _|

* __top__      Top value which will be removed by pop or pushed down by push
* __push(value)__     Add a new value top the top of the stack
* __pop()__      Remove a value from the top of the stack and return it

## Queue

FIFO (First in, first out) data structure.

    dequeue
     \___\    front
          ^--|____|____|____|
                             ^-- \___\
                                enqueue
             |-----length----|

* __front__     Front value of the list which will be removed and returned by dequeue
* __enqueue(value)__   Add an item to the end of the queue
* __dequeue()__   Remove an item from the front of the queue and return it

