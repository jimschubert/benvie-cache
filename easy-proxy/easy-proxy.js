if (typeof Proxy !== 'object') throw new Error('Proxies unsupported');

var EasyProxy = function(){
  "use strict";
  var bindbind = Function.bind.bind(Function.bind);
  var callbind = bindbind(Function.call);
  var hasOwn = callbind({}.hasOwnProperty);
  var toSource = callbind(Function.toString);
  var toString = callbind({}.toString);
  var slice = callbind([].slice);
  var apply = callbind(Function.apply);
  var normal = { writable: true, configurable: true, enumerable: true };

  function isAccessor(desc){ return desc != null && ('get' in desc || 'set' in desc) && !('value' in desc) }
  function isData(desc){ return desc != null && !('get' in desc || 'set' in desc) && 'value' in desc }

  function define(o, n, v, norm){
    normal.value = v;
    Object.defineProperty(o, n, norm ? normal : { value: v });
    return true;
  }

  function setter(desc, rcvr, val){
    var exists = desc.set != null;
    exists && desc.set.call(rcvr, val);
    return exists;
  }

  function configurable(desc){
    if (desc) desc.configurable = true;
    return desc;
  }

  function unique(a){
    return Object.keys(a.reduce(function(r,s){ r[s]=1; return r },{}));
  }

  var names = {
    getOwnPropertyDescriptor: 'describe',
    getOwnPropertyNames: 'names',
    getPropertyNames: 'names',
    defineProperty: 'define',
    delete: 'delete',
    fix: 'fix',
    keys: 'keys',
    enumerate: 'enumerate',
    hasOwn: 'owns',
    has: 'has',
    get: 'get',
    set: 'set',
    apply: 'apply',
    construct: 'construct',
  };

  var proxies = new WeakMap;

  var Reflect = {
    describe:  function(T,N  ){ return configurable(Object.getOwnPropertyDescriptor(T,N)) },
    define:    function(T,N,D){ return Object.defineProperty(T,N,D), true },
    delete:    function(T,N  ){ return delete T[N] },
    fix:       function(T    ){ return Object.freeze(T) },
    keys:      function(T    ){ return Object.keys(T) },
    names:     function(T    ){ return unique(Object.getOwnPropertyNames(T)) },
    enumerate: function(T    ){ var i=0, a=[]; for (a[i++] in T); return a },
    owns:      function(T,N  ){ return hasOwn(T, N) },
    has:       function(T,N  ){ return N in T },
    get: function(T,N,R){
      var handler = proxies.get(T);
      if (handler != null)
        return handler.get(R,N);

      if (N === '__proto__')
        return Object.getPrototypeOf(T);

      var desc = Object.getOwnPropertyDescriptor(T, N);
      if (desc == null) {
        var proto = Object.getPrototypeOf(T);
        if (proto != null) {
          //if (N === 'valueOf' || N === 'toString' && N in T)
          //  return Reflect.get(proto, N, R).bind(T);
          //else
            return Reflect.get(proto, N, R) || T[N];
        }
      } else if (isData(desc))
        return desc.value
      else if (isAccessor(desc))
        return desc.get.call(R);
    },
    set: function(T,N,V,R){
      var handler = proxies.get(T);
      if (handler != null)
        return handler.set(R,N,V);

      // existing own desc
      var ownDesc = Object.getOwnPropertyDescriptor(T,N);
      if (isData(ownDesc) && !ownDesc.writable)
        return false;
      if (isAccessor(ownDesc))
        return setter(ownDesc,R,V);

      var proto = Object.getPrototypeOf(T);

      // recurse to proto
      if (proto != null)
        return Reflect.set(proto,N,V,R);

      // new own desc
      var rcvrDesc = Object.getOwnPropertyDescriptor(R,N);
      if (isAccessor(rcvrDesc))
        return setter(rcvrDesc,R,V);
      if (isData(rcvrDesc))
        return rcvrDesc.writable ? define(R,N,V) : false;

      return Object.isExtensible(R) ? define(R,N,V, true) : false;
    },
    apply: function(T, A, R){
      return apply(T, R, A);
    },
    construct: function(T, A) {
      var handler = proxies.get(T);
      if (handler != null)
        return handler.construct(T, A);

      var proto = T.prototype;
      var instance = Object(proto) === proto ? Object.create(proto) : {};
      var result = apply(T, instance, A);
      return Object(result) === result ? result : instance;
    }
  };

  function fixargs(args){
    args.push(args.shift());
  }

  function EasyProxy(target, handler, callable){
    var metaHandler = Proxy.create({
      get: function(R, name){
        name = names[name];
        var trap = handler[name];
        if (trap == null) {
          return function(){
            var args = slice(arguments);
            if (name === 'get' || name === 'set') fixargs(args);
            return apply(Reflect[name], null, [target].concat(args));
          }
        } else {
          return function(){
            var args = slice(arguments);
            if (name === 'get' || name === 'set') fixargs(args);
            var fwd = function(){ return apply(Reflect[name], handler, [fwd.target].concat(fwd.args)) }
            fwd.target = target;
            fwd.args = args
            return apply(trap, handler, [fwd, target].concat(args));
          }
        }
      }
    });
    callable = callable || typeof target === 'function';
    if (callable) {
      var proxy = Proxy.createFunction(metaHandler,
        function(){ return metaHandler.apply(slice(arguments), this) },
        function(){ return metaHandler.construct(slice(arguments)) }
      );
    } else {
      var proxy = Proxy.create(metaHandler, Object.getPrototypeOf(target));
    }

    proxies.set(proxy, metaHandler);
    return proxy;
  }

  if (typeof module !== 'undefined') module.exports = EasyProxy;

  return EasyProxy;
}();
