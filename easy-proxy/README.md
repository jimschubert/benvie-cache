# Easy Proxy

Base on direct-proxies but simplified and without verification. EasyProxy allows for simplified creation of Proxies.

```
npm install easy-proxy
```

# Traps

Names modified for brevity, argument order normalized, `fwd` and `target` provided. A handler can be reused much more easily since the target is always provided.

```javascript
var proxied = EasyProxy(target, {
  fix:       function(fwd, target){},                  // Object.freeze(target)
  delete:    function(fwd, target, name){},            // delete target[name]

  names:     function(fwd, target){},                  // Object.getOwnPropertyNames(target)
  keys:      function(fwd, target){},                  // Object.keys(target)
  enumerate: function(fwd, target){},                  // for..in target

  owns:      function(fwd, target, name){},            // target.hasOwnProperty(name)
  has:       function(fwd, target, name){},            // name in target

  get:       function(fwd, target, name, rcvr){},      // target[name]
  set:       function(fwd, target, name, rcvr, val){}, // target[name] = value
  describe:  function(fwd, target, name){},            // Object.getOwnPropertyDescriptor(target, name)
  define:    function(fwd, target, name, desc){},      // Object.defineProperty(target, name, desc)

  apply:     function(fwd, target, args, rcvr){},      // target.apply(rcvr, args)
  construct: function(fwd, target, args){},            // new target(args...)
});
```

# Usage
Any traps not provided in the handler will default to forwarding to the target.

The first parameter, `fwd` is a function that when called will forward the action to the target and return the result. The forward function also has two properties to modify what it does

* forward.target - Change this to forward to a different target.
* forward.args - An array of the arguments that will be forwarded (the sames ones given to your function minus `target` and `rcvr`)
