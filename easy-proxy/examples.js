var EasyProxy = require('./easy-proxy');

function tracer(subject){
  return EasyProxy(subject, EasyProxy({}, {
    get: function(f, t, trap){
      return function(fwd, target, name){
        typeof name === 'string' ? console.log(trap, name) : console.log(trap);
        return forward();
      }
    }
  }));
}
