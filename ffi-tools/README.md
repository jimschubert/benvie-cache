# node ffi-tools

Some hacky experiments with the newly updated node-ffi that works cross-platform.

## Generate Interfaces from header files

Given the following win32 header file, the ffi interface can be auto-generated, resulting in the js source below.

```javascript
var ffiTools = require('ffi-tools');
var kernal32 = ffiTools.loadHeaders(new Library('kernal32'));
```

kernal32.h (relies on MSDN documentation formatting currently):

```c

// http://msdn.microsoft.com/en-us/library/windows/desktop/ms724950(v=vs.85).aspx
typedef struct _SYSTEMTIME {
  WORD wYear;
  WORD wMonth;
  WORD wDayOfWeek;
  WORD wDay;
  WORD wHour;
  WORD wMinute;
  WORD wSecond;
  WORD wMilliseconds;
} SYSTEMTIME, *PSYSTEMTIME;

// http://msdn.microsoft.com/en-us/library/windows/desktop/ms724284(v=vs.85).aspx
typedef struct _FILETIME {
  DWORD dwLowDateTime;
  DWORD dwHighDateTime;
} FILETIME, *PFILETIME;

// http://msdn.microsoft.com/en-us/library/windows/desktop/ms683175(v=vs.85).aspx
HWND WINAPI GetConsoleWindow(
  void
);

// http://msdn.microsoft.com/en-us/library/windows/desktop/aa364989(v=vs.85).aspx
DWORD WINAPI GetShortPathName(
  __in   LPCTSTR lpszLongPath,
  __out  LPTSTR lpszShortPath,
  __in   DWORD cchBuffer
);

// http://msdn.microsoft.com/en-us/library/windows/desktop/ms724320(v=vs.85).aspx
BOOL WINAPI GetFileTime(
  __in       HANDLE hFile,
  __out_opt  LPFILETIME lpCreationTime,
  __out_opt  LPFILETIME lpLastAccessTime,
  __out_opt  LPFILETIME lpLastWriteTime
);

// http://msdn.microsoft.com/en-us/library/windows/desktop/ms724390(v=vs.85).aspx
void WINAPI GetSystemTime(
  __out  LPSYSTEMTIME lpSystemTime
);
```

Produced kernal32 object:

```javascript
{ GetConsoleWindow: [Function: GetConsoleWindow],
  GetShortPathName: [Function: GetShortPathName],
  GetFileTime: [Function: GetFileTime],
  GetSystemTime: [Function: GetSystemTime] }
```


## Interface toSource to produce fixed source code bindings

To optimize performance, it's also possible to generate actual JS source code that replicates the behavior of the produced interfaces. The automated interfaces are more complicated than the code produced below but the end result is the same. Using the resulting generated source is likely more efficient (and definitely more readable).

From above, `kernel32.toSource()` produces the following fully standalone and functional code (indentation included):

```javascript
var ffi = require('node-ffi');
var structs = {};
var kernel32 = { _lib: new ffi.DynamicLibrary('kernel32.dll', ffi.DynamicLibrary.FLAGS.RTLD_NOW) };

structs.FILETIME = ffi.Struct(
  ['uint32', 'dwLowDateTime'],
  ['uint32', 'dwHighDateTime']
);

structs.SYSTEMTIME = ffi.Struct(
  ['ushort', 'wYear'],
  ['ushort', 'wMonth'],
  ['ushort', 'wDayOfWeek'],
  ['ushort', 'wDay'],
  ['ushort', 'wHour'],
  ['ushort', 'wMinute'],
  ['ushort', 'wSecond'],
  ['ushort', 'wMilliseconds']
);

kernel32.GetConsoleWindow = function(){
  var ff = new ffi.ForeignFunction(kernel32._lib.get('GetConsoleWindow'), 'ulong', [], false);
  return function GetConsoleWindow(){
    return ff();
  }
}();

kernel32.GetShortPathName = function(){
  var ff = new ffi.ForeignFunction(kernel32._lib.get('GetShortPathNameW'), 'uint32', ['string','pointer','uint32'], false);
  return function GetShortPathName(lpszLongPath, cchBuffer){
    var out = Object.create(null);
    out.lpszShortPath = new ffi.Pointer(4);
    out.ret = ff(lpszLongPath, out.lpszShortPath, cchBuffer);
    out.lpszShortPath = out.lpszShortPath.getCString();
    return out;
  }
}();

kernel32.GetFileTime = function(){
  var ff = new ffi.ForeignFunction(kernel32._lib.get('GetFileTime'), 'int8', ['ulong','pointer','pointer','pointer'], false);
  return function GetFileTime(hFile){
    var out = Object.create(null);
    out.lpCreationTime = new structs.FILETIME;
    out.lpLastAccessTime = new structs.FILETIME;
    out.lpLastWriteTime = new structs.FILETIME;
    out.ret = ff(hFile, out.lpCreationTime.pointer, out.lpLastAccessTime.pointer, out.lpLastWriteTime.pointer);
    return out;
  }
}();

kernel32.GetSystemTime = function(){
  var ff = new ffi.ForeignFunction(kernel32._lib.get('GetSystemTime'), 'void', ['pointer'], false);
  return function GetSystemTime(){
    var out = new structs.SYSTEMTIME;
    ff(out.pointer);
    return out;
  }
}();
```

## End Usage

```javascript
kernel32.GetSystemTime();
// -->
  { wYear: 2012,
    wMonth: 1,
    wDayOfWeek: 0,
    wDay: 22,
    wHour: 2,
    wMinute: 17,
    wSecond: 26,
    wMilliseconds: 871 }
