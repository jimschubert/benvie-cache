var path = require('path');
var fs = require('fs');

var ffi = require('ffi-tools');
var _ = require('rapper');
var bindings = require('./libclang-bindings');
var clang = bindings.Methods;
var Structs = bindings.Structs;
var Enums = bindings.Enums;

var TYPEKINDS = Enums.CXTypeKind;
var CURSORKINDS = Enums.CXCursorKind;
var VISITRESULT = Enums.CXChildVisitResult;


var CXString = Structs.CXString;


module.exports = {
  Index: Index,
  TranslationUnit: TranslationUnit,
  Cursor: Cursor,
  TYPEKINDS: TYPEKINDS,
  CURSORKINDS: CURSORKINDS,
  VISITRESULT: VISITRESULT
};


CXString.reify = function reify(ptr){
  return ptr.cast('CXString').toString();
}
CXString.prototype.inspect =
CXString.prototype.toString = function(){
  var string = clang.getCString(this).getCString();
  clang.disposeString(this);
  return string;
};


var indexes = [];

function Index(excludePCH, diagnostics){
  this.id = indexes.length;
  this.struct = clang.createIndex(0,0);
}

Index.prototype = {
  constructor: Index,
  toString: brand,
  set struct(v){ indexes[this.id] = v },
  get struct(){ return indexes[this.id] },
  dispose: function dispose(){
    clang.disposeIndex(this.struct);
    this.struct = null;
    this.__proto__ = null;
  },
};

var tus = [];

function TranslationUnit(index, filename, args){
  this.id = tus.length;
  if (index instanceof Cursor) {
    this.struct = clang.Cursor_getTranslationUnit(index.struct);
  } else {
    args = args ? Array.isArray(args) ? args : [args] : null;
    //this.struct = clang.createTranslationUnitFromSourceFile(index.struct, TEXT(path.resolve(filename)), args ? args.length : 0, args, 0, null);
    this.struct = clang.parseTranslationUnit(index.struct, TEXT(path.resolve(filename)), null, 0, null, 0, 0);
  }
}

function TEXT (text) {
  // malloc() the buffer for the String
  var p = new ffi.Pointer(Buffer.byteLength(text, 'utf8') + 1);
  var b = p.toBuffer();
  b.write(text, 'utf8');
  b[b.length - 1] = 0;
  return p;
}


TranslationUnit.fromAST = function fromAST(index, filename){
  var tu = Object.create(TranslationUnit.prototype);
  tu.id = tus.length;
  tu.struct = clang.createTranslationUnit(index.struct, filename);
  return tu;
}

TranslationUnit.prototype = {
  constructor: TranslationUnit,
  toString: brand,
  dispose: function dispose(){
    clang.disposeTranslationUnit(this.struct);
    this.struct = null;
    this.__proto__ = null;
  },

  set struct(v){ tus[this.id] = v },
  get struct(){ return tus[this.id] },
  get cursor(){ return new Cursor(clang.getTranslationUnitCursor(this.struct)) },
  get spelling(){ return CXString.reify(clang.getTranslationUnitSpelling(this.struct)) },

  getLocation: function getLocation(file, line, column){
    return new Location(clang.getLocation(this.struct, file, line, column));
  },
  locationForOffset: function locationForOffset(file, offset){
    return new Location(clang.getLocationForOffset(this.struct, file, offset));
  }
};


var cursors = [];

function Cursor(input){
  this.id = cursors.length;
  if (input instanceof Structs.CXCursor) {
    this.struct = input;
  } else if (!input) {
    this.struct = new Structs.CXCursor(clang.getNullCursor());
  } else if (ffi.Pointer.isPointer(input)) {
    this.struct = new Structs.CXCursor(input);
  }
}

Cursor.prototype = {
  constructor: Cursor,
  toString: brand,
  visitChildren: function visitChildren(callback){
    var visitor = new ffi.Callback('uint16', [Structs.CXCursor, Structs.CXCursor, 'pointer'], function(self, parent, data){
      return callback(new Cursor(self), new Cursor(parent));
    });
    return clang.visitChildren(this.struct.pointer, visitor, 0);
  },
  set struct         (v){ cursors[this.id] = v },
  get struct         (){ return cursors[this.id] },
  get USR            (){ return CXString.reify(clang.getCursorUSR(this.struct)) },
  get kind           (){ return clang.getCursorKind(this.struct) },
  get kindSpelling   (){ return CXString.reify(clang.getCursorKindSpelling(this.struct)) },
  get type           (){ return clang.getCursorType(this.struct) },
  get extent         (){ return clang.getCursorExtent(this.struct) },
  get access         (){ return clang.getCXXAccessSpecifier(this.struct) },
  get typedef        (){ return clang.getTypedefDeclUnderlyingType(this.struct) },
  get linkage        (){ return clang.getCursorLinkage(this.struct) },
  get spelling       (){ return CXString.reify(clang.getCursorSpelling(this.struct)) },
  get displayName    (){ return CXString.reify(clang.getCursorDisplayName(this.struct)) },
  get enumType       (){ return clang.getEnumDeclIntegerType(this.struct) },
  get enumConstant   (){ return clang.getEnumConstantDeclValue(this.struct) },
  get enumUnsigned   (){ return clang.getEnumConstantDeclUnsignedValue(this.struct) },
  get availability   (){ return clang.getCursorAvailability(this.struct) },
  get resultType     (){ return clang.getCursorResultType(this.struct) },
  get location       (){ return new Location(clang.getCursorLocation(this.struct)) },
  get semanticParent (){ return new Cursor(clang.getCursorSemanticParent(this.struct)) },
  get lexicalParent  (){ return new Cursor(clang.getCursorLexicalParent(this.struct)) },
  get canonical      (){ return new Cursor(clang.getCanonicalCursor(this.struct)) },
  get definition     (){ return new Cursor(clang.getCursorDefinition(this.struct)) },
  get referenced     (){ return new Cursor(clang.getCursorReferenced(this.struct)) },
  get translationUnit(){ return new TranslationUnit(this) },
  //get includedFile   (){ return new File(clang.getIncludedFile(this.struct)) },
};


var types = [];

function Type(input){
  this.id = types.length;
  if (input instanceof Structs.CXType) {
    this.struct = input;
  } else if (ffi.Pointer.isPointer(input)) {
    this.struct = new Structs.CXType(input);
  }
}

Type.prototype = {
  constructor: Type,
  toString: brand,
  set struct         (v){ types[this.id] = v },
  get struct         (){ return types[this.id] },
  get kind           (){ return this.struct.kind },
  get spelling       (){ return CXString.reify(clang.getTypeKindSpelling(this.struct.kind)) },
  get length         (){ return clang.getNumElements(this.struct) },
  get arraySize      (){ return clang.getArraySize(this.struct) },
  get isConst        (){ return clang.isConstQualifiedType(this.struct) },
  get isVolatile     (){ return clang.isVolatileQualifiedType(this.struct) },
  get isRestrict     (){ return clang.isRestrictQualifiedType(this.struct) },
  get callingConv    (){ return clang.getFunctionTypeCallingConv(this.struct) },
  get isPODType      (){ return clang.isPODType(this.struct) },
  get arrayType      (){ return new Type(clang.getArrayElementType(this.struct)) },
  get result         (){ return new Type(clang.getResultType(this.struct)) },
  get canonical      (){ return new Type(clang.getCanonicalType(this.struct)) },
  get elementType    (){ return new Type(clang.getElementType(this.struct)) },
  get declaration    (){ return new Cursor(clang.getTypeDeclaration(this.struct)) },
  get pointeeType    (){ return new Type(clang.getPointeeType(this.struct)) },
  get argTypes       (){
    var count = clang.getNumArgTypes(this.struct);
    var out = [];
    for (var i=0; i < count; i++) {
      out.push(new Type(clang.getArgType(this.struct, i)));
    }
    return out;
  },
};


var locations = [];

function Location(input){
  this.id = locations.length;
  if (input instanceof Structs.CXSourceLocation) {
    this.struct = input;
  } else if (ffi.Pointer.isPointer(input)) {
    this.struct = new Structs.CXSourceLocation(input);
  } else if (!input) {
    this.struct = new Structs.CXSourceLocation(clang.getNullLocation());
  }
}

Location.prototype = {
  constructor: Location,
  toString: brand,
  set struct         (v){ locations[this.id] = v },
  get struct         (){ return locations[this.id] },
  get presumed(){
    var loc = { file: new ffi.Pointer(30),
                line: new ffi.Pointer(4),
                column: new ffi.Pointer(4) };
    clang.getPresumedLocation(this.struct, loc.file, loc.line, loc.column);
    loc.file = CXString.reify(loc.file);
    loc.line = loc.line.getUInt16();
    loc.column = loc.column.getUInt16()
    return loc;
  },
  get spelling(){
    var loc =  { file: new ffi.Pointer(30),
                 line: new ffi.Pointer(4),
                 column: new ffi.Pointer(4),
                 offset: new ffi.Pointer(4) };
    clang.getSpellingLocation(this.struct, loc.file, loc.line, loc.column, loc.offset);
    loc.file = CXString.reify(loc.file);
    loc.line = loc.line.getUInt16();
    loc.column = loc.column.getUInt16();
    loc.offset = loc.offset.getUInt16();
    return loc;
  }
}

function brand(){ return '[object '+this.constructor.name+']' }




