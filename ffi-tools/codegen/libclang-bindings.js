var ffi = require('ffi-tools');

exports.Enums = {};
exports.Structs = {};
exports.Methods = {};


function Enum(name, def){
 return exports.Enums[name] = new ffi.Enum(name, def);
}

function Struct(name, def){
 return exports.Structs[name] = new ffi.Struct(name, def);
}

function Method(name, ret, params){
 return exports.Methods[name.slice(6)] = new ffi.ForeignFunction(name, 'libclang', ret, params);
}


function TypeDef(name, ffiType, type){
  return ffiType;
}

var
 time_t = TypeDef('time_t', 'int32'),
 CXIndex = TypeDef('CXIndex', 'pointer'),
 CXTranslationUnit = TypeDef('CXTranslationUnit', 'pointer'),
 CXClientData = TypeDef('CXClientData', 'pointer'),
 CXFile = TypeDef('CXFile', 'pointer'),
 CXDiagnostic = TypeDef('CXDiagnostic', 'pointer'),
 CXDiagnosticSet = TypeDef('CXDiagnosticSet', 'pointer'),
 CXCursorSet = TypeDef('CXCursorSet', 'pointer'),
 CXCursorVisitor = TypeDef('CXCursorVisitor', 'pointer'),
 CXCompletionString = TypeDef('CXCompletionString', 'pointer'),
 CXInclusionVisitor = TypeDef('CXInclusionVisitor', 'pointer'),
 CXRemapping = TypeDef('CXRemapping', 'pointer'),
 CXIdxClientEntity = TypeDef('CXIdxClientEntity', 'pointer'),
 CXIdxClientContainer = TypeDef('CXIdxClientContainer', 'pointer'),
 CXIndexAction = TypeDef('CXIndexAction', 'pointer');

var CXAvailabilityKind = Enum('CXAvailabilityKind', {
 Available: 0,
 Deprecated: 1,
 NotAvailable: 2,
 NotAccessible: 3
});

var CXDiagnosticSeverity = Enum('CXDiagnosticSeverity', {
 Ignored: 0,
 Note: 1,
 Warning: 2,
 Error: 3,
 Fatal: 4
});

var CXLoadDiag_Error = Enum('CXLoadDiag_Error', {
 None: 0,
 Unknown: 1,
 CannotLoad: 2,
 InvalidFile: 3
});

var CXDiagnosticDisplayOptions = Enum('CXDiagnosticDisplayOptions', {
 SourceLocation: 0x01,
 Column: 0x02,
 SourceRanges: 0x04,
 Option: 0x08,
 CategoryId: 0x10,
 CategoryName: 0x20
});

var CXTranslationUnit_Flags = Enum('CXTranslationUnit_Flags', {
 None: 0x0,
 DetailedPreprocessingRecord: 0x01,
 Incomplete: 0x02,
 PrecompiledPreamble: 0x04,
 CacheCompletionResults: 0x08,
 CxxPrecompiledPreamble: 0x10,
 CxxChainedPch: 0x20,
 NestedMacroExpansions: 0x40,
 NestedMacroInstantiations: 8
});

var CXSaveTranslationUnit_Flags = Enum('CXSaveTranslationUnit_Flags', {
 None: 0x0
});

var CXSaveError = Enum('CXSaveError', {
 None: 0,
 Unknown: 1,
 TranslationErrors: 2,
 InvalidTu: 3
});

var CXReparse_Flags = Enum('CXReparse_Flags', {
 None: 0x0
});

var CXTUResourceUsageKind = Enum('CXTUResourceUsageKind', {
 Ast: 1,
 Identifiers: 2,
 Selectors: 3,
 GlobalCompletionResults: 4,
 ManagerContentCache: 5,
 AstSideTables: 6,
 ManagerMembufferMalloc: 7,
 ManagerMembufferMMap: 8,
 ExternalAstMembufferMalloc: 9,
 ExternalAstMembufferMMap: 10,
 Preprocessor: 11,
 PreprocessingRecord: 12,
 ManagerDataStructures: 13,
 PreprocessorHeaderSearch: 14,
 MemoryBytesBegin: 14,
 MemoryBytesEnd: 15,
 First: 16,
 Last: 17
});

var CXCursorKind = Enum('CXCursorKind', {
 UnexposedDecl: 1,
 StructDecl: 2,
 UnionDecl: 3,
 ClassDecl: 4,
 EnumDecl: 5,
 FieldDecl: 6,
 EnumConstantDecl: 7,
 FunctionDecl: 8,
 VarDecl: 9,
 ParmDecl: 10,
 ObjInterfaceDecl: 11,
 ObjCategoryDecl: 12,
 ObjProtocolDecl: 13,
 ObjPropertyDecl: 14,
 ObjIvarDecl: 15,
 ObjInstanceMethodDecl: 16,
 ObjClassMethodDecl: 17,
 ObjImplementationDecl: 18,
 ObjCategoryImplDecl: 19,
 TypedefDecl: 20,
 CxxMethod: 21,
 Namespace: 22,
 LinkageSpec: 23,
 Constructor: 24,
 Destructor: 25,
 ConversionFunction: 26,
 TemplateTypeParameter: 27,
 NonTypeTemplateParameter: 28,
 TemplateTemplateParameter: 29,
 FunctionTemplate: 30,
 ClassTemplate: 31,
 ClassTemplatePartialSpecialization: 32,
 NamespaceAlias: 33,
 UsingDirective: 34,
 UsingDeclaration: 35,
 TypeAliasDecl: 36,
 ObjSynthesizeDecl: 37,
 ObjDynamicDecl: 38,
 CxxAccessSpecifier: 39,
 FirstDecl: 39,
 LastDecl: 40,
 FirstRef: 40,
 ObjSuperClassRef: 40,
 ObjProtocolRef: 41,
 ObjClassRef: 42,
 TypeRef: 43,
 CxxBaseSpecifier: 44,
 TemplateRef: 45,
 NamespaceRef: 46,
 MemberRef: 47,
 LabelRef: 48,
 OverloadedDeclRef: 49,
 VariableRef: 50,
 LastRef: 53,
 FirstInvalid: 70,
 InvalidFile: 70,
 NoDeclFound: 71,
 NotImplemented: 72,
 InvalidCode: 73,
 LastInvalid: 59,
 FirstExpr: 100,
 UnexposedExpr: 100,
 DeclRefExpr: 101,
 MemberRefExpr: 102,
 CallExpr: 103,
 ObjMessageExpr: 104,
 BlockExpr: 105,
 IntegerLiteral: 106,
 FloatingLiteral: 107,
 ImaginaryLiteral: 108,
 StringLiteral: 109,
 CharacterLiteral: 110,
 ParenExpr: 111,
 UnaryOperator: 112,
 ArraySubscriptExpr: 113,
 BinaryOperator: 114,
 CompoundAssignOperator: 115,
 ConditionalOperator: 116,
 StyleCastExpr: 117,
 CompoundLiteralExpr: 118,
 InitListExpr: 119,
 AddrLabelExpr: 120,
 StmtExpr: 121,
 GenericSelectionExpr: 122,
 GnuNullExpr: 123,
 CxxStaticCastExpr: 124,
 CxxDynamicCastExpr: 125,
 CxxReinterpretCastExpr: 126,
 CxxConstCastExpr: 127,
 CxxFunctionalCastExpr: 128,
 CxxTypeidExpr: 129,
 CxxBoolLiteralExpr: 130,
 CxxNullPtrLiteralExpr: 131,
 CxxThisExpr: 132,
 CxxThrowExpr: 133,
 CxxNewExpr: 134,
 CxxDeleteExpr: 135,
 UnaryExpr: 136,
 ObjStringLiteral: 137,
 ObjEncodeExpr: 138,
 ObjSelectorExpr: 139,
 ObjProtocolExpr: 140,
 ObjBridgedCastExpr: 141,
 PackExpansionExpr: 142,
 SizeOfPackExpr: 143,
 LambdaExpr: 144,
 LastExpr: 106,
 FirstStmt: 200,
 UnexposedStmt: 200,
 LabelStmt: 201,
 CompoundStmt: 202,
 CaseStmt: 203,
 DefaultStmt: 204,
 IfStmt: 205,
 SwitchStmt: 206,
 WhileStmt: 207,
 DoStmt: 208,
 ForStmt: 209,
 GotoStmt: 210,
 IndirectGotoStmt: 211,
 ContinueStmt: 212,
 BreakStmt: 213,
 ReturnStmt: 214,
 AsmStmt: 215,
 ObjAtTryStmt: 216,
 ObjAtCatchStmt: 217,
 ObjAtFinallyStmt: 218,
 ObjAtThrowStmt: 219,
 ObjAtSynchronizedStmt: 220,
 ObjAutoreleasePoolStmt: 221,
 ObjForCollectionStmt: 222,
 CxxCatchStmt: 223,
 CxxTryStmt: 224,
 CxxForRangeStmt: 225,
 SehTryStmt: 226,
 SehExceptStmt: 227,
 SehFinallyStmt: 228,
 NullStmt: 230,
 DeclStmt: 231,
 LastStmt: 139,
 TranslationUnit: 300,
 FirstAttr: 400,
 UnexposedAttr: 400,
 IbActionAttr: 401,
 IbOutletAttr: 402,
 IbOutletCollectionAttr: 403,
 CxxFinalAttr: 404,
 CxxOverrideAttr: 405,
 AnnotateAttr: 406,
 AsmLabelAttr: 407,
 LastAttr: 150,
 PreprocessingDirective: 500,
 MacroDefinition: 501,
 MacroExpansion: 502,
 MacroInstantiation: 154,
 InclusionDirective: 503,
 FirstPreprocessing: 156,
 LastPreprocessing: 157
});

var CXLinkageKind = Enum('CXLinkageKind', {
 Invalid: 0,
 NoLinkage: 1,
 Internal: 2,
 UniqueExternal: 3,
 External: 4
});

var CXLanguageKind = Enum('CXLanguageKind', {
 Invalid: 0,
 C: 1,
 ObjC: 2,
 PlusPlus: 3
});

var CXTypeKind = Enum('CXTypeKind', {
 Invalid: 0,
 Unexposed: 1,
 Void: 2,
 Bool: 3,
 CharU: 4,
 UChar: 5,
 Char16: 6,
 Char32: 7,
 UShort: 8,
 UInt: 9,
 ULong: 10,
 ULongLong: 11,
 UInt128: 12,
 CharS: 13,
 SChar: 14,
 WChar: 15,
 Short: 16,
 Int: 17,
 Long: 18,
 LongLong: 19,
 Int128: 20,
 Float: 21,
 Double: 22,
 LongDouble: 23,
 NullPtr: 24,
 Overload: 25,
 Dependent: 26,
 ObjId: 27,
 ObjClass: 28,
 ObjSel: 29,
 FirstBuiltin: 30,
 LastBuiltin: 31,
 Complex: 100,
 Pointer: 101,
 BlockPointer: 102,
 LValueReference: 103,
 RValueReference: 104,
 Record: 105,
 Enum: 106,
 Typedef: 107,
 ObjInterface: 108,
 ObjObjectPointer: 109,
 FunctionNoProto: 110,
 FunctionProto: 111,
 ConstantArray: 112,
 Vector: 113
});

var CXCallingConv = Enum('CXCallingConv', {
 Default: 0,
 C: 1,
 X86StdCall: 2,
 X86FastCall: 3,
 X86ThisCall: 4,
 X86Pascal: 5,
 Aapcs: 6,
 AapcsVfp: 7,
 Invalid: 100,
 Unexposed: 200
});

var CX_CXXAccessSpecifier = Enum('CX_CXXAccessSpecifier', {
 InvalidSpecifier: 0,
 Public: 1,
 Protected: 2,
 Private: 3
});

var CXChildVisitResult = Enum('CXChildVisitResult', {
 Break: 0,
 Continue: 1,
 Recurse: 2
});

var CXNameRefFlags = Enum('CXNameRefFlags', {
 RangeWantQualifier: 0x1,
 RangeWantTemplateArgs: 0x2,
 RangeWantSinglePiece: 0x4
});

var CXTokenKind = Enum('CXTokenKind', {
 Punctuation: 0,
 Keyword: 1,
 Identifier: 2,
 Literal: 3,
 Comment: 4
});

var CXCompletionChunkKind = Enum('CXCompletionChunkKind', {
 Optional: 0,
 TypedText: 1,
 Text: 2,
 Placeholder: 3,
 Informative: 4,
 CurrentParameter: 5,
 LeftParen: 6,
 RightParen: 7,
 LeftBracket: 8,
 RightBracket: 9,
 LeftBrace: 10,
 RightBrace: 11,
 LeftAngle: 12,
 RightAngle: 13,
 Comma: 14,
 ResultType: 15,
 Colon: 16,
 SemiColon: 17,
 Equal: 18,
 HorizontalSpace: 19,
 VerticalSpace: 20
});

var CXCodeComplete_Flags = Enum('CXCodeComplete_Flags', {
 IncludeMacros: 0x01,
 IncludePatterns: 0x02
});

var CXCompletionContext = Enum('CXCompletionContext', {
 Unexposed: 0,
 AnyType: 1,
 AnyValue: 2,
 ObjObjectValue: 3,
 ObjSelectorValue: 4,
 CxxClassTypeValue: 5,
 DotMemberAccess: 6,
 ArrowMemberAccess: 7,
 ObjPropertyAccess: 8,
 EnumTag: 9,
 UnionTag: 10,
 StructTag: 11,
 ClassTag: 12,
 Namespace: 13,
 NestedNameSpecifier: 14,
 ObjInterface: 15,
 ObjProtocol: 16,
 ObjCategory: 17,
 ObjInstanceMessage: 18,
 ObjClassMessage: 19,
 ObjSelectorName: 20,
 MacroName: 21,
 NaturalLanguage: 22,
 Unknown: 23
});

var CXVisitorResult = Enum('CXVisitorResult', {
 Break: 0,
 Continue: 1
});

var CXIdxEntityKind = Enum('CXIdxEntityKind', {
 Unexposed: 0,
 Typedef: 1,
 Function: 2,
 Variable: 3,
 Field: 4,
 EnumConstant: 5,
 ObjClass: 6,
 ObjProtocol: 7,
 ObjCategory: 8,
 ObjInstanceMethod: 9,
 ObjClassMethod: 10,
 ObjProperty: 11,
 ObjIvar: 12,
 Enum: 13,
 Struct: 14,
 Union: 15,
 CxxClass: 16,
 CxxNamespace: 17,
 CxxNamespaceAlias: 18,
 CxxStaticVariable: 19,
 CxxStaticMethod: 20,
 CxxInstanceMethod: 21,
 CxxConstructor: 22,
 CxxDestructor: 23,
 CxxConversionFunction: 24,
 CxxTypeAlias: 25
});

var CXIdxEntityLanguage = Enum('CXIdxEntityLanguage', {
 None: 0,
 C: 1,
 ObjC: 2,
 Cxx: 3
});

var CXIdxEntityCXXTemplateKind = Enum('CXIdxEntityCXXTemplateKind', {
 NonTemplate: 0,
 Template: 1,
 PartialSpecialization: 2,
 Specialization: 3
});

var CXIdxAttrKind = Enum('CXIdxAttrKind', {
 Unexposed: 0,
 IbAction: 1,
 IbOutlet: 2,
 IbOutletCollection: 3
});

var CXIdxObjCContainerKind = Enum('CXIdxObjCContainerKind', {
 ForwardRef: 0,
 Interface: 1,
 Implementation: 2
});

var CXIdxEntityRefKind = Enum('CXIdxEntityRefKind', {
 Direct: 1,
 Implicit: 2
});

var CXIndexOptFlags = Enum('CXIndexOptFlags', {
 None: 0x0,
 SuppressRedundantRefs: 0x1,
 FunctionLocalSymbols: 0x2,
 ImplicitTemplateInstantiations: 0x4
});



var CXUnsavedFile = Struct('CXUnsavedFile', {
 Filename: 'pointer',
 Contents: 'pointer',
 Length: 'uint32'
});

var CXString = Struct('CXString', {
 data: 'pointer',
 private_flags: 'uint8'
});

var CXSourceLocation = Struct('CXSourceLocation', {
 ptr_data: 'pointer',
 int_data: 'uint16'
});

var CXSourceRange = Struct('CXSourceRange', {
 ptr_data: 'pointer',
 begin_int_data: 'uint16',
 end_int_data: 'uint16'
});

var CXTUResourceUsageEntry = Struct('CXTUResourceUsageEntry', {
 kind: CXTUResourceUsageKind,
 amount: 'uint32'
});

var CXTUResourceUsage = Struct('CXTUResourceUsage', {
 data: 'pointer',
 numEntries: 'uint16',
 entries: 'pointer'
});

var CXCursor = Struct('CXCursor', {
 kind: CXCursorKind,
 xdata: 'int16',
 data: 'pointer'
});


var CXType = Struct('CXType', {
 kind: CXTypeKind,
 data: 'pointer'
});

var CXToken = Struct('CXToken', {
 int_data: 'pointer',
 ptr_data: 'pointer'
});

var CXCompletionResult = Struct('CXCompletionResult', {
 CursorKind: CXCursorKind,
 CompletionString: CXCompletionString
});

var CXCodeCompleteResults = Struct('CXCodeCompleteResults', {
 Results: 'pointer',
 NumResults: 'uint16'
});

var CXCursorAndRangeVisitor = Struct('CXCursorAndRangeVisitor', {
 context: 'pointer',
 visit: 'pointer'
});

var CXIdxLoc = Struct('CXIdxLoc', {
 ptr_data: 'pointer',
 int_data: 'uint16'
});

var CXIdxIncludedFileInfo = Struct('CXIdxIncludedFileInfo', {
 hashLoc: CXIdxLoc,
 filename: 'pointer',
 file: CXFile,
 isImport: 'int16',
 isAngled: 'int16'
});

var CXIdxImportedASTFileInfo = Struct('CXIdxImportedASTFileInfo', {
 file: 'pointer',
 loc: 'pointer',
 isModule: 'int16'
});

var CXIdxAttrInfo = Struct('CXIdxAttrInfo', {
 kind: CXIdxAttrKind,
 cursor: CXCursor,
 loc: CXIdxLoc
});

var CXIdxEntityInfo = Struct('CXIdxEntityInfo', {
 kind: CXIdxEntityKind,
 templateKind: CXIdxEntityCXXTemplateKind,
 lang: CXIdxEntityLanguage,
 name: 'pointer',
 USR: 'pointer',
 cursor: 'pointer',
 attributes: 'pointer',
 numAttributes: 'uint16'
});

var CXIdxContainerInfo = Struct('CXIdxContainerInfo', {
 cursor: 'pointer'
});

var CXIdxIBOutletCollectionAttrInfo = Struct('CXIdxIBOutletCollectionAttrInfo', {
 attrInfo: 'pointer',
 objcClass: 'pointer',
 classCursor: 'pointer',
 classLoc: 'pointer'
});

var CXIdxDeclInfo = Struct('CXIdxDeclInfo', {
 entityInfo: 'pointer',
 cursor: 'pointer',
 loc: 'pointer',
 semanticContainer: 'pointer',
 lexicalContainer: 'pointer',
 isRedeclaration: 'int16',
 isDefinition: 'int16',
 isContainer: 'int16',
 declAsContainer: 'pointer',
 isImplicit: 'int16',
 attributes: 'pointer',
 numAttributes: 'uint16'
});

var CXIdxObjCContainerDeclInfo = Struct('CXIdxObjCContainerDeclInfo', {
 declInfo: 'pointer',
 kind: CXIdxObjCContainerKind
});

var CXIdxBaseClassInfo = Struct('CXIdxBaseClassInfo', {
 base: 'pointer',
 cursor: CXCursor,
 loc: CXIdxLoc
});

var CXIdxObjCProtocolRefInfo = Struct('CXIdxObjCProtocolRefInfo', {
 protocol: 'pointer',
 cursor: CXCursor,
 loc: CXIdxLoc
});

var CXIdxObjCProtocolRefListInfo = Struct('CXIdxObjCProtocolRefListInfo', {
 protocols: 'pointer',
 numProtocols: 'uint16'
});

var CXIdxObjCInterfaceDeclInfo = Struct('CXIdxObjCInterfaceDeclInfo', {
 containerInfo: 'pointer',
 superInfo: 'pointer',
 protocols: 'pointer'
});

var CXIdxObjCCategoryDeclInfo = Struct('CXIdxObjCCategoryDeclInfo', {
 containerInfo: 'pointer',
 objcClass: 'pointer',
 classCursor: CXCursor,
 classLoc: CXIdxLoc,
 protocols: 'pointer'
});

var CXIdxCXXClassDeclInfo = Struct('CXIdxCXXClassDeclInfo', {
 declInfo: 'pointer',
 bases: 'pointer',
 numBases: 'uint16'
});

var CXIdxEntityRefInfo = Struct('CXIdxEntityRefInfo', {
 kind: CXIdxEntityRefKind,
 cursor: CXCursor,
 loc: CXIdxLoc,
 referencedEntity: 'pointer',
 parentEntity: 'pointer',
 container: 'pointer'
});

var IndexerCallbacks = Struct('IndexerCallbacks', {
 abortQuery: 'pointer',
 diagnostic: 'pointer',
 enteredMainFile: 'pointer',
 ppIncludedFile: 'pointer',
 importedASTFile: 'pointer',
 startedTranslationUnit: 'pointer',
 indexDeclaration: 'pointer',
 indexEntityReference: 'pointer'
});


Method('clang_visitChildren', 'void', { parent: 'pointer', visitor: 'pointer', client_data: 'int32' });
Method('clang_getCursorLinkage', CXLinkageKind, { cursor: 'pointer' });
Method('clang_getCursorAvailability', CXAvailabilityKind, { cursor: 'pointer' });
Method('clang_getCursorLanguage', CXLanguageKind, { cursor: 'pointer' });
Method('clang_getCursorSpelling', 'pointer', { CXCursor: 'pointer' });
Method('clang_getCursorDisplayName', 'pointer', { CXCursor: 'pointer' });
Method('clang_getCursorReferenced', 'pointer', { CXCursor: 'pointer' });
Method('clang_getCursorDefinition', 'pointer', { CXCursor: 'pointer' });
Method('clang_isCursorDefinition', 'void', { CXCursor: 'pointer' });
Method('clang_getCanonicalCursor', 'pointer', { CXCursor: 'pointer' });
Method('clang_Cursor_getTranslationUnit', 'pointer', { CXCursor: 'pointer' });
Method('clang_getCursorSemanticParent', 'pointer', { cursor: 'pointer' });
Method('clang_getCursorLexicalParent', 'pointer', { cursor: 'pointer' });
Method('clang_getCursor', 'pointer', { CXSourceLocation: 'pointer' });
Method('clang_getCursorLocation', 'pointer' , { CXCursor: 'pointer' });
Method('clang_getCursorExtent', 'pointer' , { CXCursor: 'pointer' });
Method('clang_getCursorType', 'pointer', { C: 'pointer' });

Method('clang_createIndex', 'pointer', { excludeDeclarationsFromPCH: 'int16', displayDiagnostics: 'int16' });
Method('clang_createTranslationUnitFromSourceFile', 'pointer', { CIdx: 'pointer', source_filename: 'pointer', num_clang_command_line_args: 'int16', clang_command_line_args: 'pointer', num_unsaved_files: 'uint16', unsaved_files: 'pointer' });
Method('clang_getCString', 'pointer', { string: 'pointer' });
Method('clang_disposeString', 'void', { string: 'pointer' });
Method('clang_getTranslationUnitSpelling', 'pointer', { CTUnit: 'pointer' });
Method('clang_getTranslationUnitCursor', 'pointer', { CXTranslationUnit: 'pointer' });
Method('clang_getClangVersion', 'pointer', {  });
Method('clang_disposeIndex', 'void', { index: 'pointer' });
Method('clang_getFileName', 'pointer', { SFile: 'pointer' });
Method('clang_getFileTime', time_t, { SFile: 'pointer' });
Method('clang_isFileMultipleIncludeGuarded', 'void', { tu: 'pointer', file: 'pointer' });
Method('clang_getFile', 'pointer', { tu: 'pointer', file_name: 'pointer' });
Method('clang_getNullLocation', 'pointer', {  });
Method('clang_equalLocations', 'void', { loc1: 'pointer', loc2: 'pointer' });
Method('clang_getLocation', 'pointer', { tu: 'pointer', file: 'pointer', line: 'uint16', column: 'uint16' });
Method('clang_getLocationForOffset', 'pointer', { tu: 'pointer', file: 'pointer', offset: 'uint16' });
Method('clang_getNullRange', 'pointer', {  });
Method('clang_getRange', 'pointer', { begin: 'pointer', end: 'pointer' });
Method('clang_equalRanges', 'void', { range1: CXSourceRange, range2: CXSourceRange });
Method('clang_Range_isNull', 'void', { range: CXSourceRange });
Method('clang_getExpansionLocation', 'void', { location: 'pointer', file: 'pointer', line: 'pointer', column: 'pointer', offset: 'pointer' });
Method('clang_getPresumedLocation', 'void', { location: 'pointer', filename: 'pointer', line: 'pointer', column: 'pointer' });
Method('clang_getInstantiationLocation', 'void', { location: 'pointer', file: 'pointer', line: 'pointer', column: 'pointer', offset: 'pointer' });
Method('clang_getSpellingLocation', 'void', { location: 'pointer', file: 'pointer', line: 'pointer', column: 'pointer', offset: 'pointer' });
Method('clang_getRangeStart', 'pointer', { range: CXSourceRange });
Method('clang_getRangeEnd', 'pointer', { range: CXSourceRange });
Method('clang_getNumDiagnosticsInSet', 'void', { Diags: CXDiagnosticSet });
Method('clang_getDiagnosticInSet', CXDiagnostic, { Diags: CXDiagnosticSet, Index: 'uint16' });
Method('clang_loadDiagnostics', CXDiagnosticSet, { file: 'pointer', error: 'pointer', errorString: 'pointer' });
Method('clang_disposeDiagnosticSet', 'void', { Diags: CXDiagnosticSet });
Method('clang_getChildDiagnostics', CXDiagnosticSet, { D: CXDiagnostic });
Method('clang_getNumDiagnostics', 'void', { Unit: CXTranslationUnit });
Method('clang_getDiagnostic', CXDiagnostic, { Unit: CXTranslationUnit, Index: 'uint16' });
Method('clang_getDiagnosticSetFromTU', CXDiagnosticSet, { Unit: CXTranslationUnit });
Method('clang_disposeDiagnostic', 'void', { Diagnostic: CXDiagnostic });
Method('clang_formatDiagnostic', CXString, { Diagnostic: CXDiagnostic, Options: 'uint16' });
Method('clang_defaultDiagnosticDisplayOptions', 'uint', {  });
Method('clang_getDiagnosticSeverity', CXDiagnosticSeverity, { CXDiagnostic: CXDiagnostic });
Method('clang_getDiagnosticLocation', CXSourceLocation, { CXDiagnostic: CXDiagnostic });
Method('clang_getDiagnosticSpelling', CXString, { CXDiagnostic: CXDiagnostic });
Method('clang_getDiagnosticOption', CXString, { Diag: CXDiagnostic, Disable: 'pointer' });
Method('clang_getDiagnosticCategory', 'void', { CXDiagnostic: CXDiagnostic });
Method('clang_getDiagnosticCategoryName', CXString, { Category: 'uint16' });
Method('clang_getDiagnosticNumRanges', 'void', { CXDiagnostic: CXDiagnostic });
Method('clang_getDiagnosticRange', CXSourceRange, { Diagnostic: CXDiagnostic, Range: 'uint16' });
Method('clang_getDiagnosticNumFixIts', 'void', { Diagnostic: CXDiagnostic });
Method('clang_getDiagnosticFixIt', 'pointer', { Diagnostic: CXDiagnostic, FixIt: 'uint16', ReplacementRange: 'pointer' });
Method('clang_createTranslationUnit', 'pointer', { CXIndex: CXIndex, ast_filename: 'pointer' });
Method('clang_defaultEditingTranslationUnitOptions', 'uint16', {  });
Method('clang_parseTranslationUnit', 'pointer', { CIdx: 'pointer', source_filename: 'pointer', command_line_args: 'pointer', num_command_line_args: 'int16', unsaved_files: 'pointer', num_unsaved_files: 'uint16', options: 'uint16' });
Method('clang_defaultSaveOptions', 'void', { TU: CXTranslationUnit });
Method('clang_saveTranslationUnit', 'void', { TU: CXTranslationUnit, FileName: 'pointer', options: 'uint16' });
Method('clang_disposeTranslationUnit', 'void', { CXTranslationUnit: CXTranslationUnit });
Method('clang_defaultReparseOptions', 'uint16', { TU: CXTranslationUnit });
Method('clang_reparseTranslationUnit', 'void', { TU: CXTranslationUnit, num_unsaved_files: 'uint16', unsaved_files: 'pointer', options: 'uint16' });
Method('clang_getTUResourceUsageName', 'void', { kind: CXTUResourceUsageKind });
Method('clang_getCXTUResourceUsage', CXTUResourceUsage, { TU: CXTranslationUnit });
Method('clang_disposeCXTUResourceUsage', 'void', { usage: CXTUResourceUsage });
Method('clang_getNullCursor', CXCursor, {  });
Method('clang_equalCursors', 'void', { CXCursor: CXCursor });
Method('clang_hashCursor', 'void', { CXCursor: CXCursor });
Method('clang_getCursorKind', CXCursorKind, { CXCursor: CXCursor });
Method('clang_isDeclaration', 'void', { CXCursorKind: CXCursorKind });
Method('clang_isReference', 'void', { CXCursorKind: CXCursorKind });
Method('clang_isExpression', 'void', { CXCursorKind: CXCursorKind });
Method('clang_isStatement', 'void', { CXCursorKind: CXCursorKind });
Method('clang_isAttribute', 'void', { CXCursorKind: CXCursorKind });
Method('clang_isInvalid', 'void', { CXCursorKind: CXCursorKind });
Method('clang_isTranslationUnit', 'void', { CXCursorKind: CXCursorKind });
Method('clang_isPreprocessing', 'void', { CXCursorKind: CXCursorKind });
Method('clang_isUnexposed', 'void', { CXCursorKind: CXCursorKind });
Method('clang_createCXCursorSet', CXCursorSet, {  });
Method('clang_disposeCXCursorSet', 'void', { cset: CXCursorSet });
Method('clang_CXCursorSet_contains', 'void', { cset: CXCursorSet, cursor: CXCursor });
Method('clang_CXCursorSet_insert', 'void', { cset: CXCursorSet, cursor: CXCursor });
Method('clang_getOverriddenCursors', 'void', { cursor: CXCursor, overridden: 'pointer', num_overridden: 'pointer' });
Method('clang_disposeOverriddenCursors', 'void', { overridden: 'pointer' });
Method('clang_getIncludedFile', CXFile, { cursor: CXCursor });
Method('clang_getTypedefDeclUnderlyingType', CXType, { C: CXCursor });
Method('clang_getEnumDeclIntegerType', CXType, { C: CXCursor });
Method('clang_getEnumConstantDeclValue', 'void', { C: CXCursor });
Method('clang_getEnumConstantDeclUnsignedValue', 'void', { C: CXCursor });
Method('clang_equalTypes', 'void', { A: CXType, B: CXType });
Method('clang_getCanonicalType', CXType, { T: CXType });
Method('clang_isConstQualifiedType', 'void', { T: CXType });
Method('clang_isVolatileQualifiedType', 'void', { T: CXType });
Method('clang_isRestrictQualifiedType', 'void', { T: CXType });
Method('clang_getPointeeType', CXType, { T: CXType });
Method('clang_getTypeDeclaration', CXCursor, { T: CXType });
Method('clang_getDeclObjCTypeEncoding', CXString, { C: CXCursor });
Method('clang_getTypeKindSpelling', 'pointer', { K: 'pointer' });
Method('clang_getFunctionTypeCallingConv', CXCallingConv, { T: CXType });
Method('clang_getResultType', CXType, { T: CXType });
Method('clang_getNumArgTypes', 'void', { T: 'pointer' });
Method('clang_getArgType', 'pointer', { T: 'pointer', i: 'uint16' });
Method('clang_isFunctionTypeVariadic', 'void', { T: CXType });
Method('clang_getCursorResultType', CXType, { C: CXCursor });
Method('clang_isPODType', 'void', { T: CXType });
Method('clang_getElementType', CXType, { T: CXType });
Method('clang_getNumElements', 'void', { T: CXType });
Method('clang_getArrayElementType', CXType, { T: CXType });
Method('clang_getArraySize', 'void', { T: CXType });
Method('clang_isVirtualBase', 'void', { CXCursor: CXCursor });
Method('clang_getCXXAccessSpecifier', CX_CXXAccessSpecifier, { CXCursor: CXCursor });
Method('clang_getNumOverloadedDecls', 'void', { cursor: CXCursor });
Method('clang_getOverloadedDecl', CXCursor, { cursor: CXCursor, index: 'uint16' });
Method('clang_getIBOutletCollectionType', CXType, { CXCursor: CXCursor });
Method('clang_getCursorUSR', 'pointer', { CXCursor: CXCursor });
Method('clang_constructUSR_ObjCClass', CXString, { class_name: 'pointer' });
Method('clang_constructUSR_ObjCCategory', CXString, { class_name: 'pointer', category_name: 'pointer' });
Method('clang_constructUSR_ObjCProtocol', CXString, { protocol_name: 'pointer' });
Method('clang_constructUSR_ObjCIvar', CXString, { name: 'pointer', classUSR: CXString });
Method('clang_constructUSR_ObjCMethod', CXString, { name: 'pointer', isInstanceMethod: 'uint16', classUSR: CXString });
Method('clang_constructUSR_ObjCProperty', CXString, { property: 'pointer', classUSR: CXString });
Method('clang_CXXMethod_isStatic', 'void', { C: CXCursor });
Method('clang_CXXMethod_isVirtual', 'void', { C: CXCursor });
Method('clang_getTemplateCursorKind', CXCursorKind, { C: CXCursor });
Method('clang_getSpecializedCursorTemplate', CXCursor, { C: CXCursor });
Method('clang_getCursorReferenceNameRange', CXSourceRange, { C: CXCursor, NameFlags: 'uint16', PieceIndex: 'uint16' });
Method('clang_getTokenKind', CXTokenKind, { CXToken: CXToken });
Method('clang_getTokenSpelling', CXString, { CXToken: CXToken });
Method('clang_getTokenLocation', CXSourceLocation, { CXToken: CXToken });
Method('clang_getTokenExtent', CXSourceRange, { CXToken: CXToken });
Method('clang_tokenize', 'void', { TU: CXTranslationUnit, Range: CXSourceRange, Tokens: 'pointer', NumTokens: 'pointer' });
Method('clang_annotateTokens', 'void', { TU: CXTranslationUnit, Tokens: 'pointer', NumTokens: 'uint16', Cursors: 'pointer' });
Method('clang_disposeTokens', 'void', { TU: CXTranslationUnit, Tokens: 'pointer', NumTokens: 'uint16' });
Method('clang_getCursorKindSpelling', 'pointer', { Kind: CXCursorKind });
Method('clang_getDefinitionSpellingAndExtent', 'void', { CXCursor: CXCursor, startBuf: 'pointer', endBuf: 'pointer', startLine: 'pointer', startColumn: 'pointer', endLine: 'pointer', endColumn: 'pointer' });
Method('clang_enableStackTraces', 'void', {  });
Method('clang_executeOnThread', 'void', { fn: 'pointer', user_data: 'pointer', stack_size: 'uint16' });
Method('clang_getCompletionChunkKind', CXCompletionChunkKind, { completion_string: CXCompletionString, chunk_number: 'uint16' });
Method('clang_getCompletionChunkText', CXString, { completion_string: CXCompletionString, chunk_number: 'uint16' });
Method('clang_getCompletionChunkCompletionString', CXCompletionString, { completion_string: CXCompletionString, chunk_number: 'uint16' });
Method('clang_getNumCompletionChunks', 'void', { completion_string: CXCompletionString });
Method('clang_getCompletionPriority', 'void', { completion_string: CXCompletionString });
Method('clang_getCompletionAvailability', CXAvailabilityKind, { completion_string: CXCompletionString });
Method('clang_getCompletionNumAnnotations', 'void', { completion_string: CXCompletionString });
Method('clang_getCompletionAnnotation', CXString, { completion_string: CXCompletionString, annotation_number: 'uint16' });
Method('clang_getCursorCompletionString', CXCompletionString, { cursor: CXCursor });
Method('clang_defaultCodeCompleteOptions', 'uint16', {  });
Method('clang_codeCompleteAt', CXCodeCompleteResults, { TU: CXTranslationUnit, complete_filename: 'pointer', complete_line: 'uint16', complete_column: 'uint16', unsaved_files: 'pointer', num_unsaved_files: 'uint16', options: 'uint16' });
Method('clang_sortCodeCompletionResults', 'void', { Results: 'pointer', NumResults: 'uint16' });
Method('clang_disposeCodeCompleteResults', 'void', { Results: 'pointer' });
Method('clang_codeCompleteGetNumDiagnostics', 'void', { Results: 'pointer' });
Method('clang_codeCompleteGetDiagnostic', CXDiagnostic, { Results: 'pointer', Index: 'uint16' });
Method('clang_codeCompleteGetContexts', 'void', { Results: 'pointer' });
Method('clang_codeCompleteGetContainerKind', CXCursorKind, { Results: 'pointer', IsIncomplete: 'pointer' });
Method('clang_codeCompleteGetContainerUSR', CXString, { Results: 'pointer' });
Method('clang_codeCompleteGetObjCSelector', CXString, { Results: 'pointer' });
Method('clang_toggleCrashRecovery', 'void', { isEnabled: 'uint16' });
Method('clang_getInclusions', 'void', { tu: CXTranslationUnit, visitor: CXInclusionVisitor, client_data: CXClientData });
Method('clang_getRemappings', CXRemapping, { path: 'pointer' });
Method('clang_remap_getNumFiles', 'void', { CXRemapping: CXRemapping });
Method('clang_remap_getFilenames', 'void', { CXRemapping: CXRemapping, index: 'uint16', original: 'pointer', transformed: 'pointer' });
Method('clang_remap_dispose', 'void', { CXRemapping: CXRemapping });
Method('clang_findReferencesInFile', 'void', { cursor: CXCursor, file: CXFile, visitor: CXCursorAndRangeVisitor });
Method('clang_index_isEntityObjCContainerKind', 'void', { CXIdxEntityKind: CXIdxEntityKind });
Method('clang_index_getObjCContainerDeclInfo', CXIdxObjCContainerDeclInfo, { Pointer: 'pointer' });
Method('clang_index_getObjCInterfaceDeclInfo', CXIdxObjCInterfaceDeclInfo, { Pointer: 'pointer' });
Method('clang_index_getObjCCategoryDeclInfo', CXIdxObjCCategoryDeclInfo, { Pointer: 'pointer' });
Method('clang_index_getObjCProtocolRefListInfo', CXIdxObjCProtocolRefListInfo, { Pointer: 'pointer' });
Method('clang_index_getIBOutletCollectionAttrInfo', CXIdxIBOutletCollectionAttrInfo, { Pointer: 'pointer' });
Method('clang_index_getCXXClassDeclInfo', CXIdxCXXClassDeclInfo, { Pointer: 'pointer' });
Method('clang_index_getClientContainer', CXIdxClientContainer, { Pointer: 'pointer' });
Method('clang_index_setClientContainer', 'void', { CXIdxClientContainer: CXIdxClientContainer });
Method('clang_index_getClientEntity', CXIdxClientEntity, { Pointer: 'pointer' });
Method('clang_index_setClientEntity', 'void', { CXIdxClientEntity: CXIdxClientEntity });
Method('clang_IndexAction_create', CXIndexAction, { CIdx: 'pointer' });
Method('clang_IndexAction_dispose', 'void', { CXIndexAction: CXIndexAction });
Method('clang_indexSourceFile', 'void', { CXIndexAction: CXIndexAction, client_data: CXClientData, index_callbacks: 'pointer', index_callbacks_size: 'uint16', index_options: 'uint16', source_filename: 'pointer', command_line_args: 'pointer', num_command_line_args: 'int16', unsaved_files: 'pointer', num_unsaved_files: 'uint16', out_TU: 'pointer', TU_options: 'uint16' });
Method('clang_indexTranslationUnit', 'void', { CXTranslationUnit: 'pointer', client_data: 'pointer', index_callbacks: 'pointer', index_callbacks_size: 'uint16', index_options: 'uint16' });
Method('clang_indexLoc_getFileLocation', 'void', { loc: CXIdxLoc, indexFile: 'pointer', file: 'pointer', line: 'pointer', column: 'pointer', offset: 'pointer' });
Method('clang_indexLoc_getCXSourceLocation', CXSourceLocation, { loc: CXIdxLoc });
