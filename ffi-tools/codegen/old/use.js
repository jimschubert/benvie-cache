var bindings = require('./libclang.js');
var libclang = bindings.libclang;

var getters = {};

x = Object.keys(libclang).sort().reduce(function(ret,name){
  var obj = libclang;
  var fn = libclang[name];
  delete libclang[name];
  name = name.split('_');
  if (name.length === 3) {
    obj = name[1] in libclang ? libclang[name[1]] : (libclang[name[1]]={});
    name = name[2];
  } else {
    name = name[1];
  }
  name = name.split(/((?:^[a-z]|[A-Z])[a-z]+)/).filter(Boolean);

  if (!(name[0] in ret)) ret[name[0]] = [];
  ret[name.shift()].push(name.join(''));
  return ret;
}, {});

getters = Object.keys(getters);
console.log(x)
console.log(bindings.structs)
// String
// Index
// File
// Range
// Diagnostic
// TranslationUnit
// Cursor