var util = require('util');
var path = require('path');
var fs = require('fs');

var libclang = require('./libclang.node');
var enums = require('./enums');

var u = require('./utility');
q = u.q;



Object.keys(enums).forEach(function(name){
  enums[name+'_'] = u.invert(enums[name]);
});


var KINDS = enums.CursorKind;
var TYPES = enums.TypeKind;
var KINDS_ = enums.CursorKind_;
var TYPES_ = enums.TypeKind_;

var ffitypes = require('./ffitypes');
var ffitypes_ = u.invert(ffitypes);
var USRs = {};
var typedefs = Object.create(Object.prototype, {
  toString: {
    value: function(){
      var keys = Object.keys(this);
      return 'var\n'+keys.map(function(s){ return this[s][0] }, this).sort()
                 .map(function(s,i){
                    var def = typedefs[keys[i]];
                    if (def.used < 1) return typedefs.remove(keys[i]);
                    return ' '+def[0]+" = TypeDef('"+def[0]+"', "+convertToFFI(def[1])+"),\n";
                  })
                 .join('').slice(0,-2)+';';
    }
  },
  used: {
    value: function(kind, usr){
      if (usr && this[usr] && (kind === KINDS.FieldDecl || kind === KINDS.TypeRef || kind === KINDS.ParmDecl)) {
        this[usr].used++;
      }
    }
  },
  remove: {
    value: function(usr){
      delete this[usr];
      return ''
    }
  }
});



function convertToFFI(o){
  return o in USRs ? USRs[o] : o in TYPES ? q(ffitypes[o]) : o;
}

var BREAK = enums.ChildVisitResult.Break;
var CONTINUE = enums.ChildVisitResult.Continue;
var RECURSE = enums.ChildVisitResult.Recurse;


function Serializable(types){
  Object.defineProperties(this, {
    cache: { value: {} },
    names: { value: {} },
    types: { value: types.map(function(s){ return s.name }) }
  });
  var cache = this.cache;

  types.forEach(function(type){
    var set = this[type.name] = new Joinable(type);
    Object.defineProperty(set, 'names', { value: this.names });
    Object.defineProperty(this, 'add'+type.name, {
      value: function(item, filename){
        var element = set[item.usr] = cache[item.usr] = new type(item.usr);
        if ((''+item.spelling).length) {
          Object.defineProperty(element, 'name', { value: item.spelling, writable: true })
        }
      }
    });
  }, this);
}

function TypeDef(name, ffiType, type){
  return ffiType;
}

Serializable.prototype = {
  toString: function toString(){
    var self = this;
    var out = this.types.reduce(function(ret, s){
      ret.append.push(self[s].append());
      ret.decls.push(self[s].decl());
      ret.contents.push(self[s]+'');
      return ret;
    }, {append: [], decls: [], contents: [] });


    return ["var ffi = require('ffi-tools');",
            out.decls.join('\n')+'\n',
            out.append.join('\n\n')+'\n',
            TypeDef,
            typedefs,
            out.contents.join('\n\n')
            ].join('\n\n');
  },
  setName: function setName(usr, name){
    if (usr in this.cache && name) Object.defineProperty(this.cache[usr], 'name', { value: name, writable: true });
    else if (name) Object.defineProperty(this.names, 'usr', { value: name, writable: true });
    if (usr && name) USRs[usr] = name;
  },
}

function Joinable(type){
  Object.defineProperty(this, 'type', { value: type });
  if ('decl' in type)   Object.defineProperty(this, 'decl', { value: type.decl });
  if ('append' in type) Object.defineProperty(this, 'append', { value: type.append });
}
Joinable.prototype = {
  toString: function toString(){
    return Object.keys(this).map(function(s){ return this[s] }, this).join('\n');
  },
  decl: function decl(){
    return 'exports.'+this.type.name+'s = {};';
  },
  append: function append(){
    return ['function '+this.type.name+'(name, def){',
            ' return exports.'+this.type.name+'s[name] = new ffi.'+this.type.name+'(name, def);',
            '}'].join('\n')
  }
}

function Struct(usr){
  Object.defineProperty(this, 'usr', { value: usr });
  this.members = {};
}

Struct.prototype = {
  toString: function toString(){
    var keys = Object.keys(this.members);
    if (!keys.length) return '';
    return "var "+this.name+" = Struct('"+this.name+"', {\n "+
              Object.keys(this.members).map(function(name){
                return u.makeProperty(name)+': '+convertToFFI(this[name]);
              }, this.members).join(',\n ')+
           '\n});\n';
  }
};



function Enum(usr){
  Object.defineProperty(this, 'usr', { value: usr });
}

Enum.prototype.toString = function(){
  var keys = Object.keys(this.members || {});
  var name = this.name;
  var parts = name.toLowerCase().replace(/_/g,'');
  if (!name && !keys.length) return '';
  return "var "+name+" = Enum('"+name+"', {\n "+
            Object.keys(this).map(function(n){
              var label = split(n)
                .filter(function(s,i,a){
                  var index = parts.indexOf(s);
                  return !~index || i === a.length - 1 ;
                })
                .map(function(s){ return s[0].toUpperCase()+s.slice(1) })
                .join('');
              return label+': '+this[n];
            }, this).join(',\n ')+
         '\n});\n';
}

function split(name){
  return name.split(/([A-Z][a-z]+)|(?:_)/).filter(Boolean).map(function(s){
    return s.toLowerCase();
  });
}


function Method(usr, returnType, params, libname){
  Object.defineProperty(this, 'usr', { value: usr });
  this.returnType = returnType || 'Void';
  this.params = params || {};
  libname && (this.libname = libname);
}
Object.defineProperty(Method, 'append', {
  value: function(){
    return ["function Method(name, ret, params){",
            " return exports.Methods[name] = new ffi.ForeignFunction(name, '"+this.libname+"', ret, params);",
            "}"].join('\n');
  }
});


Method.prototype = {
  toString: function(standalone){
    var ret = convertToFFI(this.returnType);
    var params = Object.keys(this.params).map(function(name,i){
      return (name||(this[name] in USRs ? convertToFFI(this[name]) : this[name]))+': '+convertToFFI(this[name]);
    }, this.params);


    return "Method('"+this.name+"', "+ret+", { "+params.join(', ')+" });";
  }
}

function Library(name){
  Object.defineProperty(this, 'name', { value: name });
}


function generate(opts){
  var idx = new libclang.index;
  var tu = new libclang.translationunit;

  opts.compilerArgs = opts.compilerArgs || [];
  opts.includes && opts.includes.forEach(function(n){ opts.compilerArgs.push('-I'+n) });
  tu.fromSource(idx, path.resolve(opts.filename), opts.compilerArgs);

  var cursor = tu.cursor();
  var pathCompare;

  var generated = new Serializable([Enum, Struct, Method], path.resolve(opts.filename));
  Object.defineProperty(generated.Method, 'libname', { value: opts.prefix });

  var enumFields = {};
  var files = {};

  function getFile(name){
    if (name in files) return files[name];
    return files[name] = fs.readFileSync(name, 'utf8').split('\n');
  }

  function extractLiteral(cursor){
    var loc = cursor.location.presumedLocation;
    var text = getFile(loc.filename)[loc.line-1].slice(loc.column-1).trim().replace(/\t/g, ' ');
    var comment = text.indexOf('//');
    if (!~comment) comment = text.indexOf('/*');
    if (~comment) text = text.slice(0, comment).trim();
    if (text[text.length-1] === ',') text = text.slice(0,-1).trim();
    return text;
  }

  var lastFilename;
  var filenames = {};

  function recursor(parent){
    var recurse;
    var filename = this.location.presumedLocation.filename;


    if (this.kind === KINDS.TypedefDecl && this.type.canonical.spelling !== 'Record' && 
        !(this.usr in typedefs) && !(this.type.canonical.declaration.usr in generated.Enum)) {
      typedefs[this.usr] = [this.spelling, this.type.canonical.spelling, filename];
      typedefs[this.usr].used = 0;
    }
    var usr = this.type.declaration.usr;
    var typename = usr in generated.Enum ? 'UInt' : this.type.canonical.spelling ||  this.type.declaration.spelling;

    if (usr && !(usr in USRs)) USRs[usr] = this.spelling;

    if (opts.localOnly && filename !== '<built-in>') {
      if (!pathCompare) pathCompare = filename;
      if (pathCompare !== filename) return CONTINUE;
    }

    if (filename !== lastFilename) {
      lastFilename = filename;
      filenames[path.basename(filename)] = true;
    }
    typedefs.used(this.kind, usr);

    switch (this.kind)  {
      case KINDS.EnumDecl:
        enumFields[this.usr] = 0;
        generated.addEnum(this, filename);
        recurse = true;
        break;

      case KINDS.FunctionDecl:
        generated.addMethod(this, filename);
        recurse = true;
        break;

      case KINDS.StructDecl:
        generated.addStruct(this, filename);
        recurse = true;
        break;

      case KINDS.EnumConstantDecl:
        if (parent.usr in generated.Enum) {
          generated.Enum[parent.usr][this.spelling] = enumFields[parent.usr]++;
          enumFields[this.usr] = generated.Enum[parent.usr];
          recurse = true;
        }
        break;

      case KINDS.IntegerLiteral:
        var val = extractLiteral(this);
        enumFields[parent.usr][parent.spelling] = val;
        break;

      case KINDS.FieldDecl:
        if (parent.usr in generated.Struct) {
          generated.Struct[parent.usr].members[this.spelling] = usr in USRs ? usr : typename;
        }
        break;

      case KINDS.TypeRef:
        if (parent.usr in generated.Method) {
          generated.Method[parent.usr].returnType = usr in USRs ? usr : typename;
        }
        break;

      case KINDS.ParmDecl:
        if (parent.usr in generated.Method) {
          generated.Method[parent.usr].params[this.spelling] = usr in USRs ? usr : typename;
        }
        break;

      case KINDS.TypedefDecl:
        var usr = this.type.canonical.declaration.usr;
        if (!generated.names[usr]) {
          generated.setName(usr, this.spelling);
        }
        break;

      case KINDS.MacroExpansion:
      case KINDS.MacroDefinition:
      case KINDS.VarDecl:
      case KINDS.ParenExpr:
      case KINDS.DeclRefExpr:
      case KINDS.UnexposedAttr:
      case KINDS.InclusionDirective:
        break;
      default:
      //var log = [KINDS_[this.kind], typename, this.spelling];
      //console.log(u.zipmap(u.pad, log, [30, 40, 50]).join(' | '))
    }
    return recurse ? RECURSE : CONTINUE;
  }

  cursor.visitChildren(recursor);
  fs.writeFileSync('out/'+opts.prefix+'.js', generated+'');
  tu.dispose();
  idx.dispose();
};

// TYPE [ kind, spelling, result, canonical, argTypes, declaration, pointeeType ]
// CURSOR [ usr, kind, spelling, displayname, type, enumType, location, visitChildren ] [getArg, isPOD]


function convert(header){
  var dir = path.dirname(header);
  var result = generate({
    localOnly: true,
    filename: header,
    library: path.basename(header),
    prefix: path.basename(header).slice(0, -path.extname(header).length),
    includes: fs.readdirSync(dir).filter(function(s){
      return path.extname(s) === '.h';
    }).map(function(s){ return dir + '/' + s })
  });
}


var clang = path.resolve('libclang.h');
var windows = 'C:\\Program Files (x86)\\Microsoft SDKs\\Windows\\v7.0A\\Include\\windows.h';

convert(clang);
/*
{
  winbase: 'kernel32',
  wincon: 'kernel32',
  wincrypt: ,
  windef: ,
  winefs: ,
  winerror: ,
  wingdi: 'gdi32',
  winioctl: ,
  winnetwk: ,
  winnls: ,
  winnt: ,
  winperf: ,
  winreg: ,
  winscard: ,
  winsmcrd: ,
  winsock: ,
  winspool: ,
  winsvc: ,
  winuser: 'user32',
  winver: ,
  wnnc: ,
  wtypes: ,
}

[ '<built-in>',
  'Windows.h',
  'basetsd.h',
  'bcrypt.h',
  'cderr.h',
  'cguid.h',
  'codegen',
  'commdlg.h',
  'crtdefs.h',
  'ctype.h',
  'dde.h',
  'ddeml.h',
  'dlgs.h',
  'driverspecs.h',
  'excpt.h',
  'guiddef.h',
  'ime_cmodes.h',
  'imm.h',
  'inaddr.h',
  'kernelspecs.h',
  'ktmtypes.h',
  'limits.h',
  'lzexpand.h',
  'mcx.h',
  'mmsystem.h',
  'msxml.h',
  'nb30.h',
  'ncrypt.h',
  'oaidl.h',
  'objbase.h',
  'objidl.h',
  'ole2.h',
  'oleauto.h',
  'oleidl.h',
  'poppack.h',
  'propidl.h',
  'prsht.h',
  'pshpack1.h',
  'pshpack2.h',
  'pshpack4.h',
  'pshpack8.h',
  'reason.h',
  'rpc.h',
  'rpcasync.h',
  'rpcdce.h',
  'rpcdcep.h',
  'rpcndr.h',
  'rpcnsi.h',
  'rpcnsip.h',
  'rpcnterr.h',
  'rpcsal.h',
  'sal.h',
  'sal_supp.h',
  'sdkddkver.h',
  'sdv_driverspecs.h',
  'servprov.h',
  'shellapi.h',
  'sourceannotations.h',
  'specstrings.h',
  'specstrings_strict.h',
  'specstrings_supp.h',
  'stdarg.h',
  'stdlib.h',
  'stralign.h',
  'string.h',
  'tvout.h',
  'unknwn.h',
  'urlmon.h',
  'vadefs.h',
  'verrsrc.h',
  'winbase.h',
  'wincon.h',
  'wincrypt.h',
  'windef.h',
  'winefs.h',
  'winerror.h',
  'wingdi.h',
  'winioctl.h',
  'winnetwk.h',
  'winnls.h',
  'winnt.h',
  'winperf.h',
  'winreg.h',
  'winscard.h',
  'winsmcrd.h',
  'winsock.h',
  'winspool.h',
  'winsvc.h',
  'winuser.h',
  'winver.h',
  'wnnc.h',
  'wtypes.h' ]
*/