module.exports = {
  q: q,
  makeProperty: makeProperty,
  makeIdentifier: makeIdentifier,
  dense: dense,
  space: space,
  repeat: repeat,
  fill: fill,
  widest: widest,
  truncate: truncate,
  indent: indent,
  pad: pad,
  columns: columns,
  zipmap: zipmap,
  invert: invert
}

function invert(o){
  return Object.keys(o).reduce(function(ret, name){
    ret[o[name]] = name;
    return ret;
  }, {});
}

function q(s){
  var quotes = ['"', "'"];
  var match = [/(')/g, /(")/g];
  s = String(s).replace(/\\/g, '\\\\');
  s = String(s).replace(/\n/g, '\\n');
  var use = +(s.match(match[0]) === null);
  return quotes[use] + s.replace(match[1-use], '\\$1') + quotes[use];
}

function makeProperty(name, literal){
  if (Array.isArray(name)) {
    return name[0] + name.slice(1).map(makeProperty.bind(this)).join('');
  }
  if (/^[a-zA-Z_\$][a-zA-Z0-9_\$]*$/.test(name)) {
    return literal ? '.'+name : name;
  }
  name = q(name);
  return literal ? '['+name+']' : name;
}

function makeIdentifier(s){
  return s.replace(/\w*/g, '_')
          .replace(/^([^a-zA-Z_$])/, '_$1')
          .replace(/[^\w_$]+(.)?/g, function(m,c){ return c ? c.toUpperCase() : '' });
}


var ansi = /\033\[(?:\d+;)*\d+m/g;


function alength(s){ return s.replace(ansi, '').length }
function noansi(s){ return s.replace(ansi, '') }
function dense(n){ return n > 0 ? new Array(n).join(0).split(0) : [] }
function space(n){ return repeat(' ', n) }
function repeat(s,n){ return ++n > 0 ? new Array(n).join(s+'') : '' }
function fill(n,cb){ var a=[]; while (n--) a[n]=cb(n); return a; }
function widest(a){ return a.reduce(function(r,x){ return Math.max(alength(x+''), r) }, 0) }
function truncate(s,l){
  var r = new RegExp('^.{'+[0,l]+'}\\b','mg'), m, last=0;
  while ((m = r.exec(s)) && r.lastIndex < l) last = r.lastIndex;
  return s.slice(0, last)+'â€¦';
}
function indent(s,n){
  n = n > 0 ? space(n) : n+'';
  return s.split('\n').map(function(x){ return s + x }).join('\n');
}
function pad(s,x){
  s=s||'';
  var o = space(Math.abs(x) - s.length);
  return x < 0 ? o + s : s + o;
}

function columns(array){
  fill(array[0].length, function(i){ return array[i].map(widest) })
  if (typeof array[0] === 'string') array = [array];
  return array.reduce(f, dense(array[0].length));
}

function zipmap(fn, p1, p2, bind){
  return p1.map(function(item,i){
    item = Array.isArray(item) ? item : [item];
    return fn.apply(bind || item, item.concat(p2[i]));
  });
}
