exports.NameRefFlags = {
  WantTemplateArgs                   : 1,
  WantSinglePiece                    : 2
}
exports.ChildVisitResult = {
  Break                              : 0,
  Continue                           : 1,
  Recurse                            : 2
}
exports.LanguageKind = {
  C                                  : 1,
  ObjC                               : 2,
  CPlusPlus                          : 3
}
exports.SaveError = {
  None                               : 0,
  Unknown                            : 1,
  TranslationErrors                  : 2,
  InvalidTU                          : 3
}
exports.LoadDiag_Error = {
  Unknown                            : 1,
  CannotLoad                         : 2,
  InvalidFile                        : 3
}
exports.AvailabilityKind = {
  Deprecated                         : 1,
  NotAvailable                       : 2,
  NotAccessible                      : 3
}
exports.DiagnosticSeverity = {
  Note                               : 1,
  Warning                            : 2,
  Error                              : 3,
  Fatal                              : 4
}
exports.LinkageKind = {
  NoLinkage                          : 1,
  Internal                           : 2,
  UniqueExternal                     : 3,
  External                           : 4
}
exports.CXXAccessSpecifier = {
  Public                             : 1,
  Protected                          : 2,
  Private                            : 3
}
exports.TokenKind = {
  Keyword                            : 1,
  Identifier                         : 2,
  Literal                            : 3,
  Comment                            : 4
}
exports.DiagnosticDisplayOptions = {
  DisplaySourceLocation              : 0x01,
  DisplayColumn                      : 0x02,
  DisplaySourceRanges                : 0x04,
  DisplayOption                      : 0x08,
  DisplayCategoryId                  : 0x10,
  DisplayCategoryName                : 0x20
}
exports.TranslationUnit_Flags = {
  None                               : 0x00,
  DetailedPreprocessingRecord        : 0x01,
  Incomplete                         : 0x02,
  PrecompiledPreamble                : 0x04,
  CacheCompletionResults             : 0x08,
  CXXPrecompiledPreamble             : 0x10,
  CXXChainedPCH                      : 0x20,
  NestedMacroExpansions              : 0x40,
  NestedMacroInstantiations          : 0x80
}
exports.CallingConv = {
  C                                  : 1,
  X86StdCall                         : 2,
  X86FastCall                        : 3,
  X86ThisCall                        : 4,
  X86Pascal                          : 5,
  AAPCS                              : 6,
  AAPCS_VFP                          : 7,
  Invalid                            : 100,
  Unexposed                          : 200
}
exports.TUResourceUsageKind = {
  AST                                : 1,
  Identifiers                        : 2,
  Selectors                          : 3,
  GlobalCompletionResults            : 4,
  SourceManagerContentCache          : 5,
  AST_SideTables                     : 6,
  SourceManager_Membuffer_Malloc     : 7,
  SourceManager_Membuffer_MMap       : 8,
  ExternalASTSource_Membuffer_Malloc : 9,
  ExternalASTSource_Membuffer_MMap   : 10,
  Preprocessor                       : 11,
  PreprocessingRecord                : 12,
  SourceManager_DataStructures       : 13,
  Preprocessor_HeaderSearch          : 14,
}
exports.CursorKind = {
  UnexposedDecl                      : 1,
  StructDecl                         : 2,
  UnionDecl                          : 3,
  ClassDecl                          : 4,
  EnumDecl                           : 5,
  FieldDecl                          : 6,
  EnumConstantDecl                   : 7,
  FunctionDecl                       : 8,
  VarDecl                            : 9,
  ParmDecl                           : 10,
  ObjCInterfaceDecl                  : 11,
  ObjCCategoryDecl                   : 12,
  ObjCProtocolDecl                   : 13,
  ObjCPropertyDecl                   : 14,
  ObjCIvarDecl                       : 15,
  ObjCInstanceMethodDecl             : 16,
  ObjCClassMethodDecl                : 17,
  ObjCImplementationDecl             : 18,
  ObjCCategoryImplDecl               : 19,
  TypedefDecl                        : 20,
  CXXMethod                          : 21,
  Namespace                          : 22,
  LinkageSpec                        : 23,
  Constructor                        : 24,
  Destructor                         : 25,
  ConversionFunction                 : 26,
  TemplateTypeParameter              : 27,
  NonTypeTemplateParameter           : 28,
  TemplateTemplateParameter          : 29,
  FunctionTemplate                   : 30,
  ClassTemplate                      : 31,
  ClassTemplatePartialSpecialization : 32,
  NamespaceAlias                     : 33,
  UsingDirective                     : 34,
  UsingDeclaration                   : 35,
  TypeAliasDecl                      : 36,
  ObjCSynthesizeDecl                 : 37,
  ObjCDynamicDecl                    : 38,
  CXXAccessSpecifier                 : 39,

  ObjCSuperClassRef                  : 40,
  ObjCProtocolRef                    : 41,
  ObjCClassRef                       : 42,
  TypeRef                            : 43,
  CXXBaseSpecifier                   : 44,
  TemplateRef                        : 45,
  NamespaceRef                       : 46,
  MemberRef                          : 47,
  LabelRef                           : 48,
  OverloadedDeclRef                  : 49,
  VariableRef                        : 50,

  InvalidFile                        : 70,
  NoDeclFound                        : 71,
  NotImplemented                     : 72,
  InvalidCode                        : 73,

  UnexposedExpr                      : 100,
  DeclRefExpr                        : 101,
  MemberRefExpr                      : 102,
  CallExpr                           : 103,
  ObjCMessageExpr                    : 104,
  BlockExpr                          : 105,
  IntegerLiteral                     : 106,
  FloatingLiteral                    : 107,
  ImaginaryLiteral                   : 108,
  StringLiteral                      : 119,
  CharacterLiteral                   : 110,
  ParenExpr                          : 111,
  UnaryOperator                      : 112,
  ArraySubscriptExpr                 : 113,
  BinaryOperator                     : 114,
  CompoundAssignOperator             : 115,
  ConditionalOperator                : 116,
  CStyleCastExpr                     : 117,
  CompoundLiteralExpr                : 118,
  InitListExpr                       : 129,
  AddrLabelExpr                      : 120,
  StmtExpr                           : 121,
  GenericSelectionExpr               : 122,
  GNUNullExpr                        : 123,
  CXXStaticCastExpr                  : 124,
  CXXDynamicCastExpr                 : 125,
  CXXReinterpretCastExpr             : 126,
  CXXConstCastExpr                   : 127,
  CXXFunctionalCastExpr              : 128,
  CXXTypeidExpr                      : 139,
  CXXBoolLiteralExpr                 : 130,
  CXXNullPtrLiteralExpr              : 131,
  CXXThisExpr                        : 132,
  CXXThrowExpr                       : 133,
  CXXNewExpr                         : 134,
  CXXDeleteExpr                      : 135,
  UnaryExpr                          : 136,
  ObjCStringLiteral                  : 137,
  ObjCEncodeExpr                     : 138,
  ObjCSelectorExpr                   : 149,
  ObjCProtocolExpr                   : 140,
  ObjCBridgedCastExpr                : 141,
  PackExpansionExpr                  : 142,
  SizeOfPackExpr                     : 143,
  LambdaExpr                         : 144,

  UnexposedStmt                      : 200,
  LabelStmt                          : 201,
  CompoundStmt                       : 202,
  CaseStmt                           : 203,
  DefaultStmt                        : 204,
  IfStmt                             : 205,
  SwitchStmt                         : 206,
  WhileStmt                          : 207,
  DoStmt                             : 208,
  ForStmt                            : 209,
  GotoStmt                           : 210,
  IndirectGotoStmt                   : 211,
  ContinueStmt                       : 212,
  BreakStmt                          : 213,
  ReturnStmt                         : 214,
  AsmStmt                            : 215,
  ObjCAtTryStmt                      : 216,
  ObjCAtCatchStmt                    : 217,
  ObjCAtFinallyStmt                  : 218,
  ObjCAtThrowStmt                    : 219,
  ObjCAtSynchronizedStmt             : 220,
  ObjCAutoreleasePoolStmt            : 221,
  ObjCForCollectionStmt              : 222,
  CXXCatchStmt                       : 223,
  CXXTryStmt                         : 224,
  CXXForRangeStmt                    : 225,
  SEHTryStmt                         : 226,
  SEHExceptStmt                      : 227,
  SEHFinallyStmt                     : 228,
  NullStmt                           : 230,
  DeclStmt                           : 231,

  TranslationUnit                    : 300,

  UnexposedAttr                      : 400,
  IBActionAttr                       : 401,
  IBOutletAttr                       : 402,
  IBOutletCollectionAttr             : 403,
  CXXFinalAttr                       : 404,
  CXXOverrideAttr                    : 405,
  AnnotateAttr                       : 406,
  AsmLabelAttr                       : 407,

  PreprocessingDirective             : 500,
  MacroDefinition                    : 501,
  MacroExpansion                     : 502,
  MacroInstantiation                 : 502,
  InclusionDirective                 : 503
}
exports.TypeKind = {
  Invalid                            : 0,
  Unexposed                          : 1,
  Void                               : 2,
  Bool                               : 3,
  Char_U                             : 4,
  UChar                              : 5,
  Char16                             : 6,
  Char32                             : 7,
  UShort                             : 8,
  UInt                               : 9,
  ULong                              : 10,
  ULongLong                          : 11,
  UInt128                            : 12,
  Char_S                             : 13,
  SChar                              : 14,
  WChar                              : 15,
  Short                              : 16,
  Int                                : 17,
  Long                               : 18,
  LongLong                           : 19,
  Int128                             : 20,
  Float                              : 21,
  Double                             : 22,
  LongDouble                         : 23,
  NullPtr                            : 24,
  Overload                           : 25,
  Dependent                          : 26,
  ObjCId                             : 27,
  ObjCClass                          : 28,
  ObjCSel                            : 29,
  Complex                            : 100,
  Pointer                            : 101,
  BlockPointer                       : 102,
  LValueReference                    : 103,
  RValueReference                    : 104,
  Record                             : 105,
  Enum                               : 106,
  Typedef                            : 107,
  ObjCInterface                      : 108,
  ObjCObjectPointer                  : 109,
  FunctionNoProto                    : 110,
  FunctionProto                      : 111,
  ConstantArray                      : 112,
  Vector                             : 113
}
exports.CompletionChunkKind = {
  TypedText                          : 1,
  Text                               : 2,
  Placeholder                        : 3,
  Informative                        : 4,
  CurrentParameter                   : 5,
  LeftParen                          : 6,
  RightParen                         : 7,
  LeftBracket                        : 8,
  RightBracket                       : 9,
  LeftBrace                          : 10,
  RightBrace                         : 11,
  LeftAngle                          : 12,
  RightAngle                         : 13,
  Comma                              : 14,
  ResultType                         : 15,
  Colon                              : 16,
  SemiColon                          : 17,
  Equal                              : 18,
  HorizontalSpace                    : 19,
  VerticalSpace                      : 20
}
exports.CompletionContext = {
  AnyType                            : 1,
  AnyValue                           : 2,
  ObjCObjectValue                    : 3,
  ObjCSelectorValue                  : 4,
  CXXClassTypeValue                  : 5,
  DotMemberAccess                    : 6,
  ArrowMemberAccess                  : 7,
  ObjCPropertyAccess                 : 8,
  EnumTag                            : 9,
  UnionTag                           : 10,
  StructTag                          : 11,
  ClassTag                           : 12,
  Namespace                          : 13,
  NestedNameSpecifier                : 14,
  ObjCInterface                      : 15,
  ObjCProtocol                       : 16,
  ObjCCategory                       : 17,
  ObjCInstanceMessage                : 18,
  ObjCClassMessage                   : 19,
  ObjCSelectorName                   : 20,
  MacroName                          : 21,
  NaturalLanguage                    : 22,
  Unknown                            : 23
}