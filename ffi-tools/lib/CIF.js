var ffi = require('./ffi');
var bindings = require('./bindings');

module.exports = CIF;

function CIF(retType, types){
  if (!ffi.isValidReturnType(retType)) throw new Error('Invalid Return Type: ' + retType);

  this.argsPointer = new ffi.Pointer(types.length * bindings.FFI_TYPE_SIZE);
  this.retPointer = ffi.ffiTypeFor(retType);

  var tptr = this.argsPointer.clone();
  var argsCount = types.length;

  for (var i=0; i < argsCount; i++) {
    if (!ffi.isValidReturnType(types[i])) throw new Error('Invalid Return Type: ' + types[i]);
    tptr.putPointer(ffi.ffiTypeFor(types[i]), true);
  }

  this.pointer = bindings.prepCif(types.length, this.retPointer, this.argsPointer);
}
