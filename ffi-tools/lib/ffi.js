var ffi = module.exports = {};

var bindings = require('./bindings')
var utility = require('./utility');


ffi.TYPE_TO_POINTER_METHOD_MAP = {
  uint8   : 'UInt8',
  int8    : 'Int8' ,
  uint8   : 'UInt8',
  int16   : 'Int16',
  uint16  : 'UInt16',
  int32   : 'Int32',
  uint32  : 'UInt32',
  int64   : 'Int64',
  uint64  : 'UInt64',
  float   : 'Float',
  double  : 'Double',
  string  : 'CString',
  pointer : 'Pointer',
}

ffi.SIZE_TO_POINTER_METHOD_MAP = {
  1: 'Int8',
  2: 'Int16',
  4: 'Int32',
  8: 'Int64'
}

ffi.PLATFORM_EXTENSIONS = {
  linux  : '.so',
  linux2 : '.so',
  sunos  : '.so',
  darwin : '.dylib',
  mac    : '.dylib',
  win32  : '.dll'
}

// A list of types with no hard C++ methods to read/write them
ffi.NON_SPECIFIC_TYPES = {
   byte      : 'Byte',
   char      : 'Char',
   uchar     : 'UChar',
   short     : 'Short',
   ushort    : 'UShort',
   int       : 'Int',
   uint      : 'UInt',
   long      : 'Long',
   ulong     : 'ULong',
   longlong  : 'LongLong',
   ulonglong : 'ULongLong',
   size_t    : 'SizeT',
}

// ------------------------------------------------------
// Miscellaneous Utility Functions
// ------------------------------------------------------

// Returns true if the passed type is a valid param type
ffi.isValidParamType = function(type) {
  return ffi.isStructType(type) || ffi.isEnum(type) || bindings.FFI_TYPES[type] !== undefined
}

// Returns true if the passed type is a valid return type
ffi.isValidReturnType = function(type) {
  return ffi.isValidParamType(type) || type == 'void'
}

ffi.derefValuePtr = function(type, ptr) {
  if (!ffi.isValidParamType(type)) {
    throw new Error('Invalid Type: ' + type)
  }
  return deref(type)(ptr);
}
ffi.refValuePtr = function(type, ptr, val) {
  if (!ffi.isValidParamType(type)) {
    throw new Error('Invalid Type: ' + type)
  }
  return ref(type)(ptr, val);
}

function deref(type){
  if (type in deref) {
    return deref[type];
  } else if (ffi.isEnum(type)) {
    return deref[type.name] = function(ptr){
      return type.reverse[ptr.getUInt16()];
    }
  } else if (ffi.isStructType(type)) {
    return deref[type] = function(ptr){ return new type(ptr && 'pointer' in ptr ? ptr.pointer : ptr) }
  } else if (type in ffi.TYPE_TO_POINTER_METHOD_MAP) {
    var getType = 'get' + ffi.TYPE_TO_POINTER_METHOD_MAP[type];
    return deref[type] = function(ptr){ return ptr[getType]() }
  } else {
    throw new Error('Unknown type "' + type + '"');
  }
}

deref.void = function(ptr){ return null }

deref.string = function(ptr){
  ptr = ptr.getPointer();
  return ptr.isNull() ? null : ptr.getCString();
}


function ref(type){
  if (ffi.isEnum(type)) type = 'uint16';
  if (type in ref) {
    return ref[type];
  } else if (ffi.isStructType(type)) {
    return ref[type] = function(ptr, v){ new type(ptr, v) }
  } else if (type in ffi.TYPE_TO_POINTER_METHOD_MAP) {
    var putType = 'put' + ffi.TYPE_TO_POINTER_METHOD_MAP[type];
    return ref[type] = function(ptr, v){ ptr[putType](v) }
  } else {
    throw new Error('Unknown type "' + type + '"');
  }
}

ref.string = function(ptr, v){
  if (typeof v === 'undefined' || v === null) {
    return ptr.putPointer(ffi.NULL)
  }
  var len = Buffer.byteLength(v, 'utf8');
  var strPtr = new ffi.Pointer(len+1);
  strPtr.putCString(v);
  ptr.putPointer(strPtr);
}

ffi.derefValuePtrFunc = deref;
ffi.refValuePtrFunc = ref;

/**
 * Returns the byte size of the given type. `type` may be a string name
 * identifier or a Struct type.
 * Roughly equivalent to the C sizeof() operator.
 */

function sizeOf(type){
  if (ffi.isEnum(type)) type = 'uint32';
  return ffi.isStructType(type)
      ? type.structInfo.size
      : bindings.TYPE_SIZE_MAP[type];
}
ffi.sizeOf = sizeOf;
/**
 * Returns the FFI_TYPE for the given `type`. May be a `Struct` type.
 */

function ffiTypeFor(type){
  if (ffi.isStructType(type)) return type._ffiType().pointer;
  if (ffi.isEnum(type)) type = 'uint32';
  return bindings.FFI_TYPES[type];
}
ffi.ffiTypeFor = ffiTypeFor;



/**
 * Returns true if the given `type` is a Struct type, false otherwise.
 */




// Direct exports from the bindings
ffi.free             = bindings.free
ffi.CallbackInfo     = bindings.CallbackInfo
ffi.TYPE_SIZES       = bindings.TYPE_SIZE_MAP;

// Include our other modules
ffi.Enum             = require('./Enum');
ffi.isEnum           = ffi.Enum.isEnum;
ffi.Struct           = require('./Struct');
ffi.isStructType     = ffi.Struct.isStructType;
ffi.isStruct         = ffi.Struct.isStruct;
ffi.Pointer          = require('./Pointer');
ffi.CIF              = require('./CIF');
ffi.ForeignFunction  = require('./ForeignFunction');
ffi.DLL              = require('./DLL');
ffi.Library          = require('./Library');
ffi.Callback         = require('./Callback');
ffi.errno            = require('./errno');


/**
 * Define the `FFI_TYPE` struct for use in JS.
 * This struct type is used internally to define custom struct rtn/arg types.
 */

ffi.FFI_TYPE = ffi.Struct('FFI_TYPE', {
  size      :   'size_t',
  type      :   'ushort',
  alignment :   'ushort',
  elements  :   'pointer',
});

utility.hide(ffi, [ 'TYPE_SIZES', 'FFI_TYPE', 'TYPE_TO_POINTER_METHOD_MAP', 'SIZE_TO_POINTER_METHOD_MAP',
                    'PLATFORM_EXTENSIONS', 'NON_SPECIFIC_TYPES' ]);
