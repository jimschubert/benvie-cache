module.exports = {
  hide: hide,
  hidden: hidden,
}

function hide(o,p){
  p = p || Object.keys(o);
  return Array.isArray(p) ? p.map(hide.bind(null, o)) : Object.defineProperty(o, p, { enumerable: false });
}

function hidden(o,p,v){
  Object.defineProperty(o, p, { value: v, configurable: true, writable: true });
}
