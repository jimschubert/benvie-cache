var ffi = require('./ffi');
var utility = require('./utility');
var bindings = require('./bindings');


module.exports = StructType;


function StructType(name, definition){
  var ctor = eval('(function '+name+'(args, data){ return StructType.create('+name+', args, data) })');
  ctor.__proto__ = StructType.prototype;

  Object.defineProperties(ctor, {
    structInfo: { value: new StructInfo(definition) },
    ref: { value: function ref(){ return StructType.create(ctor) } },
  });
  ctor.prototype = new Struct(ctor)

  return StructType.types[name] = ctor;
}

StructType.create = function create(type, args, data){
  type = StructType.getType(type);
  if (!type) {
    throw new Error('Unknown Struct type');
  }

  var instance = Object.create(type.prototype);
  if (ffi.Pointer.isPointer(args)) {
    instance.pointer = args;
    args = data;
  } else {
    instance.pointer = new ffi.Pointer(type.structInfo.size);
  }
  if (args) {
    for (var key in args) {
      instance[key] = args[key];
    }
  }
  return instance;
}

StructType.types = {};

StructType.getType = function get(type){
  if (!type) return null;
  switch (typeof type) {
    case 'function':
      return type.name in StructType.types ? type : null;
    case 'object':
      var ctor = type.constructor;
      return ctor && StructType.types[ctor.name] === ctor ? ctor : null;
    case 'string':
      return type in StructType.types ? StructType.types[type] : null;
    default:
      return null;
  }
}

StructType.isStructType = function isStructType(val){
  return typeof val === 'function'
          ? !!(val.name && StructType.types[val.name] === val)
          : isStructType(val.constructor);
}

StructType.isStruct = function isStruct(val){
  return !!(val && StructType.isStructType(val.constructor));
}

StructType.getInfo = function getInfo(type){
  type = StructType.getType(type);
  return type ? type.structInfo : null;
}

StructType.prototype = {
  constructor: StructType,
  __proto__: Function.prototype,

  _ffiType: function _ffiType(){
    var info = this.structInfo;

    var structType = new ffi.FFI_TYPE;
    structType.size = 0;
    structType.type = 13;
    structType.alignment = 0;
    structType.elements = new ffi.Pointer(bindings.POINTER_SIZE * (info.length+1));

    var typePtr = info.members.reduce(function(ptr, name){
      ptr.putPointer(ffi.ffiTypeFor(info.fields[name].type), true);
      return ptr;
    }, structType.elements.clone());

    typePtr.putPointer(ffi.NULL);

    Object.defineProperty(this, '_ffiType', { value: function(){ return structType } });
    return structType;
  },

  reify: function reify(instance){
    return this.structInfo.members.reduce(function(obj, name){
      obj[name] = instance[name];
      return obj;
    }, {});
  }
};
utility.hide(StructType.prototype, ['constructor', '_ffiType', 'reify']);


function StructInfo(fields){
  this.fields = {};
  this.members = [];
  this.size = 0;
  this.alignment = 0;
  this.length = 0;

  if (Array.isArray(fields)) {
    fields.forEach(this.addField.bind(this));
  } else if (Object(fields) === fields) {
    Object.keys(fields).forEach(function(name){
      this.addField(fields[name], name);
    }, this);
  }

  var left = this.size % this.alignment;
  if (left) this.size += this.alignment - left;
}

StructInfo.prototype = {
  constructor: StructInfo,

  addField: function addField(type, name){
    if (Array.isArray(type)) {
      name = type[1];
      type = type[0];
    }
    if (name in this.fields) {
      throw new Error('Error when constructing Struct: ' + name + ' field specified twice!')
    }
    var sizeOf = ffi.sizeOf(type);
    this.alignment = Math.max(this.alignment, StructType.isStructType(type) ? type.structInfo.alignment : sizeOf);
    var offset = this.size;
    var left = offset % this.alignment;
    if (sizeOf > left) offset += left
    this.size = offset + sizeOf;
    this.length++;
    this.members.push(name);
    this.fields[name] = {
      name: name,
      type: type,
      size: sizeOf,
      offset: offset
    };
  },
};
utility.hide(StructInfo.prototype, ['constructor', 'addField', 'members']);


function structReify(info){
  var read = ffi.derefValuePtrFunc(info.type);
  return function(){
    return read(this.pointer.seek(info.offset));
  }
}

function structRef(info){
  var write = ffi.refValuePtrFunc(info.type);
  return function(v){
    write(this.pointer.seek(info.offset), v);
  }
}

function Struct(ctor){
  Object.defineProperty(this, 'constructor', { value: ctor });
  var info = ctor.structInfo;
  info.members.forEach(function(name){
    Object.defineProperty(this, name, {
      get: structReify(info.fields[name]),
      set: structRef(info.fields[name]),
      enumerable: true,
      configurable: true
    });
  }, this);
}


Struct.prototype = {
  clone: function clone(){
    return new this.constructor(this.pointer);
  },
  reify: function reify(){
    return this.constructor.reify(this);
  },
  cast: function cast(type){
    if (type = StructType.getType(type)) {
      return new type(this.pointer);
    } else {
      throw new Error("Couldn't find type");
    }
  }
};
utility.hide(Struct.prototype, ['clone', 'reify', 'cast']);

bindings.Pointer.prototype.cast = function cast(type){
  if (type = StructType.getType(type)) {
    return new type(this);
  } else {
    throw new Error("Couldn't find type");
  }
}