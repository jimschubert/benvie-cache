var ffi = require('./ffi');
var bindings = require('./bindings');

module.exports = Callback;

function Callback(retType, types, func){
  this._cif = new ffi.CIF(retType, types);
  this._info = new ffi.CallbackInfo(this._cif.pointer, function(retval, params){
    var pptr = params.clone();
    var args = types.map(function(type){ return ffi.derefValuePtr(type, pptr.getPointer(true)) });
    var result = func.apply(null, args);
    if (retType !== 'void') {
      retval['put' + ffi.TYPE_TO_POINTER_METHOD_MAP[retType]](result);
    }
  });
  this.pointer = this._info.pointer;
}
