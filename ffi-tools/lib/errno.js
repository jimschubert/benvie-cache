var ffi = require('./ffi');

module.exports = errno;

var errnoPtr;

function errno(){
  if (errnoPtr) return errnoPtr().getInt32();
  switch (process.platform) {
    case 'darwin':
    case 'mac':
      var __error = new ffi.DLL().get('__error');
      errnoPtr = new ffi.ForeignFunction('errno', __error, 'pointer', []);
      break;
    case 'win32':
      var _errno = new ffi.DLL('msvcrt.dll').get('_errno')
      errnoPtr = new ffi.ForeignFunction('errno', _errno, 'pointer', [])
      break;
    default:
      var errnoGlobal = new ffi.DLL().get('errno');
      errnoPtr = function(){ return errnoGlobal }
  }
  return errnoPtr().getInt32();
}
