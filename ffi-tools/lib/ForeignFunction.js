var EventEmitter = require('events').EventEmitter

var ffi = require('./ffi');
var bindings = require('./bindings');
var POINTER_SIZE = bindings.POINTER_SIZE;
var NULL = ffi.NULL;
var Pointer = ffi.Pointer;

module.exports = ForeignFunction;

var libs = {};

function ForeignFunction(name, ptr, returnType, params, async) {
  if (!(this instanceof ForeignFunction)) {
    return new ForeignFunction(name, ptr, returnType, params, async);
  }

  var self = this;
  if (!Array.isArray(params)) {
    var names = Object.keys(params);
    params = names.map(function(name){ return params[name] });
  } else {
    var names = params.map(function(s,i){ return i });
  }

  if (typeof ptr === 'string') {
    if (!(ptr in libs)) {
      libs[ptr] = new ffi.DLL(ptr + ffi.Library.ext, ffi.DLL.FLAGS.RTLD_NOW);
    }
    ptr = libs[ptr].get(name);
    if (ptr.isNull()) {
      throw new Error('DynamicLibrary "'+this.name+'" returned NULL function pointer for "'+name+'"')
    }
  }

  var numArgs = params.length;
  var argsList = new Pointer(numArgs * POINTER_SIZE);

  var setters = params.map(function(type, i) {
    var argPtr = argsList.seek(i * POINTER_SIZE);

    if (ffi.isStructType(type)) {
      return function(v){
        argPtr.putPointer(v.pointer);
      };

    } else {
      var write = ffi.refValuePtrFunc(type);
      var valPtr = new Pointer(ffi.sizeOf(type));
      argPtr.putPointer(valPtr);

      return function(v){
        write(valPtr, v);
      };
    }
  });

  var result = new Pointer(ffi.sizeOf(returnType));
  var cif = this._ = new ffi.CIF(returnType, params);
  var caller = new bindings.ForeignCaller(cif.pointer, ptr, argsList, result, async);
  var deref = ffi.derefValuePtrFunc(returnType);

  var proxy = function(){
    self;
    if (arguments.length !== numArgs) throw new Error("Argument count didn't match");
    for (var i=0; i<numArgs; i++) {
      setters[i](arguments[i] === null ? NULL : arguments[i]);
    }
    var r = caller.exec();
    if (async) {
      var emitter = new EventEmitter;
      r.on('success', function(){ emitter.emit('success', deref(result)) });
      return emitter;
    }
    return deref(result);
  }
  Object.defineProperty(proxy, 'inspect', {
    value: function(){
      return '[Foreign Function: '+name+'('+names.map(function(name,i){
        return name + ': '+(typeof params[i] === 'string' ? params[i] : params[i].name);
      }).join(', ')+')]';
    }
  });
  return proxy;
}
