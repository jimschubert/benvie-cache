module.exports = Enum;

var enums = {};

function Enum(name, records){
  enums[name] = this;
  Object.defineProperties(this, {
    reverse: { value: {} },
    name: { value: name }
  });
  for (var k in records) {
    this.addRecord(k, records[k]);
  }
}

Enum.isEnum = function isEnum(n){
  return n instanceof Enum || typeof n === 'string' && enums.hasOwnProperty(n);
}

Enum.prototype = {
  addRecord: function addRecord(name, value){
    this[name] = value;
    this.reverse[value] = name;
  },
  has: function has(name){
    return this.hasOwnProperty(name);
  },
  get: function get(n){
    if (this.hasOwnProperty(n)) return this[n];
    if (this.reverse.hasOwnProperty(n)) return this.reverse[n];
    return null;
  }
};