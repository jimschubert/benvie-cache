var ffi = require('./ffi');
var bindings = require('./bindings');
var utility = require('./utility');


module.exports = Library;

function Library(name, functions) {
  utility.hidden(this, 'name', name);
  this._lib = new ffi.DLL(name ? name + Library.ext : null, ffi.DLL.FLAGS.RTLD_NOW);
  functions && this.createFunction(functions);
}

Library.ext = ffi.PLATFORM_EXTENSIONS[process.platform];


Library.prototype = {
  constructor: Library,

  set _lib(v){ Object.defineProperty(this, '_lib', { value: v }) },

  get filename(){ return this.name + Library.ext },

  toString: function toString(){
    return 'var ' + this.name + ' = new ffi.Library("'+this.name+'", {\n  ' + Object.keys(this).map(function(key){
      return key + ': ' + this[key];
    }, this).join(',\n  ') + '\n})';
  },

  createFunction: function createFunction(name, ret, params){
    if (typeof name === 'object') {
      // multiple functions were provided
      var self = this;
      return Object.keys(name).reduce(function(fns, name){
        fns[name] = createFunction.apply(self, [name].concat(fns[name]));
        return fns;
      }, name);
    }

    var handle = this._lib.get(name);

    if (handle.isNull()) {
      throw new Error('DynamicLibrary "'+this.name+'" returned NULL function pointer for "'+name+'"')
    }

    var paramNames = params ? Object.keys(params) : [];
    var paramTypes = paramNames.map(function(param){ return params[param] });
    var func = this[name] = new ffi.ForeignFunction(name, handle, ret, paramTypes);
    var code =  'function '+name+'('+paramNames.join(', ')+'){ [native code] }';
    Object.defineProperties(func, {
      toString: { value: function(){ return code } },
      params: { value: params }
    });
  }
};

utility.hide(Library.prototype, ['constructor', '_lib', 'filename', 'createFunction']);
