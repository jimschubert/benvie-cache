var util = require('util');

var ffi = require('./ffi');
var bindings = require('./bindings');
var Pointer = module.exports = bindings.Pointer;


Pointer.isPointer = function isPointer(p) {
  return p instanceof Pointer;
}

Pointer.alloc = function alloc(type, value) {
  var size = type == 'string'
           ? Buffer.byteLength(value, 'utf8') + 1
           : ffi.sizeOf(type);

  var ptr = new Pointer(size);

  ptr['put' + ffi.TYPE_TO_POINTER_METHOD_MAP[type]](value);

  if (type == 'string') {
    var dptr = new Pointer(ffi.TYPE_SIZES.pointer);
    ptr.attach(dptr);
    dptr.putPointer(ptr);
    return dptr;
  }
  return ptr;
}

Object.defineProperties(Pointer.prototype, {
  attach: {
    configurable: true,
    writable: true,
    value: function attach(f){
      (f.__attached = f.__attached || []).push(this);
    }
  },
  clone: {
    configurable: true,
    writable: true,
    value: function clone(){
      return this.seek(0);
    }
  },
  putPointer: {
    configurable: true,
    writable: true,
    value: function putPointer(ptr, seek) {
      return this._putPointer(ptr && 'pointer' in ptr ? ptr.pointer : ptr, seek);
    }
  }
});


Object.keys(ffi.NON_SPECIFIC_TYPES).forEach(function(type) {
  var method = ffi.NON_SPECIFIC_TYPES[type];
  var suffix = ffi.TYPE_TO_POINTER_METHOD_MAP[type];

  if (!suffix) {
    suffix = (type !== 'byte' && type !== 'size_t' && type[0] !== 'u') ? '' : 'U';
    suffix += ffi.SIZE_TO_POINTER_METHOD_MAP[ffi.sizeOf(type)];
  }

  ffi.TYPE_TO_POINTER_METHOD_MAP[type] = suffix;

  Pointer.prototype['put' + method] = Pointer.prototype['put' + suffix];
  Pointer.prototype['get' + method] = Pointer.prototype['get' + suffix];
});
ffi.NULL = new Pointer(0);