var read  = require('fs').readFileSync;
var ffi = require('./ffi');
var fns = require('./bindings').StaticFunctions;
var FF = ffi.ForeignFunction;
var dlopen =  new FF('dlopen', fns.dlopen, 'pointer', { path: 'string', flags: 'int32' });
var dlclose = new FF('dlclose', fns.dlclose, 'int32', { handle: 'pointer' });
var dlsym =   new FF('dlsym', fns.dlsym, 'pointer', { handle: 'pointer', symbol: 'string' });
var dlerror = new FF('dlerror', fns.dlerror, 'string', {});




module.exports = DLL;


function DLL(path, mode) {
  this._handle = dlopen(path, mode || DLL.FLAGS.RTLD_NOW);
  this.path = path;
  if (this._handle.isNull()) {
    var err = this.error();
    var match;

    if (match = err.match(/^(([^ \t()])+\.so([^ \t:()])*):([ \t])*invalid ELF header$/)) {
      var content = read(match[1], 'ascii');
      if (match = content.match(/GROUP *\( *(([^ )])+)/)){
        return DLL.call(this, match[1], mode);
      }
    }
    throw new Error('Dynamic Linking Error: ' + err);
  }
}

DLL.FLAGS = {
  RTLD_LAZY:    0x1,
  RTLD_NOW:     0x2,
  RTLD_LOCAL:   0x4,
  RTLD_GLOBAL:  0x8,
};

DLL.prototype = {
  constructor: DLL,
  close: function close(){
    return dlclose(this._handle);
  },
  get: function get(symbol){
    var ptr = dlsym(this._handle, symbol);
    if (ptr.isNull()) throw new Error(symbol+', Dynamic Symbol Retrieval Error: ' + this.error());
    return ptr;
  },
  error: function error() {
    return dlerror();
  }
};

