module.exports = {
  numbers: numbers,
  Desc: Desc
}

function Desc(value, hidden, readonly){
  this.value = value;
  if (hidden) this.enumerable = false;
  if (readonly) this.writable = false;
}
Desc.prototype = { enumerable: true, writable: true, configurable: true };


var flatten = Function.apply.bind([].concat, []);

function enumerate(obj){
  var props = [], k=0;
  for (props[k++] in obj);
  return props;
}

function inArrays(value){
  for (var i=1; i<arguments.length; i++) {
    if (~arguments[i].indexOf(value)) return true;
  }
  return false;
}

function notIn(v){
  return !~this.indexOf(v);
}

function unique(a){
  return Object.keys(a.reduce(function(r,s){return r[s]=1,r},{}));
}


function numbers(start, end){
  if (!isFinite(end)) {
    end = start;
    start = 0;
  }
  var length = end - start;
  if (end > numbers.cache.length) {
    while (length--) numbers.cache[length+start] = String(length+start);
  }
  return numbers.cache.slice(start, end);
}

numbers.cache = [];