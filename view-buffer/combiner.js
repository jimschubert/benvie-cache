var Combiner = function(){
"use strict";
if (typeof require === 'function') {
  var EasyProxy = require('easy-proxy');
  var numbers = require('./utility').numbers;
  var Desc = require('./utility').Desc;
}

module.exports = Combiner;

function Combiner(sources){
  var target = this instanceof Combiner ? this : Object.create(Combiner.prototype);
  Object.defineProperty(target, 'sources', {
    value: arguments.length ? arguments.length > 1 ? [].slice.call(arguments) : sources.slice() : [],
    configurable: true
  });
  return EasyProxy(target, handler);
}

Combiner.prototype = {
  constructor: Combiner,
  forEach: Array.prototype.forEach,
  reduce:  Array.prototype.reduce,
  map:     Array.prototype.map,
  reverse: Array.prototype.reverse,
  join:    Array.prototype.join,

  fill: function fill(v, start, end){
    start = +start || 0;
    end = isFinite(end) ? +end : this.length;
    while (end-- > start) {
      this[end] = v;
    }
  },

  clone: function clone(to){
    if (typeof to === 'function') {
      to = new to(this.length);
    }
    this.copy(to);
    return to;
  },
  write: function write(value, start, length){
    start = +start || 0;
    length = isFinite(length) ? length : value.length;
    length = Math.min(this.length - start, length);

    while (length--) {
      this[start+length] = value[length];
    }

    return this;
  },
  copy: function copy(target, offset, start, end){
    offset = offset > 0 ? +offset : 0;
    start = start > 0 ? +start : 0;
    end = end ? Math.min(+end, this.length) : this.length;

    var length = end - start;

    if (!Array.isArray(target)) {
      length = Math.min(length, target.length - offset);
    }

    while (length--) {
      target[offset+length] = this[start+length];
    }

    return target;
  },
  append: function append(){
    [].push.apply(this.sources, arguments[0]);
    return this.length;
  },
  push: function append(){
    [].push.apply(this.sources, arguments);
    return this.length;
  },
  pop: function pop(){
    return this.sources.pop();
  },
  toArray: function toArray(){
    return this.map(function(x){ return x });
  },
  toString: function toString(){
    return this.join();
  }
}



function setter(target,i,v){
  var info = lookup(target, i);
  if (!info) throw new RangeError('Index is out of bounds');
  info[0][info[1]] = v;
  return v;
}

function getter(target,i){
  var info = lookup(target, i);
  if (info) return info[0][info[1]];
}

function lookup(target,i){
  var index = 0;
  var sources = target.sources;
  while ((i -= sources[index].length) >= 0) {
    if (++index === sources.length) return null;
  }
  return [sources[index], i+sources[index].length];
}

function length(target){
  var i = target.sources.length, total = 0;
  while (i--) total += target.sources[i].length;
  return total;
}


function list(fwd, target){
  return numbers(length(target)).concat('length').concat(fwd());
}
function has(fwd, target, name){
  return isFinite(name) ? name < length(target) : fwd();
}

var handler = {
  names: list,
  enumerate: list,
  keys: list,
  has: has,
  owns: has,

  describe: function(fwd, target, name){
    if (isFinite(name)) {
      return new Desc(getter(target, name));
    } else if (name === 'length') {
      return new Desc(length(target), false, true);
    } else {
      return fwd();
    }
  },

  define: function(fwd, target, name, desc){
    if (!desc) desc = { value: undefined };
    if (isFinite(name)) {
      return setter(target, name, desc.value);
    } else if (name !== 'length') {
      return fwd();
    }
  },

  get: function(fwd, target, name, own, rcvr){
    if (isFinite(name)) {
      return getter(target, name);
    } else if (name === 'length') {
      return length(target);
    } else {
      return fwd();
    }
  },

  set: function(fwd, target, name, val, rcvr){
    if (isFinite(name)) {
      return setter(target, name, val);
    } else if (name !== 'length') {
      return fwd()
    }
  },
}


if (typeof module !== 'undefined') module.exports = Combiner;
return Combiner;
}();
