var Matrix = function(){
"use strict";
if (typeof require === 'function') {
  var EasyProxy = require('easy-proxy');
  var numbers = require('./utility').numbers;
  var Desc = require('./utility').Desc;
}

function Matrix(width, height, values){
  if (!Matrix.prototype.isPrototypeOf(this)) {
    return new Matrix(width, height, values);
  }
  if (typeof height === 'undefined' && width && width.length) {
    values = width;
    width = Math.pow(values.length, 0.5) | 0;
    height = width + 1;
  }
  this.originWidth = this.width = num(width);
  this.originHeight = this.height = num(height);
  this.left = 0;
  this.top = 0;
  if (this.width <= 0) throw new RangeError('Width must be greater than zero');
  if (this.height <= 0) throw new RangeError('Height must be greater than zero');

  values = values || 0;

  if (values.length && values[values.length-2]) {
    this.backing = values;
  } else {
    this.backing = dense(width, height);
    if (typeof values === 'function' || values.length) {
      this.write(values);
    } else {
      this.fill(values);
    }
  }
  return EasyProxy(this, matrixHandler);
}



Matrix.prototype = {
  constructor: Matrix,
  indexer: 'width',
  flip: function flip(){
    return this.indexer = this.indexer === 'height' ? 'width' : 'height';
  },

  get: function get(column, row){
    if (row == null) {
      return this.indexer === 'height' ? this.getRow(column) : this.getColumn(column);
    } else {
      column = this.left + num(column);
      row = this.top + num(row);
      var offset = row * this.originWidth + column;
      if (offset < this.backing.length) {
        return this.backing[offset];
      }
    }
  },

  set: function set(column, row, value){
    if (value == null) {
      return this.indexer === 'height' ? this.setRow(column, row) : this.setColumn(column, row);
    } else {
      column = this.left + num(column);
      row = this.top + num(row);
      var offset = row * this.originWidth + column;
      if (offset < this.backing.length) {
        return this.backing[offset] = value;
      }
    }
  },
  fill: function fill(v){
    v = v || 0;
    for (var x=0; x < this.width; x++) {
      for (var y=0; y < this.height; y++) {
        this.set(x, y, v);
      }
    }
  },
  getColumn: function getColumn(column){
    if (column >= this.width) throw new RangeError('Column is out of bounds: '+this.width+'/'+column);
    return Column(this, column);
  },
  setColumn: function setColumn(column, value){
    if (column >= this.width) throw new RangeError('Column is out of bounds: '+this.width+'/'+column);
    Column(this, column).write(value);
  },
  getRow: function getRow(row){
    if (row >= this.height) throw new RangeError('Row is out of bounds: '+row);
    return Row(this, row);
  },
  setRow: function setRow(row, value){
    if (row >= this.height) throw new RangeError('Row is out of bounds: '+row);
    Row(this, row).write(value);
  },
  write: function write(settings, values){
    var callback;
    if (typeof settings === 'function') {
      callback = settings;
      settings = null;
    } else if (!values) {
      values = settings;
      settings = null;
    }
    if (!settings && values && values.width && values.height) {
      settings = [0, 0, values.width, values.height];
    }

    settings = makeView(this, settings);

    if (!callback) {
      if (typeof values === 'function') {
        callback = values;
      } else if (values.width && values.height) {
        if (values.get) {
          callback = function(x,y){ return values.get(x,y) };
        } else {
          callback = function(x,y){ return values[x][y] };
        }
      } else if (values.length) {
        callback = function(x,y){ return values[this.width*y+x] };
      } else {
        throw new TypeError('Could not determine how to write value');
      }
    }

    for (var x=settings[0]; x < settings[2]; x++) {
      for (var y=settings[1]; y < settings[3]; y++) {
        this.set(x, y, callback.call(this, x, y));
      }
    }

    return this;
  },

  toString: function toString(){
    return this.join('', '\n');
  },
  join: function join(x,y){
    return this.toArray().map(function(row){
      return row.join(x);
    }).join(y);
  },
  toArray: function toArray(){
    var out = [];
    for (var i=0; i < this[this.indexer]; i++) {
      out.push(this.get(i).toArray());
    }
    return out;
  },
  get bottom(){
    return this.top + this.height;
  },
  get right(){
    return this.left + this.width;
  },
  subview: function subview(x1, y1, x2, y2){
    var view = makeView(this, [x1, y1, x2, y2]);
    var inst = Object.create(Object.getPrototypeOf(this));
    inst.originWidth  = this.originWidth;
    inst.originHeight = this.originHeight;
    inst.left         = this.left + view[0];
    inst.top          = this.top + view[1];
    inst.width        = view[2] - view[0];
    inst.height       = view[3] - view[1];
    inst.backing      = this.backing;
    return Proxy(inst, matrixHandler);
  }
}

function notIn(v){ return !~this.indexOf(v) }

var matrixHandler = {
  hidden: ['originWidth', 'originHeight', 'left', 'top', 'right', 'bottom', 'backing', 'indexer'],
  names: function(fwd, target){
    return numbers(target[target.indexer]).concat(fwd(), 'length');
  },
  keys: function(fwd, target){
    return numbers(target[target.indexer]).concat(fwd().filter(notIn, this.hidden));
  },
  enumerate: function(fwd, target){
    return numbers(target[target.indexer]).concat(fwd().filter(notIn, this.hidden));
  },
  has: function(fwd, target, name){
    if (isFinite(name)) {
      return name < target[target.indexer];
    } else {
      return fwd();
    }
  },
  owns: function(fwd, target, name){
    if (isFinite(name)) {
      return name < target[target.indexer];
    } else {
      return fwd();
    }
  },
  describe: function(fwd, target, name){
    if (isFinite(name)) {
      if (name < target[target.indexer]) return new Desc(target.get(name));
    } else if (name === 'length') {
      return new Desc(target[target.indexer], true);
    } else {
      return fwd();
    }
  },
  get: function(fwd, target, name, rcvr){
    if (isFinite(name)) {
      if (name < target[target.indexer]) return target.get(name);
    } else if (name === 'length') {
      return target[target.indexer];
    } else {
      return fwd();
    }
  },
  define: function(fwd, target, name, desc){
    if (isFinite(name)) {
      if ('value' in desc && desc.value < target[target.indexer]) {
        target.set(name, desc.value);
      }
      return true;
    } else {
      return fwd();
    }
  },
  set: function(fwd, target, name, val, rcrv){
    if (isFinite(name) && name < target[target.indexer]) {
      target.set(name, val);
    } else {
      fwd();
    }
    return true;
  },
  delete: function(fwd, target, name){
    if (!isFinite(name)) {
      return fwd();
    } else {
      return false;
    }
  }
};





var columns = new WeakMap;

function Column(subject, index){
  var cache = columns.has(subject) ? columns.get(subject) : columns.set(subject, []);
  if (cache[index]) return cache[index];

  var col = this instanceof Column ? this : Object.create(Column.prototype);
  col.matrix = subject;
  col.index = index || 0;
  return cache[index] = EasyProxy(col, colRowHandler);
}

Column.prototype = {
  __proto__: Array.prototype,
  get length(){
    return this.matrix.height;
  },
  get: function get(index){
    return this.matrix.get(this.index, index);
  },
  set: function set(index, value){
    return this.matrix.set(this.index, index, value);
  },
  write: function write(value){
    var len = this.length;
    for (var i=0; i < len && i < value.length; i++) {
      this.set(i, value[i]);
    }
  },
  toArray: function toArray(){
    var len = this.length;
    var out = [];
    for (var i=0; i < len; i++) {
      out[i] = this.get(i);
    }
    return out;
  }
};


var rows = new WeakMap;

function Row(subject, index){
  var cache = rows.has(subject) ? rows.get(subject) : rows.set(subject, []);
  if (cache[index]) return cache[index];

  var row = this instanceof Row ? this : Object.create(Row.prototype);
  row.matrix = subject;
  row.index = index || 0;
  return cache[index] = EasyProxy(row, colRowHandler);
}

Row.prototype = {
  __proto__: Array.prototype,
  get length(){
    return this.matrix.width;
  },
  get: function get(index){
    return this.matrix.get(index, this.index);
  },
  set: function set(index, value){
    return this.matrix.set(index, this.index, value);
  },
  write: function write(value){
    var len = this.length;
    for (var i=0; i < len && i < value.length; i++) {
      this.set(i, value[i]);
    }
  },
  toArray: function toArray(){
    var len = this.length;
    var out = [];
    for (var i=0; i < len; i++) {
      out[i] = this.get(i);
    }
    return out;
  }
};


var colRowHandler = {
  hidden: ['length', 'index', 'matrix'],
  names: function(fwd, target){
    return numbers(target.length).concat(fwd());
  },
  keys: function(fwd, target){
    return numbers(target.length).concat(fwd().filter(notIn, this.hidden));
  },
  enumerate: function(fwd, target){
    return numbers(target.length).concat(fwd().filter(notIn, this.hidden));
  },
  describe: function(fwd, target, name){
    if (isFinite(name)) {
      return new Desc(target.get(name));
    } else if (name === 'length') {
      return new Desc(target.length, true);
    } else {
      return fwd();
    }
  },
  has: function(fwd, target, name){
    if (isFinite(name)) {
      return name < target.length;
    } else {
      return name === 'length' || fwd();
    }
  },
  owns: function(fwd, target, name){
    if (isFinite(name)) {
      return name < target.length;
    } else {
      return name === 'length' || fwd();
    }
  },
  get: function(fwd, target, name, rcvr){
    if (isFinite(name)) {
      if (name < target.length) return target.get(name);
    } else if (name === 'length') {
      return target.length;
    } else {
      return fwd();
    }
  },
  set: function(fwd, target, name, value, rcvr){
    if (isFinite(name)) {
      if (name < target.length) {
        return target.set(name, value);
      }
    } else {
      return fwd();
    }
  },
  define: function(fwd, target, name, desc){
    if (isFinite(name)) {
      if (name < target.length && 'value' in desc) {
        return target.set(name, desc.value);
      }
    } else {
      return fwd();
    }
  },
  delete: function(fwd, target, name){
    if (!isFinite(name) && name !== 'length') {
      return fwd();
    } else {
      return false;
    }
  }
};


function translate(val, def, max){
  val = isFinite(val) ? +val : def;
  if (1 / val === -Infinity) return max;
  if (val > -1 && val < 1) val = (val * max + 0.5) | 0;
  return val >= 0 ? val : val + max;
}

function dense(width, height){
  return Array.apply(null, new Array(width * height || 1));
}
function num(n){ return isFinite(n) ? +n : 0 }
function isPercent(v){ return v >= -1 && v <= 1 }

function makeView(matrix, span){
  if (span && span.length) {
    if (isPercent(span[0]) && isPercent(span[2])) {
      span[0] *= matrix.width | 0;
      span[2] *= matrix.width | 0;
    }
    if (isPercent(span[1]) && isPercent(span[3])) {
      span[1] *= matrix.height | 0;
      span[3] *= matrix.height | 0;
    }
    span[0] = translate(span[0], 0, matrix.width);
    span[1] = translate(span[1], 0, matrix.height);
    span[2] = translate(span[2], matrix.width-span[0], matrix.width);
    span[3] = translate(span[3], matrix.height-span[1], matrix.height);
    return span;
  } else {
    return [ 0, 0, matrix.width, matrix.height ];
  }
}


if (typeof module !== 'undefined') module.exports = Matrix;
return Matrix;
}();
