# Advanced Buffers

Using the magic of Proxies to bring new types of buffers to JavaScript.

* ViewBuffer is a polymorphous buffer interface that allows each instance to encompass all the features of DataView, Buffer, and the Typed Arrays.
* MatrixBuffer wraps vector objects like buffers in a two dimensional interface, allowing two level indexed access and various transforms. It also allows for the equivelent of subarrays on arbitrary rectangular sections and nesting.
* CombinerBuffer wrap any set of vector objects like buffers and presents a single combined index that addresses them in the order they're organized in the internal array. Items can be pushed onto the end of the internal array. Changes to size are automatically reflected.

```
npm install view-buffer
```

# Example

```javascript
var b = require('./view-buffer');

var str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
var buff = new b.ViewBuffer(str+str+str);
var test1 = new b.MatrixBuffer(buff);
var test2 = test1.subview(3, 3, -4, -3);
var test3 = test2.subview(2, 2, -2, -2);
test2.fill(' ');
test3.write(test1)

test1.join('','\n')
//-->
0123456789abcd
efghijklmnopqr
stuvwxyzABCDEF
GHI        RST
UVW        567
89a  0123  jkl
mno  efgh  xyz
ABC  stuv  LMN
OPQ        Z01
234        def
ghijklmnopqrst
uvwxyzABCDEFGH
IJKLMNOPQRSTUV


//and the original buffer
//-->
0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHI        RSTUVW        56789a  0123  jklmno  efgh  xyzABC  stuv  LMNOPQ        Z01234        defghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ

```



# API

Functions from Array.prototype: forEach, map, reduce, reduceRight, join, reverse

## ViewBuffer

* __get(index, [view])__, __set(index, value, [view])__: functional version of indexed accessors also allowing a specific view. If the view has a different size index is in terms of that view.
* __setView(view)__: functional way to set view property
* __subarray([start], [end])__: new ViewBuffer using the same underlying buffer where start and end are indexes based on the current view
* __slice([start], [end])__: like subarray but it's always based on bytes
* __write(value, [start], [length], [view])__: write a value with indexes and a length like a string, array, or buffer. Writes based on indexes relative to current view or the optionally provided one
* __clone()__: create's a new buffer and copies the current ones' data to it using the current view
* __fill([v], [start], [end])__: fill with a value or zero, from start to end of 0 to length if none given. Relative to current view's indexes
* __toString([encoding])__: convert to encoding or current string format or hex if current is numeric and joins to a string
* __copy(target, [targetOffset], [start], [end])__: copy data from current buffer to target. Target can be anything with indices and a length. Iterates based on current view.

__views__

* numeric - Int8, Uint8, Int16, Uint16, Int32, Uint32, Float32, Float64
* string - Ascii, Ucs2, Hex
* other - Binary (individual bits)


### MatrixBuffer
The matrix is used over top an existing buffer to view it two dimensionally. Any underlying object that provides a lengrg and indexed properties (string, array, any kind of buffer) can act as the backing for this.

* __subview__(left, top, right, bottom): create a new view on top of a matrix that scopes in to a particular part. Positive numbers are from the top/left, negative numbers measure from the bottom/right. -0 works to specify the very right or bottom. Decimals work as percentages.
* __fill__(value): fill with a value.
* __write__(settings, values): settings is an array which will have missing values filled to the tune of [0,0,width,height] to specify where to write. Values can either be a matching 2d vector structure or a callback that will be called for each index to generate the value.
* __join__(x, y): two dimensional version of join
* __toArray__: returns a nested array with the same structure
* __bisect__(cols, rows): splits the matrix into equal part sub-matrices covering the same area


### CombinerBuffer

A combiner buffer's main job is to take any amount of input vector objects and provide a single "buffer" that combines them all. The exposed interface is indexed and has a length so it's a suitable tarket for most array/buffer apis. The magic is that it will dynamically reflect any changes to the underlying data objects transparently, making it incredibly versatile.

* __fill__(value, start end): write a single value
* __clone__(to): copy the entire buffer onto target object. You must provide the object, as opposed to most clone functions.
* __write__(input, start, length): write a vector value to the indices of the Combiner
* __copy__(target, offset, start, end): write values from the combiner to a target
* __append__(...): adds all the arguments to be data sources for the buffer. This doesn't added data to the end, it adds data providers.
* __push__(): Add a data provider to the end of the internal set.
* __pop__(): Remove a single data provider from the end.
* __toArray__(): Converts to array using indices.

# Usage


### buffer creation

A ViewBuffer can be used to wrap an existing Buffer or TypedArray or it can be used to create new buffers instead of using ArrayBuffer or Buffer. The constructor can handle the same options as other buffers as well as a few others. The simplest usage is simply passing a size in bytes.

```javascript
var ViewBuffer = require('view-buffer');

var b = ViewBuffer(10);
b[3] = 100;
{ view: 'Uint8',
  bytes: 10,
  length: 10,
  '0': 0,
  '1': 0,
  '2': 0,
  '3': 100,
  '4': 0,
  '5': 0,
  '6': 0,
  '7': 0,
  '8': 0,
  '9': 0 }
```

### Changings Views
Views are important because most of the functions ViewBuffer has iterate based on the current view's length and indices, and values are formatted based on the current view.

```javascript
b.view = 'uint32';
{ view: 'Uint32',
  bytes: 10,
  length: 2,
  '0': 1677721600,
  '1': 0 }

b.view = 'uint16';
{ view: 'Uint16',
  bytes: 10,
  length: 5,
  '0': 0,
  '1': 25600,
  '2': 0,
  '3': 0,
  '4': 0 }
```
### Strings
When writing a string the default will be to use ascii and one byte per character. The view will automatically be changed internally to do the write and then changed back. Changing the view to a text format will cause the returned values to actually be strings.

```javascript
b.write('viewbuffer')
//oops
{ view: 'Uint16',
  bytes: 10,
  length: 5,
  '0': 26998,
  '1': 30565,
  '2': 30050,
  '3': 26214,
  '4': 29285 }

b.view = 'ascii';
{ view: 'Ascii',
  bytes: 10,
  length: 10,
  '0': 'v',
  '1': 'i',
  '2': 'e',
  '3': 'w',
  '4': 'b',
  '5': 'u',
  '6': 'f',
  '7': 'f',
  '8': 'e',
  '9': 'r' }
```

The magic of the ViewBuffer is that it allows for seamlessly abstracting away the underlying data sizes and offsets and types. Writing from a ascii view to a ucs2 view is simple.

```javascript
var ucs2 = ViewBuffer('ucs2', 20);
b.copy(ucs2);
{ view: 'Ucs2',
  bytes: 20,
  length: 10,
  '0': 'v',
  '1': 'i',
  '2': 'e',
  '3': 'w',
  '4': 'b',
  '5': 'u',
  '6': 'f',
  '7': 'f',
  '8': 'e',
  '9': 'r' }

ucs2.view = 'ascii';
{ view: 'Ascii',
  bytes: 20,
  length: 20,
  '0': 'v',
  '1': '\u0000',
  '2': 'i',
  '3': '\u0000',
  '4': 'e',
  '5': '\u0000',
  '6': 'w',
  '7': '\u0000',
  '8': 'b',
  '9': '\u0000',
  '10': 'u',
  '11': '\u0000',
  '12': 'f',
  '13': '\u0000',
  '14': 'f',
  '15': '\u0000',
  '16': 'e',
  '17': '\u0000',
  '18': 'r',
  '19': '\u0000' }
```