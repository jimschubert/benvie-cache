var ViewBuffer = function(){
"use strict";
if (typeof require === 'function') {
  var EasyProxy = require('easy-proxy');
  var numbers = require('./utility').numbers;
  var Desc = require('./utility').Desc;
}
var has = Function.call.bind({}.hasOwnProperty);




// ##############################
// ### ViewBuffer Constructor ###
// ##############################

function ViewBuffer(){
  var args = [].slice.call(arguments);
  var buffer, type, subject = args[0], data, length;
  if (typeof subject === 'string') {
    if (subject.length < 10) {
      type = subject[0].toUpperCase() + subject.slice(1).toLowerCase();
      if (has(ViewBuffer.types, type)) {
        subject = args[1];
        args = [].slice.call(args, 1);
      } else {
        type = 'Ascii';
      }
    } else {
      type = 'Ascii';
    }
    if (typeof subject === 'string') {
      data = subject;
      length = type === 'Ascii' ? data.length : data.length * 2;
      subject =  args[0];
      args = [].slice.call(args, 1);
    }
  }
  if (Array.isArray(subject)) {
    data = subject;
    length = data.length;
    subject = args[0];
    args = [].slice.call(args, 1);
  }
  if (data) {
    var start = isFinite(args[1]) ? +args[1] : 0;
    length = length || +args[2];
    buffer = new Buffer(length);
  } else {
    if (isFinite(subject)) {
      buffer = Buffer.apply(null, args);
    } else if (args.length === 1 && Buffer.isBuffer(subject)) {
      buffer = subject;
    } else if (subject instanceof ArrayBuffer) {
      buffer = subject;
    } else if (!Array.isArray(subject) && subject.constructor.name.slice(-5) === 'Array') {
      buffer = subject;
    } else {
      buffer = Buffer.apply(null, args);
    }
  }
  var data = new DataView(buffer);
  Object.defineProperties(this, {
    data: { value:data },
    byteLength: new ReadOnly(data.byteLength)
  });

  var proxy = EasyProxy(this, handler);
  proxy.view = type || 'Uint8';
  handlers.set(proxy, this);
  if (data) proxy.write(data);
  return proxy;
}

// ###############################
// ### Proxy Handler Prototype ###
// ###############################

var handler = {
  names : function(fwd, target){
    return numbers(target.length).concat(fwd());
  },
  enumerate: function(fwd, target){
    return numbers(target.length).concat(fwd());
  },
  keys: function(fwd, target){
    return numbers(Math.min(ViewBuffer.INSPECT_MAX_BYTES, target.length)).concat(fwd());
  },
  has: function(fwd, target, name){
    return name < target.length || fwd();
  },
  owns: function(fwd, target, name){
    return isFinite(name) ? name < target.length : fwd();
  },
  describe: function(fwd, target, name, rcvr){
    if (isFinite(name)) {
      return new Desc(target.get(name));
    } else {
      return fwd();
    }
  },
  get: function(fwd, target, name, rcvr){
    if (isFinite(name)) {
      return target.get(name);
    } else {
      return fwd();
    }
  },
  set: function(fwd, target, name, val, rcvr){
    if (isFinite(name)) {
      target.set(name, val);
    } else if (name === 'view') {
      val = val[0].toUpperCase() + val.slice(1).toLowerCase();
      if (has(nameMap, val)) val = nameMap[val];
      if (!has(ViewBuffer.types, val)) throw new Error('Unknown data type');
      target.bytesEach = ViewBuffer.types[val][0];
      target.view = val;
      target.length = target.byteLength / target.bytesEach | 0;
      target.overflow = target.byteLength % target.bytesEach;
    } else {
      fwd();
    }
    return true;
  },
};





function getter(target, type, index){
  return target.data['get'+type](index * ViewBuffer.types[type][0], target.endian === 'little');
}

function setter(target, type, index, value){
  return target.data['set'+type](index * ViewBuffer.types[type][0], value, target.endian === 'little');
}

// ############################
// ### ViewBuffer Prototype ###
// ############################

ViewBuffer.prototype = {
  constructor: ViewBuffer,

  forEach: Array.prototype.forEach,
  reduce:  Array.prototype.reduce,
  map:     Array.prototype.map,
  reverse: Array.prototype.reverse,
  join:    Array.prototype.join,

  endian: 'little',

  get: function get(i){
    switch (this.view) {
      case 'Ascii':
        return String.fromCharCode(getter(this, 'Uint8', i));
      case 'Ucs2':
        return ucs2(getter(this, 'Uint16', i));
      case 'Hex':
        return ('00'+getter(this, 'Uint8', i).toString(16)).slice(-2);
      case 'Binary':
        return getter(this, 'Uint8', i / 8 | 0) & powers[i] ? 1 : 0;
      default:
        return getter(this, this.view, i);
    }
  },

  set: function set(i, v){
    switch (this._view) {
      case 'Ascii':
        return setter(this, 'Uint8', i, typeof v === 'string' ? v.charCodeAt(0) : v);
      case 'Ucs2':
        return setter(this, 'Uint16', i, typeof v === 'string' ? ucs2(v) : v);
      case 'Hex':
        return setter(this, 'Uint8', i, Number('0x'+v));
      case 'Binary':
        var power = powers[i];
        var current = getter(this, 'Uint8', i = i / 8 | 0);
        return setter(this, 'Uint8', i, v ? current | power : current & ~power);
      default:
        if (isFinite(v)) v = +v;
        else if (typeof v === 'string') v = ucs2(v);
        else throw new TypeError('Unable to write to buffer: ' + v);
        return setter(this, this.view, i, v);
    }
  },

  subarray: function subarray(start, end, view){
    if (typeof start === 'string' && isNaN(start)) {
      view = start;
      start = 0;
    }
    start = (+start || 0) * this.bytesEach;
    end = (+end || this.length) * this.bytesEach;
    var buff = new ViewBuffer(new Uint8Array(this.data).subarray(start, end));
    buff.endian = view || this.endian;
    buff.view = this.view;
    return buff;
  },

  slice: function slice(start, end, view){
    if (typeof start === 'string' && isNaN(start)) {
      view = start;
      start = 0;
    }
    start = +start || 0;
    end = +end || this.byteLength;
    var buff = new ViewBuffer(new Uint8Array(this.data).subarray(start, end));
    buff.view = view || this.view;
    buff.endian = this.endian;
    return buff;
  },

  write: function write(value, start, length, view){
    if (!view && typeof value === 'string' && this.view !== 'Ucs2') view = 'Ascii';
    if (view) this.view = view;
    start = +start || 0;
    if (isNaN(length)) {
      length = isFinite(value.length) ? value.length : this.length;
      length = length > this.length - start ? this.length - start : +length;
    }
    if (start < 0) throw new RangeError('Start less than zero');
    if (length < 0) throw new RangeError('Length less than zero');
    if (start + length > this.length) throw new RangeError('Start + length is greater than this length');

    while (length--) {
      this[start+length] = value[length];
    }
    if (view) this.view = null;
    return this;
  },

  clone: function clone(){
    var buffer = new ViewBuffer(this.view, this.length);
    for (var i=0; i < this.length; i++) {
      buffer[i] = this[i];
    }
    return buffer;
  },

  fill: function fill(v, start, end){
    start = +start || 0;
    end = isFinite(end) ? +end : this.length;
    while (end-- > start) {
      this[end] = v;
    }
  },

  toString: function toString(encoding){
    if (!encoding) {
      if (this.view === 'Ascii' || this.view === 'Ucs2' || this.view === 'Binary') {
        encoding = this.view;
      } else {
        encoding = 'Hex';
      }
    }
    this.view = encoding;
    var out = this.join(encoding === 'Hex' ? ' ' : '');
    this.view = null;
    return out;
  },

  copy: function copy(target, targetOffset, start, end){
    if (isFinite(target)) {
      end = start, start = targetOffset, targetOffset = target;
      target = null;
    }
    targetOffset = +targetOffset || 0;
    start = +start || 0;
    end = end ? +end : this.length;
    if (start > end) throw new Error('End less than start');
    if (start < 0) throw new RangeError('Start less than zero');
    end = Math.min(end, this.length);
    var length = end - start;
    if (!target) {
      target = new ViewBuffer(length);
      target.view = this.view;
      target.endian = this.endian;
    } else if (!Array.isArray(target)) {
      if (targetOffset + length > target.length) {
        length = target.length;
      }
    }

    for (var i=0; i<length; i++) {
      target[targetOffset + i] = this[start+i];
    }
    return target;
  },
}



// ###################################
// ### Interface Static Properties ###
// ###################################

Object.defineProperties(ViewBuffer, {
  INSPECT_MAX_BYTES: {
    value: require('buffer').INSPECT_MAX_BYTES,
    writable: true,
    configurable: true
  },
  types: {
    value: {
      Int8:     8,
      Uint8:    8,
      Int16:   16,
      Uint16:  16,
      Int32:   32,
      Uint32:  32,
      Float32: 32,
      Float64: 64,
      Ascii:    8,
      Ucs2:    16,
      Hex:      8,
      Binary:   1
    },
    writable: true,
    configurable: true
  }
});

Object.keys(ViewBuffer.types).forEach(function(name){
  ViewBuffer.types[name] = [ViewBuffer.types[name] / 8, ViewBuffer.types[name]];
});

var nameMap = {
  Float: 'Float32',
  Double: 'Float64',
  'Ucs-2': 'Ucs2',
};



// ###################################
// ### String and Number utilities ###
// ###################################

var char = String.fromCharCode;
var code = Function.call.bind(''.charCodeAt);

var ucs2 = function(){
  var a = 0x3ff,
      o = 0x10000,
      x = o - 0x800,
      y = o - 0x2400,
      z = o - 0x2800;
  return function ucs2(v) {
    if (typeof v === 'string') {
      var r = code(v, 0);
      return r & x === z ? o + ((r & a) << 10) + (code(v, 1) & a) : r;
    } else if (isFinite(v)) {
      return v >= o ? char((v -= o) >>> 10 & a | z) + char(y | v & a) : char(v);
    }
  };
}();



var powers = numbers(32).map(Math.pow.bind(null, 2));

if (typeof module !== 'undefined') module.exports = ViewBuffer;
return ViewBuffer;
}();
