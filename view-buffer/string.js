var EasyProxy = require('easy-proxy');
var Combiner = require('view-buffer').Combiner;
var ViewBuffer = require('view-buffer').ViewBuffer;
var Tracer = require('easy-proxy/examples').Tracer;
var Grid = require('view-buffer').Grid;




function FakeString(string){
  return Combiner.call(Object.create(FakeString.prototype), new String(string));
}

FakeString.prototype = {
  __proto__: String.prototype,
  map: Array.prototype.map,
  toString: function toString(){
    return this.map(String).join('');
  },
  valueOf: function valueOf(){
    return this.map(String).join('');
  }
}


var page = new Grid('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');

console.log(page.subgrid(2, 2));