var tap = require('tap');
var test = tap.test;
var ViewBuffer
var b;


test('initialize', function(t){
  t.ok(ViewBuffer = require('../view-buffer'), 'ViewBuffer loaded');
  t.ok(b = ViewBuffer(1), 'created a buffer');
  t.end();
});


var names = [ 'view', 'bytes', 'length', 'endian',  'excess', 'offset', 'bytesEach', '0' ]
var enums = [ 'view','bytes','length','forEach','reduce','map','reverse','join',
              'get','set','setView','subarray','slice','write','clone','fill','copy','0' ];

test('basics', function(t){
  b.fill(100);
  t.equal(Object.getPrototypeOf(b), ViewBuffer.prototype, 'correct prototype');
  t.equivalent(names.reduce(function(r,s){if (s !== 'offset') r[s]=b[s];return r },{}),{
    '0': 100,
    view: 'Uint8',
    bytes: 1,
    length: 1,
    endian: 'little',
    excess: 0,
    bytesEach: 1
  }, 'correct starting properties');
  t.similar(Object.getOwnPropertyNames(b), names, 'getOwnPropertyNames shows right properties');
  var k=[],i=0;
  for (k[i++] in b);
  t.similar(k, enums, 'Enumeration shows right properties');
  t.equal(b.get(0), 100, 'get');
  b.set(0, 200);
  t.equal(b.get(0), 200, 'set');
  var clone = b.clone();
  t.similar(b, clone, 'clone starts similar');
  b[0] = 10;
  t.notSimilar(b, clone, 'clone has different data');
  clone.write(b);
  t.similar(b, clone, 'write');
  var sub = b.subarray(0, 1);
  t.similar(b, sub, 'subarray create');
  b[0] = 123;
  t.similar(b, sub, 'subarray uses same data');
  sub = sub.slice(0, 1);
  b[0] = 222;
  t.similar(b, sub, 'slice uses same data');
  t.end();
});

test('more stuff', function(t){
  b = new ViewBuffer(50);
  b.fill(255, 0, 25);
  b.fill(100, 25, 50);
  t.similar(b.subarray(20, 30), [255, 255, 255, 255, 255, 100, 100, 100, 100, 100]);
  b.view = 'uint32';
  t.same(b[0], 4294967295);
  t.end();
});

