# Environment

Provides node's `process.env` object functionality as stand-alone functions instead of a C++ fake object that does weird things. As an added bonus, if running node with Proxies enabled ("node --harmony") it also exports a function that creates an `Environment` object that is similar to `process.env` but behaves like a real object.

# Install

		npm install environment

# Use
Mostly self-explanatory:

* __environment.get(key)__
* __environment.set(key, value)__
* __environment.delete(key)__
* __environment.enumerate()__

# Environment object
If `Proxy` is available, due to running node using the `--harmony` flag, a constructor is exported as well which allows the creation of `Environment` objects.

These objects aren't entirely normal JavaScript objects, but they're much closer than node's `process.env`. `Environment.prototype` doesn't inherit from `Object.prototype` but it does have most of the functions from it. `toString`, `valueOf`, `isPrototypeOf`, `hasOwnProperty`, as well as `constructor` pointing to `Environment`.

It's possible to add methods to `Environment.prototype` and have them work on instances correctly. It's possible to change an instance's `__proto__` and point it to something else and also have that work correctly (though Object.getPrototypeOf may not reflect the change).

```javascript
// normal creation
var env = require('environment').create();
```
```javascript
// or modifying the prototype
var Environment = require('environment');
Environment.prototype.paths = function paths(){
	return this.PATH.split(process.platform === 'win32' ? ';' : ':');
}

var env = new Environment;
console.log(env.paths());
```
