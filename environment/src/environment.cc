// This is node.cc from node 0.8.0 with small modifications:
// * change function signatures from use as named property interceptor to normal functions
// * "property" and "value" replaced with args[0]/args[1]
// * in Windows, expose keys starting with '=' in EnvEnumerator
// * return undefined from EnvGetter instead of the recently changed prototype recursing lookup

#include <v8.h>
#include <node.h>

using namespace v8;
using namespace node;


#define EXPORT_FUNCTION(n, m) \
  Local<String> name_##n = String::NewSymbol(#n); \
  Local<Function> fn_##n = FunctionTemplate::New(m)->GetFunction(); \
  fn_##n->SetName(name_##n); \
  target->Set(name_##n, fn_##n);

#define ARRAY_SIZE(a) \
  ((sizeof(a) / sizeof(*(a))) / static_cast<size_t>(!(sizeof(a) % sizeof(*(a)))))



static Handle<Value> EnvGetter(const Arguments& args){
  HandleScope scope;

#ifdef __POSIX__
  String::Utf8Value key(args[0]);
  const char* val = getenv(*key);
  if (val) {
    return scope.Close(String::New(val));
  }
#else  // _WIN32
  String::Value key(args[0]);
  WCHAR buffer[32767]; // The maximum size allowed for environment variables.
  DWORD result = GetEnvironmentVariableW(reinterpret_cast<WCHAR*>(*key),
                                         buffer,
                                         ARRAY_SIZE(buffer));
  // If result >= sizeof buffer the buffer was too small. That should never
  // happen. If result == 0 and result != ERROR_SUCCESS the variable was not
  // not found.
  if ((result > 0 || GetLastError() == ERROR_SUCCESS) &&
      result < ARRAY_SIZE(buffer)) {
    return scope.Close(String::New(reinterpret_cast<uint16_t*>(buffer), result));
  }
#endif
  // Not found
  return Undefined();
}

static Handle<Value> EnvSetter(const Arguments& args) {
  HandleScope scope;

#ifdef __POSIX__
  String::Utf8Value key(args[0]);
  String::Utf8Value val(args[1]);
  setenv(*key, *val, 1);
#else  // _WIN32
  String::Value key(args[0]);
  String::Value val(args[1]);
  WCHAR* key_ptr = reinterpret_cast<WCHAR*>(*key);
  // Environment variables that start with '=' are read-only.
  if (key_ptr[0] != L'=') {
    SetEnvironmentVariableW(key_ptr, reinterpret_cast<WCHAR*>(*val));
  }
#endif
  // Whether it worked or not, always return rval.
  return scope.Close(args[1]->ToString());
}


static Handle<Value> EnvDeleter(const Arguments& args) {
  HandleScope scope;

#ifdef __POSIX__
  String::Utf8Value key(args[0]);
  if (!getenv(*key)) return False();
  unsetenv(*key); // can't check return value, it's void on some platforms
  return True();
#else
  String::Value key(args[0]);
  WCHAR* key_ptr = reinterpret_cast<WCHAR*>(*key);
  if (key_ptr[0] == L'=' || !SetEnvironmentVariableW(key_ptr, NULL)) {
    // Deletion failed. Return true if the key wasn't there in the first place,
    // false if it is still there.
    bool rv = GetEnvironmentVariableW(key_ptr, NULL, NULL) == 0 &&
              GetLastError() != ERROR_SUCCESS;
    return scope.Close(Boolean::New(rv));
  }
  return True();
#endif
}


static Handle<Value> EnvEnumerator(const Arguments& args) {
  HandleScope scope;

#ifdef __POSIX__
  int size = 0;
  while (environ[size]) size++;

  Local<Array> env = Array::New(size);

  for (int i = 0; i < size; ++i) {
    const char* var = environ[i];
    const char* s = strchr(var, '=');
    const int length = s ? s - var : strlen(var);
    env->Set(i, String::New(var, length));
  }
#else  // _WIN32
  WCHAR* environment = GetEnvironmentStringsW();
  if (environment == NULL) {
    // This should not happen.
    return scope.Close(Handle<Array>());
  }
  Local<Array> env = Array::New();
  WCHAR* p = environment;
  int i = 0;
  while (*p != NULL) {
    WCHAR *s;
    if (*p == L'=') {
      s = wcschr(p + 1, L'=');
    } else {
      s = wcschr(p, L'=');
    }
    if (!s) {
      s = p + wcslen(p);
    }
    env->Set(i++, String::New(reinterpret_cast<uint16_t*>(p), s - p));
    p = s + wcslen(s) + 1;
  }
  FreeEnvironmentStringsW(environment);
#endif
  return scope.Close(env);
}

static void init(Handle<Object> target) {
  EXPORT_FUNCTION(get, EnvGetter)
  EXPORT_FUNCTION(set, EnvSetter)
  EXPORT_FUNCTION(delete, EnvDeleter)
  EXPORT_FUNCTION(enumerate, EnvEnumerator)
}
NODE_MODULE(environment, init);
