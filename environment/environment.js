var bindings = require('./bindings');

bindings.toObject = function toObject(){
  var out = {};
  bindings.enumerate().forEach(function(key){
    out[key] = bindings.get(key);
  });
  return out;
}


if (typeof Proxy === 'undefined' || typeof Proxy.create !== 'function') {
  return module.exports = bindings;
}




if (process.platform === 'win32') {
  var enumerator = function enumerate(){
    return bindings.enumerate().filter(function(key){
      return key[0] !== '=';
    });
  };

  var setter = function set(key, value){
    return key[0] === '=' ? false : bindings.set(key, value);
  };

  var query = function query(key, value){
    var hidden = key[0] === '=';
    return { configurable: true,
             enumerable: !hidden,
             writable: !hidden,
             value: value };
  };
} else {
  var enumerator = bindings.enumerate;

  var setter = bindings.set

  var query = function query(key, value){
    return { configurable: true,
             enumerable: true,
             writable: true,
             value: value };
  };
}


function EnvHandler(proto){
  this.proto = proto;
}

EnvHandler.prototype = {
  delete: bindings.delete,
  getOwnPropertyNames: bindings.enumerate,
  keys: enumerator,
  enumerate: function enumerateTrap(){
    var keys = enumerator();
    for (var k in this.proto)
      keys.push(k);
    return keys;
  },
  has: function hasTrap(key){
    return bindings.get(key) === undefined ? key in this.proto : true;
  },
  hasOwn: function hasOwnTrap(key){
    return bindings.get(key) !== undefined;
  },
  get: function getTrap(receiver, key){
    if (key === '__proto__')
      return this.proto;

    var ret = bindings.get(key);
    return ret === undefined ? this.proto[key] : ret;
  },
  set: function setTrap(receiver, key, value){
    if (key === '__proto__') {
      if (receiver === value || Object.prototype.isPrototypeOf.call(receiver, value))
        throw new Error('Cyclic prototype chain');
      else
        return this.proto = value;
    } else
      return setter(key, value);
  },
  defineProperty: function definePropertyTrap(key, desc){
    return desc.set || desc.get ? false : setter(key, desc.value);
  },
  getOwnPropertyDescriptor: function getOwnPropertyDescriptorTrap(key){
    var value = bindings.get(key);
    if (value !== undefined)
      return query(key, value);
  }
};



function Environment(){
  return Proxy.create(new EnvHandler(Environment.prototype), Environment.prototype);
}


Environment.prototype = Object.create(null, {
  valueOf: _(Object.prototype.valueOf),
  toString: _(function toString(){ return '[object Environment]' }),
  constructor: _(Environment),
  isPrototypeOf: _(Object.prototype.isPrototypeOf),
  hasOwnProperty: _(Object.prototype.hasOwnProperty)
});


function _(v){
  return { configurable: true,
           enumerable: false,
           writable: true,
           value: v };
}


Environment.get = bindings.get;
Environment.set = bindings.set;
Environment.delete = bindings.delete;
Environment.enumerate = bindings.enumerate;
Environment.toObject = bindings.toObject;
Environment.create = function create(){
  return new Environment;
};

module.exports = Environment;