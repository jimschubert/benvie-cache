var path = require('path'),
    fs = require('fs');
fs.existsSync || (fs.existsSync = path.existsSync);

function absolutelyRelative(filepath){
  return path.resolve(__dirname, filepath);
}

function requireBindings(name){
  var selfCompiled =  absolutelyRelative('./build/Release/'+name+'.node');

  if (fs.existsSync(selfCompiled))
    return require(selfCompiled);

  var preCompiled = absolutelyRelative([
    'precompiled',
    process.version.match(/v(\d+\.\d+)/)[1],
    process.platform,
    process.arch,
    name + '.node'
  ].join('/'));

  if (fs.existsSync(preCompiled))
    return require(preCompiled);

  throw new Error('Could not find binaries for ' + name);
}


module.exports = requireBindings('environment');