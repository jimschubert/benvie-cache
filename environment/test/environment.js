var assert = require('assert');
var env = require('environment');

function equal(a, b, message){
	assert.equal(a, b, message);
	console.log('ok: '+message);
}

if (process.platform === 'win32') {
	var enumerate = env.enumerate;
	env.enumerate = function(){
		return enumerate().filter(function(s){ return s[0] !== '=' });
	};
}

var keys = env.enumerate();

var firstValue = env.get(keys[0]);
assert(env.delete(keys[0]));
equal(process.env[keys[0]], undefined, 'deleting key works');
env.set(keys[0], firstValue);
equal(process.env[keys[0]], firstValue, 'adding deleted value back again works');

equal(keys+'', Object.keys(process.env)+'', 'env.enumerate() has same keys as process.env');

keys.forEach(function(key){
	assert.equal(env.get(key), process.env[key]);
});
console.log('ok: all values match process.env');


if (typeof Proxy !== 'undefined') {
	var Environment = env;
	equal(typeof Environment, 'function', 'Environment is a function when Proxy is available');
	assert(env = new Environment);
	equal(env+'', '[object Environment]', 'shows "Environment" brand')
	equal(Object.keys(env)+'', Object.keys(process.env)+'', 'Object.keys(env) is same as Object.keys(process.env)');
	Object.keys(env).forEach(function(key){
		assert.equal(env[key], process.env[key]);
	});
	console.log('ok: all values in env match process.env');

	var i = 0, keys = [];
	for (keys[i++] in env);
	equal(keys+'', Object.keys(process.env)+'', '`for (key in env)` has same keys Object.keys(process.env)');

	Environment.prototype.keyOnProto = "hello";
	equal(env.keyOnProto, 'hello', 'env.keyOnProto === "hello"');
	equal('keyOnProto' in env, true, '"keyOnProto" in env === true');

	function paths(){
		return this.PATH.split(process.platform === 'win32' ? ';' : ':');
	};

	Environment.prototype.paths = paths;
	equal(env.paths()+'', paths.call(process.env)+'', 'prototype method works correctly');
}

// ok, fail
// equal, notEqual
// deepEqual, notDeepEqual
// strictEqual, notStrictEqual
// throws, doesNotThrow