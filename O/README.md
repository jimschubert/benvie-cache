# O
Object Renaissance. This module has been reworked and has no readme. It's an awesome module though

# install
For Node.js
```javascript
npm install O
```

For browsers just include it.

# Use

```javascript
// in node
require('O').O()
// browser
O()
```

Or specify which global object:

```javascript
// node
var context = O(vm.createContext());
// browser
var context = O(document.createElement('iframe'));
```


### Function
* __Function.stringify(f)__
* __Function.create(name, call, construct, proto, descriptors)__
* __Function.isFunction(o)__
* __Function.isArguments(o)__
* __Function.invoker(name)__
* __Function.compose(f1, f2, ...)__
* __Function.collate(o, context)__
* __Function.dobind(f)__
* __Function.doapply(f)__
* __Function.docall(f)__

### Function.prototype
* __Function.prototype.bindbind()__
* __Function.prototype.callbind()__
* __Function.prototype.applybind()__
* __Function.prototype.partial()__
* __Function.prototype.inherit(Super, properties)__
* __Function.prototype.collate(context)__

### Object
* __Object.stringify(o)__
* __Object.clone(o)__
* __Object.lower(o)__
* __Object.isObject(o)__
* __Object.isIndexed(o)__
* __Object.decorate(o)__
* __Object.enumerate(o)__
* __Object.get(o, key)__
* __Object.set(o, key, value)__
* __Object.unset(o, key)__
* __Object.has(o, key)__
* __Object.hasOwn(o, key)__
* __Object.hide(o, key, value)__
* __Object.show(o, key)__
* __Object.is(o, p)__
* __Object.getPrototypesOf(o)__
* __Object.getPropertyNames(o)__
* __Object.getPropertyDescriptor(o, key)__
* __Object.getPropertyDescriptors(o)__
* __Object.getOwnPropertyDescriptors(o)__
* __Object.defineLazyProperty(o, key, getter, readonly)__
* __Object.setPrototypeOf(o, proto)__
* __Object.injectPrototype(o, proto)__

### Object.prototype
* __Object.prototype.forEach(callback, context)__
* __Object.prototype.map(callback, context)__
* __Object.prototype.decorate(...)__

### Array
* __Array.stringify(o)__
* __Array.create(proto, descriptors)__
* __Array.from(o)__
* __Array.merge()__


### Array.prototype
* __Array.prototype.unique()__
* __Array.prototype.first(callback, context)__
* __Array.prototype.pushAll()__
* __Array.prototype.flatten(deep)__
* __Array.prototype.chunk(size)__
* __Array.prototype.forEachF(callback, context)__
* __Array.prototype.mapF(callback, context)__


### Date
* __Date.stringify(o)__
* __Date.isDate(o)__
* __Date.create(args, proto, descriptors)__

### String
* __String.isString(o)__
* __String.uid()__
* __String.unique(array, ignoreCase)__

### String.prototype
* __String.prototype.repeat(count)__
* __String.prototype.startsWith(substring, fromIndex)__
* __String.prototype.endsWith(substring, fromIndex)__
* __String.prototype.contains(substring, fromIndex)__
* __String.prototype.reverse()__
* __String.prototype.slug()__
* __String.prototype.recase(n)__
* __String.prototype.toIdentifier()__
* __String.prototype.camel(s)__
* __String.prototype.dash(s)__
* __String.prototype.toBuffer()__

### Number
* __Number.isNumber(o)__
* __Number.isInt(n)__
* __Number.isNaN(n)__
* __Number.fromPercent(v)__
* __Number.toUint32(n)__
* __Number.toInt32(n)__
* __Number.toInt(n)__
* __Number.toFinite(n)__
* __Number.type(n)__
* __Number.unique(array)__

### Number.prototype
* __Number.prototype.toPercent()__
* __Number.prototype.isInt()__
* __Number.prototype.clamp(min, max)__
* __Number.prototype.min(n)__
* __Number.prototype.max(n)__
* __Number.prototype.pow(n)__
* __Number.prototype.avg(n)__
* __Number.prototype.sqrt()__
* __Number.prototype.cubert()__
* __Number.prototype.sq()__
* __Number.prototype.cube()__

### RegExp
* __RegExp.stringify(o)__
* __RegExp.toSource(o)__
* __RegExp.isRegExp(o)__
* __RegExp.create(pattern, flags, proto, descriptors)__
* __RegExp.escape(s) __
* __RegExp.concat(flags, regexes)__

### RegExp.prototype
* __RegExp.prototype.globalize()__
* __RegExp.prototype.flags()__
* __RegExp.prototype.match(str)__
* __RegExp.prototype.filter(array)__

### Map
* __Map.createStorage(creator)__

### WeakMap
* __WeakMap.createStorage(creator)__
