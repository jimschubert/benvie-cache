void function(global, exports, contexts){

exports.O = function O(ctx){
  ctx || (ctx = new O.constructor('return this')());
  if (~contexts.indexOf(ctx)) return;
  contexts.push(ctx);

  if (typeof HTMLIFrameElement !== 'undefined') {
    if (ctx instanceof HTMLIFrameElement) {
      if (ctx.parentNode === null) {
        document.body.appendChild(ctx);
        ctx.contentWindow.close();
      }
      ctx = ctx.contentWindow;
    }
  } else if (typeof require === 'function') {
    var vm = require('vm');
    var Context = vm.createContext().constructor;
    if (ctx instanceof Context) {
      vm.runInContext('this', ctx);
    }
  }

  var Array        = ctx.Array,
      Boolean      = ctx.Boolean,
      Date         = ctx.Date,
      Function     = ctx.Function,
      Map          = ctx.Map,
      Number       = ctx.Number,
      Object       = ctx.Object,
      RegExp       = ctx.RegExp,
      Set          = ctx.Set,
      String       = ctx.String,
      WeakMap      = ctx.WeakMap,
      Math         = ctx.Math,
      Int8Array    = ctx.Int8Array,
      Int16Array   = ctx.Int16Array,
      Int32Array   = ctx.Int32Array,
      Uint8Array   = ctx.Uint8Array,
      Uint16Array  = ctx.Uint16Array,
      Uint32Array  = ctx.Uint32Array,
      Float32Array = ctx.Float32Array,
      Float64Array = ctx.Float64Array;

  var FP = Function.prototype,
      OP = Object.prototype,
      AP = Array.prototype,
      DP = Date.prototype,
      RP = RegExp.prototype;

  var _bind   = FP.bind,
      _call   = FP.call,
      _apply  = FP.apply,
      _hasOwn = OP.hasOwnProperty;

  var defineProperty   = Object.defineProperty,
      ownKeys          = Object.keys,
      ownNames         = Object.getOwnPropertyNames,
      getOwnDescriptor = Object.getOwnPropertyDescriptor,
      getPrototype     = Object.getPrototypeOf,
      isArray          = Array.isArray,
      _slice           = AP.slice

  var hiddenDesc = { enumerable: false },
      visibleDesc = { enumerable: true };

  var stringifiers = {
    Function: FP.toString,
    Object  : OP.toString,
    Array   : AP.toString,
    Date    : DP.toString,
    RegExp  : RP.toString
  };

var mutableProto = function(){
  var x = Object.create(null);
  x.__proto__ = Object;
  return 'prototype' in x;
}();


// #################################
// ### function name shim for IE ###
// #################################

if (!('name' in FP)) {
  void function(){
    defineOwn(FP, 'name', {
      configurable: true,
      get: function(){
        if (this === FP)
          return 'Empty';
        var src = stringifiers.Function.call(this);
        src = src.slice(9, src.indexOf('('));
        defineOwn(this, 'name', { value: src });
        return src;
      }
    });
  }();
}




  // #############################
  // ### Fundamental Utilities ###
  // #############################

  function Definable(desc){
    for (var k in desc)
      this[k] = desc[k];
    this.value = null;
  }

  Definable.prototype.define = function define(o, key, v){
    this.value = v;
    defineProperty(o, key, this);
    this.value = null;
  }

  var normalValue = Definable.normal = new Definable({ enumerable: true, configurable: true, writable: true });
  var hiddenValue = Definable.hidden = new Definable({ enumerable: false, configurable: true, writable: true });
  var readonlyValue = Definable.readonly = new Definable({ enumerable: true, configurable: true, writable: false });

  function isObject(o){
    return o != null && typeof o === 'object' || typeof o === 'function';
  }

  function isPrimitive(o){
    return o == null || typeof o !== 'object' && typeof o !== 'function';
  }

  function isIndexed(o){
    return isArray(o) || isObject(o) && 'length' in o && (o.length - 1) in o;
  }

  function isArguments(o){
    return o && stringifiers.Object.call(o) === '[object Arguments]';
  }

  function slice(a,o,p){
    switch (a.length) {
      case 0: return [];
      case 1: return o ? [] : [a[0]];
      case 2: a = [a[0],a[1]];
    }
    return (o || p) ? _slice.call(a,o,p) : a;
  }

  function decorate(o){
    var i = 1, a = arguments, b, c, d;
    if (isArguments(o)) {
      i = 0;
      a = o;
      o = this;
    }
    var hidden = a[i] === true,
        overwrite = a[i+1] !== false;

    for (; i < a.length; i++) {
      if (isArray(b = a[i])) {
        for (c = 0; c < b.length; c++) {
          if (typeof b[c] === 'function' && (d = b[c].name)) {
            if (hidden || d === 'toString') {
              hiddenValue.define(o, d, b[c]);
            } else {
              o[d] = b[c];
            }
          }
        }
      } else if (isObject(b)) {
        for (c in b) {
          try {
            if (d = getOwnDescriptor(b, c)) {
              if (d.get || d.set) {
                defineProperty(o, c, d);
              } else {
                o[c] = d.value;
                if (hidden || c === 'toString' || c === 'constructor') {
                  defineProperty(o, c, hiddenDesc);
                }
              }
            }
          } catch (e) {}
        }
      }
    }
    return o;
  }

  function embellish(o, p){
    decorate(o, true, false, p);
  }


var initialize = mutableProto
  ? function(target, proto, descriptors){
    if (isObject(proto) || proto === null)
      Object.setPrototypeOf(target, proto);
    if (isObject(descriptors))
      Object.defineProperties(target, descriptors);
    return target;
  }
  : function(target, proto, descriptors){
      if (isObject(proto) || proto === null)
        Introspect.describe(proto).applyTo(target);
      if (isObject(descriptors))
        Object.defineProperties(target, descriptors);
      return target;
    };


  // #################################
  // ### Function Static Functions ###
  // #################################


  embellish(Function, [
    function stringify(f){
      return stringifiers.Function.call(f);
    },
    function create(name, call, construct, proto, descriptors){
      construct || (construct = function Constructor(){
        var instance = Object.create(isObject(func.prototype) ? func.prototype : OP);
        var result = _apply.call(call, instance, arguments);
        return isObject(result) ? result : instance;
      });

      var func = new Function('call', 'construct',
        'return function '+name+'(){\n'+
        '  return (this instanceof '+name+' ? construct : call).apply(this, arguments);\n'+
        '}')(call, construct);

      return initialize(func, proto, descriptors);
    },
    function isFunction(o){
      return typeof o === 'function';
    },
    function isArguments(o){
      return o && Object.stringify(o) === '[object Arguments]';
    },
    function invoker(name){
      var args = arguments.length > 1 ? Array.slice(arguments, 1) : new Array;
      return function(o){
        return _apply.call(o[name], o, args.concat(Array.slice(arguments, 1)));
      };
    },
    function compose(){
      var functions = slice(arguments),
          count = functions.length;

      return function(){
        var i = count - 1;
        var result = functions[i].apply(this, arguments);
        while (i--)
          result = functions[i](result);
        return result;
      };
    },
    function collate(o, context){
      var args = Array.merge.apply(null, Array.slice(arguments, 1));
      return _apply.call(o, context, args);
    },
    function dobind(){
      return _call.apply(_bind, arguments);
    },
    function doapply(){
      return _call.apply(_apply, arguments);
    },
    function docall(){
      return _call.apply(_call, arguments);
    },
  ]);


  // ##############################
  // ### Function Class Methods ###
  // ##############################

  embellish(Function.prototype, [
    function bindbind(){
      var self = this;
      return function(){
        return _bind.apply(self, arguments);
      }
    },
    function callbind(){
      var self = this;
      return function(){
        return _call.apply(self, arguments);
      }
    },
    function applybind(){
      var self = this;
      if (arguments.length === 1) {
        var receiver = arguments[0];
        return function(){
          return _apply.call(self, receiver, arguments[0]);
        }
      } else {
        return function(){
          if (arguments.length === 2)
            return _apply.call(self, arguments[0], arguments[1]);
          else
            return _apply.call(self, this, arguments[0]);
        }
      }
    },
    function partial(){
      var self = this;
      var args = slice(arguments);
      return function(){
        return _apply.call(self, this, Array.concat(args, arguments));
      }
    },
    function inherit(Super, properties){
      this.prototype = decorate(Object.create(Super.prototype), true, {
        constructor: this,
        super: Super.prototype
      });
      this.super = Super;
      if (properties)
        decorate(this.prototype, true, properties);
      return this;
    },
    function collate(context){
      var args = Array.merge.apply(null, Array.slice(arguments, 1));
      return _apply.call(this, context, args);
    }
  ]);






  // ###############################
  // ### Object Static Functions ###
  // ###############################

  embellish(Object, [
    function stringify(o){
      return stringifiers.Object.call(o);
    },
    function clone(o){
      return Object.create(getPrototype(o = Object(o)), Object.getOwnPropertyDescriptors(o));
    },
    function lower(o){
      return Object.create(getPrototype(o = Object(o)), Object.getPropertyDescriptors(o));
    },
    decorate,
    isObject,
    isIndexed,
    function enumerate(o){
      var k = 0, props = [];
      for (props[k++] in o);
      return props;
    },
    function get(o, key){
      return o[key];
    },
    function set(o, key, value){
      return o[key] = value;
    },
    function unset(o, key){
      return delete o[key];
    },
    function has(o, key){
      return key in o;
    },
    function hasOwn(o, key){
      return OP.hasOwnProperty.call(o, key);
    },
    function hide(o, key, value){
      if (arguments.length === 3)
        hiddenValue.define(o, key, value);
      else if (typeof key === 'string')
        defineProperty(o, key, hiddenDesc);
      else if (isArray(key))
        key.forEach(function(key){
          if (Object.hasOwn(o, key))
            defineProperty(o, key, hiddenDesc);
        });
      else
        ownKeys(o).forEach(function(key){
          defineProperty(o, key, hiddenDesc);
        });

      return o;
    },
    function show(o, key){
      if (typeof key === 'string')
        defineProperty(o, key, visibleDesc);
      else if (isArray(key))
        key.forEach(function(key){
          if (Object.hasOwn(o, key))
            defineProperty(o, key, visibleDesc);
        });
      else
        ownKeys(o).forEach(function(key){
          defineProperty(o, key, visibleDesc);
        });

      return o;
    },
    function is(o, p){
      return o === p || (o === 0 && p === 0 && 1 / o === 1 / p) || (o !== o && p !== p);
    },
    function getPrototypesOf(o){
      var protos = [];
      while (o = getPrototype(o))
        protos.push(o);
      return protos;
    },
    function getPropertyNames(o){
      var props = [];
      do {
        props.pushAll(ownNames(o));
      } while (o = getPrototype(o))
      return props.unique();
    },
    function getPropertyDescriptor(o, key){
      var desc;
      while (o) {
        try {
          if (desc = getOwnDescriptor(o, key))
            return desc;
          o = getPrototype(o);
        } catch (e) {
          return { value: o[key],
                   writable: true,
                   enumerable: true,
                   configurable: true };
        }
      }
      return desc;
    },
    function getPropertyDescriptors(o){
      var seen = Object.create(null);
      var descs = {};
      while (o) {
        ownNames(o).forEach(function(key){
          if (!(key in seen)) {
            descs[key] = getOwnDescriptor(o, key);
            seen[key] = true;
          }
        });
        o = getPrototype(o);
      }
      return descs;
    },
    function getOwnPropertyDescriptors(o){
      var out = {};
      ownNames(o).forEach(function(key){
        out[key] = getOwnDescriptor(o, key);
      });
      return out;
    },
    function defineLazyProperty(o, key, getter, readonly){
      if (!getter && typeof key === 'function') {
        getter = key;
        key = getter.name;
      }
      defineProperty(o, key, {
        configurable: true,
        enumerable: true,
        get: function get(){
          var v = getter.call(this, key);
          normalValue.define(this, key, v);
          return v;
        },
        set: function set(v){
          if (readonly)
            v = getter.call(this, key);
          normalValue.define(this, key, v);
        }
      });
  }
]);

if (mutableProto)
  embellish(Object, [
    function setPrototypeOf(o, proto){
      o = Object(o);
      o.__proto__ = proto;
      return o;
    },
    function injectPrototype(o, proto){
      var origProto = getPrototype(o);
      if (proto)
        Object.setPrototypeOf(proto, origProto);
      else
        proto = Object.create(origProto);
      Object.setPrototypeOf(o, proto);
      return proto;
    }
  ]);



  // ############################
  // ### Object Class Methods ###
  // ############################

  embellish(Object.prototype, [
    function forEach(callback, context){
      var keys = ownKeys(Object(this));
      context = context || this;
      for (var i=0; i < keys.length; i++)
        callback.call(context, this[keys[i]], keys[i], this);
      return this;
    },
    function map(callback, context){
      var keys = ownKeys(Object(this));
      var result = [];
      context = context || this;
      for (var i=0; i < keys.length; i++)
        result.push(callback.call(context, this[keys[i]], keys[i], this));
      return result;
    },
    function decorate(){
      Object.decorate.call(this, arguments);
      return this;
    }
  ]);





  // ##############################
  // ### Array Static Functions ###
  // ##############################

  embellish(Array, [
    function stringify(o){
      return stringifiers.Array.call(o);
    },
    function create(proto, descriptors){
      return initialize(Array.slice(arguments, 2), proto, descriptors);
    },
    function from(o){
      return slice(o);
    },
    function merge(){
      var out = new Array;
      for (var k in arguments)
        out.pushAll(arguments[k]);

      return out;
    }
  ]);


  // ###########################
  // ### Array Class Methods ###
  // ###########################

  embellish(Array.prototype, [
    function unique(){
      var result = new Array,
          seen = new Set,
          i = this.length;

      while (i--)
        if (!seen.has(this[i]))
          seen.add(result[result.length] = this[i]);

      return result;
    },
    function first(callback, context){
      var ret;
      context = context || this;
      if (callback instanceof RegExp) {
        callback = function(s){ return }
      }
      for (var i=0; i < this.length; i++) {
        if (ret = callback.call(context, this[i], i, this))
          return ret === true ? this[i] : ret;
      }
    },
    function pushAll(){
      var item;
      for (var k in arguments) {
        item = arguments[k];
        if (isIndexed(item))
          AP.push.apply(this, item);
      }
    },
    function flatten(deep){
      var out = Array.isArray(deep) ? deep : new Array;
      for (var i=0; i < this.length; i++) {
        if (Array.isArray(this[i])) {
          if (deep)
            this[i].flatten(out);
          else
            out.pushAll(this[i]);
        } else if (isIndexed(this[i]))
          out.pushAll(this[i]);
        else
          out.push(this[i]);
      }
      return out;
    },
    function chunk(size){
      var out = [];
      var chunks = this.length / size;
      if (chunks | 0 !== chunks)
        chunks = chunks | 0 + 1;

      while (chunks--)
        out.push(this.slice(chunks * size, size * (chunks + 1)));

      return out;
    },
    function forEachF(callback, context){
      context = context || this;
      for (var i=0; i < this.length; i++)
        if (typeof this[i] === 'function')
          callback.call(context, this[i], this[i].name, i, this);

      return this;
    },
    function mapF(callback, context){
      var out = [];
      context = context || this;
      for (var i=0; i < this.length; i++)
        if (typeof this[i] === 'function')
          out.push(callback.call(context, this[i], this[i].name, i, this));

      return out;
    }
  ]);

  ownNames(Array.prototype).forEach(function(key){
    if (typeof Array.prototype[key] === 'function' && key === 'forEach' || key === 'map' || !(key in Array))
      hiddenValue.define(Array, key, _call.bind(Array.prototype[key]));
  });





  // #############################
  // ### Date Static Functions ###
  // #############################

  embellish(Date, [
    function stringify(o){
      return stringifiers.Date.call(o);
    },
    function isDate(o){
      return o instanceof Date || Object.stringify(o) === '[object Date]';
    },
    function create(args, proto, descriptors){
      var date;

      if (Array.isArray(args))
        date = new (_bind.apply(Date, Array.concat([null], args)));

      else if (typeof args === 'number')
        date = new Date(args);

      else if (typeof args === 'string')
        date = new Date(Date.parse(args));

      return initialize(date, proto, descriptors);
    }
  ]);


  // ##########################
  // ### Date Class Methods ###
  // ##########################

  embellish(Date.prototype, [
    function toShortDate(){
      return this.toJSON().slice(0, 10);
    }
  ]);






  // ###############################
  // ### String Static Functions ###
  // ###############################

  embellish(String, [
    function isString(o){
      return typeof o === 'string' || o instanceof String;
    },
    function uid(){
      return '_'+(Date.now() * Math.random() / 1.1).toString(36).replace('.', '');
    },
    function unique(array, ignoreCase){
      var seen = Object.create(null);
      var out = new Array, item;
      for (var i=0; i < array.length; i++) {
        item = ignoreCase ? array[i].toLowerCase() : array[i];
        if (!(item in seen)) {
          seen[item] = array[i];
          out.push(array[i]);
        }
      }
      return out;
    }
  ]);



  // ############################
  // ### String Class Methods ###
  // ############################

  embellish(String.prototype, [
    function repeat(count){
      return new Array(++count > 2 ? count : 2).join(this + '');
    },
    function startsWith(substring, fromIndex){
      return this.lastIndexOf(substring, fromIndex) === (fromIndex || 0);
    },
    function endsWith(substring, fromIndex){
      return this.substring(-(substring+='').length - (fromIndex || 0), fromIndex) === substring;
    },
    function contains(substring, fromIndex){
      return this.indexOf(substring, fromIndex) > -1;
    },
    function reverse(){
      return this.split('').reverse().join('');
    },
    function slug(){
      return this.replace(/[^\w]/g, '-').replace(/--+/g, '-');
    },
    function recase(n){
      n = n || 1;
      return this.slice(0, n).toUpperCase() + this.slice(n).toLowerCase();
    },
    function toIdentifier(){
      return this.replace(/^[^a-zA-Z_$]|[^\w_$]/g, '_').replace(/__+/g, '_');
    },
    function camel(s){
      return s.replace(/-([a-z])/g, function(a,b){ return b.toUpperCase() });
    },
    function dash(s){
      return s.replace(/([A-Z])/g, function(a){ return '-'+a.toLowerCase() });
    },
  ]);



  // ###############################
  // ### Number Static Functions ###
  // ###############################

if (global.Buffer && typeof global.Buffer.prototype.inspect === 'function' && ctx.ArrayBuffer) {
  ArrayBuffer.prototype.inspect = global.Buffer.prototype.inspect;
  Int8Array.prototype.__proto__ = ArrayBuffer.prototype;
  Int16Array.prototype.__proto__ = ArrayBuffer.prototype;
  Int32Array.prototype.__proto__ = ArrayBuffer.prototype;
  Uint8Array.prototype.__proto__ = ArrayBuffer.prototype;
  Uint16Array.prototype.__proto__ = ArrayBuffer.prototype;
  Uint32Array.prototype.__proto__ = ArrayBuffer.prototype;
  Float32Array.prototype.__proto__ = ArrayBuffer.prototype;
  Float64Array.prototype.__proto__ = ArrayBuffer.prototype;
}

  void function(){
    if (typeof Uint8Array !== 'undefined') {
      embellish(String.prototype, [
        function toBuffer(){
          var i = this.length;
          var buff = new Uint8Array(i);
          while (i--)
            buff[i] = this.charCodeAt(i);
          return buff;
        }
      ]);
      var Int8 = Int8Array,
          Int16 = Int16Array,
          Int32 = Int32Array,
          Uint8 = Uint8Array,
          Uint16 = Uint16Array,
          Uint32 = Uint32Array,
          Float32 = Float32Array,
          Float64 = Float64Array;
    } else {
      var Int8 = 'Int8',
          Int16 = 'Int16',
          Int32 = 'Int32',
          Uint8 = 'Uint8',
          Uint16 = 'Uint16',
          Uint32 = 'Uint32',
          Float32 = 'Float32',
          Float64 = 'Float64';
    }

  var Int64 = 'Int64',
      Uint64 = 'Uint64';

    decorate(Number, { tokens: {} });

    function token(name, number){
      Number.tokens[name] = (new Number(number)).decorate(true, true, {
        isToken: true
      });
      Number.tokens[name].name = name;
      return Number.tokens[name];
    }


    var NOT_A_NUMBER = token('NOT_A_NUMBER', NaN),
        NEGATIVE_ZERO = token('NEGATIVE_ZERO', -0),
        POSITIVE_ZERO = token('POSITIVE_ZERO', 0),
        NEGATIVE_INFINITY = token('NEGATIVE_INFINITY', -Infinity),
        POSITIVE_INFINITY = token('POSITIVE_INFINITY', Infinity);

    embellish(Number, [
      function isNumber(o){
        return o === o && typeof o === 'number' || o instanceof Number;
      },
      function isInt(n){
        if (typeof n === 'number')
          return n | 0 === n;
        else if (isArray(b))
          return n.every(isInt);
      },
      function isNaN(n){
        return typeof value !== 'number' && global.isNaN(value);
      },
      function fromPercent(v){
        if (typeof v === 'string' && v[v.length - 1] === '%')
          return v.slice(0, -1) / 100;
        if (v >= 0 && v <= 1)
          return +v;
        if (v > 1 && v <= 100)
          return v / 100;
        return 0;
      },
      function toUint32(n){
        return n >>> 0;
      },
      function toInt32(n){
        return n >> 0;
      },
      function toInt(n){
        return n ? Number.toFinite(n) + .5 | 0 : 0;
      },
      function toFinite(n){
        return typeof n === 'number' ? n || 0 : isFinite(n /= 1) ? n : 0;
      },
      function type(n){
        if (n === null || n === undefined)
          return NOT_A_NUMBER;

        if (n instanceof Number || typeof n === 'string' || n instanceof String)
          n /= 1;

        if (typeof n !== 'number' || n !== n)
          return NOT_A_NUMBER

        switch (n) {
          case 0: return 1 / n === -Infinity ? NEGATIVE_ZERO : POSITIVE_ZERO;
          case NaN: return NOT_A_NUMBER;
          case Infinity: return POSITIVE_INFINITY;
          case -Infinity: return NEGATIVE_INFINITY;
        }

        if (n < 0) {
          if (n | 0 === n) {
            if (n >= -0x80)               return Int8;
            if (n >= -0x8000)             return Int16;
            if (n >= -0x80000000)         return Int32;
            if (n >= -0x8000000000000000) return Int64;
          } else {
            if (n >= -0x800000)           return Float32;
            if (n >= -0xfffffffffffff)    return Float64;
          }
          return NEGATIVE_INFINITY;
        } else if (n > 0) {
          if (n | 0 === n) {
            if (n <= 0x7f)                return Int8;
            if (n <= 0xff)                return Uint8;
            if (n <= 0x7fff)              return Int16;
            if (n <= 0xffff)              return Uint16;
            if (n <= 0x7fffffff)          return Int32;
            if (n <= 0xffffffff)          return Uint32;
            if (n <= 0x7fffffffffffffff)  return Int64;
            if (n <= 0xffffffffffffffff)  return Uint64;
          } else {
            if (n <= 0x7fffff)            return Float32;
            if (n <= 0x10000000000000)    return Float64;
          }
          return POSITIVE_INFINITY;
        }
      },
      function unique(array){
        var seen = Object.create(null);
        var out = [];
        for (var i=0; i < array.length; i++) {
          if (Number.isNumber(array[i]) && !(array[i] in seen)) {
            seen[array[i]] = array[i];
            out.push(array[i]);
          }
        }
        return out;
      }
    ]);
  }();

  Math.decorate([
    function clamp(min, max, v){
      return v < min ? min : v > max ? max : v;
    },
    function cubert(v){
      return Math.pow(v, 1/3);
    }
  ]);


  // ############################
  // ### Number Class Methods ###
  // ############################

  embellish(Number.prototype, [
    function toPercent(){
      return (this * 100 | 0) + '%';
    },
    function isInt(){
      return this | 0 === this;
    },
    function clamp(min, max){
      return this < min ? min : this > max ? max : this;
    },
    function min(n){
      return this < n ? n : this;
    },
    function max(n){
      return this > n ? n : this;
    },
    function pow(n){
      return Math.pow(this, n);
    },
    function avg(n){
      return (this + n) / 2;
    },
    function sqrt(){
      return Math.sqrt(this);
    },
    function cubert(){
      return Math.pow(this, 1/3);
    },
    function sq(){
      return this * this;
    },
    function cube(){
      return this * this * this;
    }
  ]);







  // ###############################
  // ### RegExp Static Functions ###
  // ###############################

  embellish(RegExp, [
    function stringify(o){
      return stringifiers.RegExp.call(o);
    },
    function toSource(o){
      if (o instanceof RegExp)
        return o.source;
      else if (typeof o === 'string')
        return o;
      else if (o instanceof String)
        return o + '';
      else
        return '';
    },
    function isRegExp(o){
      return Object.stringify(o) === '[object RegExp]';
    },
    function create(pattern, flags, proto, descriptors){
      if (pattern instanceof RegExp) {
        flags = (flags || '') + pattern.flags();
        pattern = pattern.source;
      }

      return initialize(new RegExp(pattern+'', flags), proto, descriptors);
    },
    function escape(s) {
      return s.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
    },
    function concat(flags, regexes){
      if (Array.isArray(arguments[0]))
        regexes = arguments[0];
      else if (Array.isArray(arguments[1]))
        regexes = arguments[1];
      else
        regexes = Array.slice(arguments, typeof flags === 'string' ? 1 : 0);

      flags = typeof flags === 'string' ? flags : regexes[0] instanceof RegExp ? regexes[0].flags() : '';

      var source = regexes.map(function(regex, i){
        regex = RegExp.toSource(regex);
        if (i === 0)
          return regex.replace(/\$$/, '');
        else if (i === regexes.length - 1)
          return regex.replace(/^\^/, '');
        else
          return regex.replace(/^\^|\$$/g, '');
      });

      return new RegExp(source.join(''), flags);
    }
  ]);


  // ############################
  // ### RegExp Class Methods ###
  // ############################

  embellish(RegExp.prototype, [
    function globalize(){
      return this.global ? this : new RegExp(this.source, 'g' + this.flags());
    },
    function flags(){
      return [
        this.global     ? 'g' : '',
        this.ignoreCase ? 'i' : '',
        this.multiline  ? 'm' : '',
        this.extended   ? 'x' : '',
        this.sticky     ? 'y' : ''
      ].join('');
    },
    function match(str){
      var match, matches=[], i=0;
      var regex = this.globalize();
      while ((match = regex.exec(str)) !== null)
        matches.push(match);
      return matches
    },
    function filter(array){
      var self = this;
      function tester(s){ return self.test(s) }

      if (array && isIndexed(array))
        return Array.filter(array, tester);
      else
        return tester;
    }
  ]);


  // ################################
  // ### WeakMap Static Functions ###
  // ################################

  if (typeof WeakMap !== 'undefined') {
    [Map, WeakMap].forEach(function(Ctor){
      embellish(Ctor, [
        function createStorage(creator){
          creator = creator || Object.create.bind(null, null, {});
          var map = new Ctor;
          return function storage(o, v){
            if (1 in arguments) {
              map.set(o, v);
            } else {
              v = map.get(o);
              if (v == null) {
                v = creator(o);
                map.set(o, v);
              }
            }
            return v;
          };
        }
      ]);
    });
  }



  return ctx;
}

}(new Function('return this')(), typeof exports === 'undefined' ? this : exports, []);

