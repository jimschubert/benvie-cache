if (typeof Map === 'undefined') {
  var collections = require('harmony-collections');
  var Map = collections.map;
}

function paths(o){

  var p = '__proto__', G = this;
  return function recurse(current, path, seen, stack){
    var prop = Object.getOwnPropertyNames(current),val;
    prop.push(p);
    return prop.reduce(function(ret, prop){
      try {
        if (prop === p) {
          val = current[p];
        } else {
          val = Object.getOwnPropertyDescriptor(current, prop);
          if (!val || !('value' in val) || val.value === G) return ret;
          val = val.value;
        }
      } catch (e) { return ret }
      if (val === o) {
        ret.push(path.concat(prop));
      }
      if (Object(val) === val && !~seen.indexOf(val)) {
        seen.push(val);
        return ret.concat(recurse(val, path.concat(prop), seen, stack++));
      }
      return ret;
    }, []);
  }(G, [], [], 0);
}
